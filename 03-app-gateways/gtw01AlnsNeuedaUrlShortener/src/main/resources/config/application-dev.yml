# ===================================================================
# Spring Boot configuration.
#
# This configuration will be overridden by the Spring profile you use,
# for example application-dev.yml if you use the "dev" profile.
#
# More information on profiles: https://www.jhipster.tech/profiles/
# More information on configuration properties: https://www.jhipster.tech/common-application-properties/
# ===================================================================

# ===================================================================
# Standard Spring Boot properties.
# Full reference is available at:
# http://docs.spring.io/spring-boot/docs/current/reference/html/common-application-properties.html
# ===================================================================

server:
    port: 8080
    servlet:
        session:
            cookie:
                http-only: true

    # ===================================================================
    # Activate this profile to enable TLS and HTTP/2.
    #
    # JHipster has generated a self-signed certificate, which will be used to encrypt traffic.
    # As your browser will not understand this certificate, you will need to import it.
    #
    # Another (easiest) solution with Chrome is to enable the "allow-insecure-localhost" flag
    # at chrome://flags/#allow-insecure-localhost
    # ===================================================================
    # ssl:
    #    key-store: classpath:config/tls/keystore.p12
    #    key-store-password: password
    #    key-store-type: PKCS12
    #    key-alias: selfsigned
    #    ciphers: TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256, TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384, TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA, TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA, TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA256, TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA384, TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256, TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384, TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA, TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA, TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA256, TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA384, TLS_DHE_RSA_WITH_AES_128_GCM_SHA256, TLS_DHE_RSA_WITH_AES_256_GCM_SHA384, TLS_DHE_RSA_WITH_AES_128_CBC_SHA, TLS_DHE_RSA_WITH_AES_256_CBC_SHA, TLS_DHE_RSA_WITH_AES_128_CBC_SHA256, TLS_DHE_RSA_WITH_AES_256_CBC_SHA256
    #    enabled-protocols: TLSv1.2


spring:
    application:
        name: gtw01AlnsNeuedaUrlShortener

    mvc:
        favicon:
            enabled: false
    profiles:
        active: dev
        include:
            - swagger
            # Uncomment to activate TLS for the dev profile
            #- tls

    messages:
        basename: i18n/messages
        cache-duration: PT1S # 1 second, see the ISO 8601 standard

    devtools:
        restart:
            enabled: true
            additional-exclude: .h2.server.properties
        livereload:
            enabled: true # we use Webpack dev server + BrowserSync for livereload

    jackson:
        serialization:
            indent-output: true

    sleuth:
        sampler:
            probability: 1 # report 100% of traces

    zipkin: # Use the "zipkin" Maven profile to have the Spring Cloud Zipkin dependencies
        base-url: http://localhost:9411
        enabled: false
        locator:
            discovery:
                enabled: true

logging:
    level:
        ROOT: DEBUG
        io.github.jhipster: DEBUG
        br.org.codingtests.alnsneuedaurlshortener.gateway: DEBUG


eureka:
    client:
        enabled: true
        service-url:
            defaultZone: http://admin:${jhipster.registry.password}@localhost:8761/eureka/
        healthcheck:
            enabled: true
        fetch-registry: true
        register-with-eureka: true
        instance-info-replication-interval-seconds: 10
        registry-fetch-interval-seconds: 10
    instance:
        appname: gtw01alnsneuedaurlshortener
        prefer-ip-address: true
        instanceId: gtw01alnsneuedaurlshortener:${spring.application.instance-id:${random.value}}
        lease-renewal-interval-in-seconds: 5
        lease-expiration-duration-in-seconds: 10
        status-page-url-path: ${management.endpoints.web.base-path}/info
        health-check-url-path: ${management.endpoints.web.base-path}/health
        metadata-map:
            zone: primary # This is needed for the load balancer
            profile: ${spring.profiles.active}
            version: ${info.project.version:}
            git-version: ${git.commit.id.describe:}
            git-commit: ${git.commit.id.abbrev:}
            git-branch: ${git.branch:}

ribbon:
    eureka:
        enabled: true
# See http://cloud.spring.io/spring-cloud-netflix/spring-cloud-netflix.html
zuul: # those values must be configured depending on the application specific needs
    sensitive-headers: Cookie,Set-Cookie #see https://github.com/spring-cloud/spring-cloud-netflix/issues/3126
    host:
        max-total-connections: 1000
        max-per-route-connections: 100
    semaphore:
        max-semaphores: 500

# See https://github.com/Netflix/Hystrix/wiki/Configuration
hystrix:
    command:
        default:
            execution:
                isolation:
                    strategy: SEMAPHORE
                    thread:
                        timeoutInMilliseconds: 100000
    # See https://github.com/spring-cloud/spring-cloud-netflix/issues/1330
    #                    thread:
    #                        timeoutInMilliseconds: 10000
    shareSecurityContext: true

feign:
    hystrix:
        enabled: true
    client:
        config:
            default:
                connectTimeout: 60000
                readTimeout: 60000



# Properties to be exposed on the /info management endpoint
info:
    # Comma separated list of profiles that will trigger the ribbon to show
    display-ribbon-on-profiles: "dev"

# ===================================================================
# JHipster specific properties
#
# Full reference is available at: https://www.jhipster.tech/common-application-properties/
# ===================================================================

jhipster:
    http:
        version: V_1_1
        # version: V_2_0 # To use HTTP/2 you will need to activate TLS (see application-tls.yml)
        # CORS is disabled by default on microservices, as you should access them through a gateway.
        # If you want to enable it, please uncomment the configuration below.
        # By default CORS is disabled. Uncomment to enable.
        # cors:
        # allowed-origins: "*"
        # allowed-methods: "*"
        # allowed-headers: "*"
        # exposed-headers: "Authorization,Link,X-Total-Count"
        # allow-credentials: true
        # max-age: 1800
    # async:
    #    core-pool-size: 2
    #    max-pool-size: 50
    #    queue-capacity: 10000
    swagger:
        default-include-pattern: /api/.*
        title: VH-ALNS-Neueda-ShorterURLAPI
        description: CodingTest for Neueda '::' Java '::' By Andre Nascimento '::' API documentation
        version: 0.0.1
        terms-of-service-url:
        contact-name:
        contact-url:
        contact-email:
        license:
        license-url:

management:
    endpoints:
        web:
            base-path: /management
            exposure:
                include: ["configprops", "env", "health", "info", "threaddump", "logfile", "jhi-metrics", "prometheus" ]
            server:
                auto-time-requests: true


# ===================================================================
# Application specific properties
# Add your own application properties here, see the ApplicationProperties class
# to have type-safe configuration, like in the JHipsterProperties above
#
# More documentation is available at:
# https://www.jhipster.tech/common-application-properties/
# ===================================================================

# application:

# ===================================================================
# Application specific properties
# Add your own application properties here, see the ApplicationProperties class
# to have type-safe configuration, like in the JHipsterProperties above
#
# More documentation is available at:
# https://www.jhipster.tech/common-application-properties/
# ===================================================================

# application:
