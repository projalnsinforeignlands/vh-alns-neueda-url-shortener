import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { Gtw01AlnsNeuedaUrlShortenerSharedModule } from 'app/shared';
import { HOME_ROUTE, HomeComponent } from './';

import { UserExtraInfoComponent } from 'app/entities/ms01Users/user-extra-info';
import { ShortenedURLComponent } from 'app/entities/ms03UrlManagement/shortened-url';
import { StatisticsInfoComponent } from 'app/entities/ms03UrlManagement/statistics-info';
import { Gtw01AlnsNeuedaUrlShortenerEntityModule } from 'app/entities/entity.module';
import { KeyTextEncodedPreGeneratedComponent } from 'app/entities/ms02TextEncoder/key-text-encoded-pre-generated';

@NgModule({
    imports: [
        Gtw01AlnsNeuedaUrlShortenerSharedModule, 
        RouterModule.forChild([HOME_ROUTE]),
        Gtw01AlnsNeuedaUrlShortenerEntityModule,
    ],
    declarations: [
        HomeComponent,
        UserExtraInfoComponent,
        ShortenedURLComponent,
        StatisticsInfoComponent,
        KeyTextEncodedPreGeneratedComponent 
    ],
    entryComponents: [
        
    ],

    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class Gtw01AlnsNeuedaUrlShortenerHomeModule {}
