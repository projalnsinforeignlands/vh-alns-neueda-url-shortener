import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { NgbDateAdapter } from '@ng-bootstrap/ng-bootstrap';

import { NgbDateMomentAdapter } from './util/datepicker-adapter';
import {
    Gtw01AlnsNeuedaUrlShortenerSharedLibsModule,
    Gtw01AlnsNeuedaUrlShortenerSharedCommonModule
} from './';

@NgModule({
    imports: [Gtw01AlnsNeuedaUrlShortenerSharedLibsModule, Gtw01AlnsNeuedaUrlShortenerSharedCommonModule],
    declarations: [],
    providers: [{ provide: NgbDateAdapter, useClass: NgbDateMomentAdapter }],
    entryComponents: [],
    exports: [Gtw01AlnsNeuedaUrlShortenerSharedCommonModule],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class Gtw01AlnsNeuedaUrlShortenerSharedModule {
    static forRoot() {
        return {
            ngModule: Gtw01AlnsNeuedaUrlShortenerSharedModule
        };
    }
}
