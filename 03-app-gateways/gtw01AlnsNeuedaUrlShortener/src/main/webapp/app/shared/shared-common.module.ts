import { NgModule } from '@angular/core';

import { Gtw01AlnsNeuedaUrlShortenerSharedLibsModule, FindLanguageFromKeyPipe, JhigtwAlertComponent, JhigtwAlertErrorComponent } from './';

@NgModule({
    imports: [Gtw01AlnsNeuedaUrlShortenerSharedLibsModule],
    declarations: [FindLanguageFromKeyPipe, JhigtwAlertComponent, JhigtwAlertErrorComponent],
    exports: [Gtw01AlnsNeuedaUrlShortenerSharedLibsModule, FindLanguageFromKeyPipe, JhigtwAlertComponent, JhigtwAlertErrorComponent]
})
export class Gtw01AlnsNeuedaUrlShortenerSharedCommonModule {}
