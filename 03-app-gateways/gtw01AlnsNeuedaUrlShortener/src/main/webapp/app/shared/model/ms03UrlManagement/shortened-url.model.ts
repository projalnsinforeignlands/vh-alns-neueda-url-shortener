import { Moment } from 'moment';
import { IStatisticsInfo } from 'app/shared/model/ms03UrlManagement/statistics-info.model';

export const enum AlgorithmShorteningURLEnum {
    BASE64_UUID = 'BASE64_UUID',
    GOOGLE_API = 'GOOGLE_API'
}

export interface IShortenedURL {
    id?: number;
    userId?: number;
    descriptionLongURL?: string;
    descriptionShortenedURL?: string;
    generationAlgorithmType?: AlgorithmShorteningURLEnum;
    dateEncoding?: Moment;
    dateLastAccess?: Moment;
    isActive?: boolean;
    fullLinkToRedirect?: string;
    qtyDecodingRequestsByAPI?: number;
    qtyCallsForRedirecting?: number;
    statisticsInfos?: IStatisticsInfo[];
}

export class ShortenedURL implements IShortenedURL {
    constructor(
        public id?: number,
        public userId?: number,
        public descriptionLongURL?: string,
        public descriptionShortenedURL?: string,
        public generationAlgorithmType?: AlgorithmShorteningURLEnum,
        public dateEncoding?: Moment,
        public dateLastAccess?: Moment,
        public isActive?: boolean,
        public fullLinkToRedirect?: string,
        public qtyDecodingRequestsByAPI?: number,
        public qtyCallsForRedirecting?: number,
        public statisticsInfos?: IStatisticsInfo[]
    ) {
        this.isActive = this.isActive || false;
    }
}
