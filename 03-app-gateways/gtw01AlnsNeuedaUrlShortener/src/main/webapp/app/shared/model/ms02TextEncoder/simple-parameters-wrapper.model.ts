import { Moment } from 'moment';

export interface ISimpleParametersWrapper {
    id?: string;
    simpleParameterAsInteger?: number;
    simpleParameterAsString?: string;
    simpleParameterAsDate?: Moment;
    simpleParameterAsDouble?: number;
    complexParametersAsMap?: Map<string, string>;
    complexParametersAsList?: string[];

}

export class SimpleParametersWrapper implements ISimpleParametersWrapper {
    constructor(
        public id?: string, 
        public simpleParameterAsInteger?: number, 
        public simpleParameterAsString?: string,
        public simpleParameterAsDate?: Moment,
        public simpleParameterAsDouble?: number, 
        public complexParametersAsMap?: Map<string, string>,
        public complexParametersAsList?: string[]

    ) {}
}
