export interface IUserExtraInfo {
    id?: number;
    descriptionUserNameLogin?: string;
    descriptionFullname?: string;
    descriptionMainEmail?: string;
    descriptionGeographicLocalization?: string;
}

export class UserExtraInfo implements IUserExtraInfo {
    constructor(
        public id?: number,
        public descriptionUserNameLogin?: string,
        public descriptionFullname?: string,
        public descriptionMainEmail?: string,
        public descriptionGeographicLocalization?: string
    ) {}
}
