import { Moment } from 'moment';

export const enum KeyStatusEnum {
    AVAILABLE = 'AVAILABLE',
    RESERVED = 'RESERVED',
    IN_USE = 'IN_USE',
    INACTIVE = 'INACTIVE'
}

export const enum AlgorithmShorteningURLEnum {
    BASE64_UUID = 'BASE64_UUID',
    GOOGLE_API = 'GOOGLE_API'
}

export interface IKeyTextEncodedPreGenerated {
    id?: number;
    instantCreation?: Moment;
    status?: KeyStatusEnum;
    generationAlgorithmType?: AlgorithmShorteningURLEnum;
}

export class KeyTextEncodedPreGenerated implements IKeyTextEncodedPreGenerated {
    constructor(
        public id?: number, 
        public instantCreation?: Moment, 
        public status?: KeyStatusEnum,
        public generationAlgorithmType?: AlgorithmShorteningURLEnum
    ) {}
}
