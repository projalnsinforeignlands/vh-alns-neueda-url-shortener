import { Moment } from 'moment';

export const enum ReasonCallEnum {
    TO_REDIRECT = 'TO_REDIRECT',
    JUST_DECODING = 'JUST_DECODING'
}

export interface IStatisticsInfo {
    id?: number;
    reasonCallDoneType?: ReasonCallEnum;
    dateAccess?: Moment;
    numberIPAddressCaller?: string;
    countryCallerCode?: string;
    shortenedURLDescriptionShortenedURL?: string;
    shortenedURLId?: number;
}

export class StatisticsInfo implements IStatisticsInfo {
    constructor(
        public id?: number,
        public reasonCallDoneType?: ReasonCallEnum,
        public dateAccess?: Moment,
        public numberIPAddressCaller?: string,
        public countryCallerCode?: string,
        public shortenedURLDescriptionShortenedURL?: string,
        public shortenedURLId?: number
    ) {}
}
