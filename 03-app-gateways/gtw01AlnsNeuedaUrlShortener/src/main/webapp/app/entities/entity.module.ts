import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { UserExtraInfoComponent } from 'app/entities/ms01Users/user-extra-info';
import { ShortenedURLComponent } from 'app/entities/ms03UrlManagement/shortened-url';
import { StatisticsInfoComponent } from 'app/entities/ms03UrlManagement/statistics-info';


@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: 'user-extra-info',
                loadChildren: './ms01Users/user-extra-info/user-extra-info.module#Ms01UsersUserExtraInfoModule'
            },
            {
                path: 'key-text-encoded-pre-generated',
                loadChildren:
                    './ms02TextEncoder/key-text-encoded-pre-generated/key-text-encoded-pre-generated.module#Ms02TextEncoderKeyTextEncodedPreGeneratedModule'
            },
            {
                path: 'shortened-url',
                loadChildren: './ms03UrlManagement/shortened-url/shortened-url.module#Ms03UrlManagementShortenedURLModule'
            },
            {
                path: 'statistics-info',
                loadChildren: './ms03UrlManagement/statistics-info/statistics-info.module#Ms03UrlManagementStatisticsInfoModule'
            }
        ])
    ],
    declarations: [
        
    ],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class Gtw01AlnsNeuedaUrlShortenerEntityModule {}
