import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT, DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IShortenedURL } from 'app/shared/model/ms03UrlManagement/shortened-url.model';
import { ISimpleParametersWrapper } from 'app/shared/model/ms02TextEncoder/simple-parameters-wrapper.model';

type EntityResponseType = HttpResponse<IShortenedURL>;
type EntityArrayResponseType = HttpResponse<IShortenedURL[]>;

@Injectable({ providedIn: 'root' })
export class ShortenedURLService {
    public resourceUrl = SERVER_API_URL + 'ms03urlmanagement/api/shortened-urls';

    constructor(protected http: HttpClient) {}

    create(shortenedURL: IShortenedURL): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(shortenedURL);
        return this.http
            .post<IShortenedURL>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    update(shortenedURL: IShortenedURL): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(shortenedURL);
        return this.http
            .put<IShortenedURL>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http
            .get<IShortenedURL>(`${this.resourceUrl}/${id}`, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<IShortenedURL[]>(this.resourceUrl, { params: options, observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    findByUserId(pUserId: number, req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<IShortenedURL[]>(`${this.resourceUrl}/_search-by-userid/${pUserId}`, { params: options, observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }

    deleteByUserId(pUserId: number): Observable<HttpResponse<ISimpleParametersWrapper>> {
        return this.http.delete<any>(`${this.resourceUrl}/_delete-by-userid/${pUserId}`, { observe: 'response' });
    }

    protected convertDateFromClient(shortenedURL: IShortenedURL): IShortenedURL {
        const copy: IShortenedURL = Object.assign({}, shortenedURL, {
            dateEncoding:
                shortenedURL.dateEncoding != null && shortenedURL.dateEncoding.isValid()
                    ? shortenedURL.dateEncoding.format(DATE_TIME_FORMAT)
                    : null,
            dateLastAccess:
                shortenedURL.dateLastAccess != null && shortenedURL.dateLastAccess.isValid()
                    ? shortenedURL.dateLastAccess.format(DATE_TIME_FORMAT)
                    : null
        });
        return copy;
    }

    protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
        if (res.body) {
            res.body.dateEncoding = res.body.dateEncoding != null ? moment(res.body.dateEncoding) : null;
            res.body.dateLastAccess = res.body.dateLastAccess != null ? moment(res.body.dateLastAccess) : null;
        }
        return res;
    }

    protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
        if (res.body) {
            res.body.forEach((shortenedURL: IShortenedURL) => {
                shortenedURL.dateEncoding = shortenedURL.dateEncoding != null ? moment(shortenedURL.dateEncoding) : null;
                shortenedURL.dateLastAccess = shortenedURL.dateLastAccess != null ? moment(shortenedURL.dateLastAccess) : null;
            });
        }
        return res;
    }
}
