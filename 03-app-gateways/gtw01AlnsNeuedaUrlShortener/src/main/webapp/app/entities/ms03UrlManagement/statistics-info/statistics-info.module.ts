import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { Gtw01AlnsNeuedaUrlShortenerSharedModule } from 'app/shared';
import {
    StatisticsInfoComponent,
    statisticsInfoRoute
} from './';

const ENTITY_STATES = [...statisticsInfoRoute];

@NgModule({
    imports: [Gtw01AlnsNeuedaUrlShortenerSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        // StatisticsInfoComponent
    ],
    entryComponents: [
        StatisticsInfoComponent
    ],
    providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class Ms03UrlManagementStatisticsInfoModule {
    constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
        this.languageHelper.language.subscribe((languageKey: string) => {
            if (languageKey !== undefined) {
                this.languageService.changeLanguage(languageKey);
            }
        });
    }
}
