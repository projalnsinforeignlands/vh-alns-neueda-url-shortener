import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription, Observable } from 'rxjs';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { IShortenedURL, ShortenedURL, AlgorithmShorteningURLEnum } from 'app/shared/model/ms03UrlManagement/shortened-url.model';

import { ITEMS_PER_PAGE, DATE_TIME_FORMAT } from 'app/shared';
import { ShortenedURLService } from './shortened-url.service';
import { KeyTextEncodedPreGeneratedService } from 'app/entities/ms02TextEncoder/key-text-encoded-pre-generated';
import { IUserExtraInfo } from 'app/shared/model/ms01Users/user-extra-info.model';
import * as moment from 'moment';

@Component({
    selector: 'jhigtw-shortened-url',
    templateUrl: './shortened-url.component.html'
})
export class ShortenedURLComponent implements OnInit, OnDestroy {
    shortenedURL: IShortenedURL;
    userExtraInfoParent: IUserExtraInfo;
    isSaving: boolean;
    dateEncodingDp: any;
    dateLastAccessDp: any;

    shortenedURLS: IShortenedURL[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    routeData: any;
    links: any;
    totalItems: any;
    itemsPerPage: any;
    page: any = 1;
    predicate: any = 'id';
    previousPage: any = 1;
    reverse: any = true;

    constructor(
        protected shortenedURLService: ShortenedURLService,
        protected keyTextEncodedPreGeneratedService: KeyTextEncodedPreGeneratedService,
        protected parseLinks: JhiParseLinks,
        protected jhiAlertService: JhiAlertService,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe(data => {
            if (data && data.pagingParams) {
                this.page = data.pagingParams.page;
                this.previousPage = data.pagingParams.page;
                this.reverse = data.pagingParams.ascending;
                this.predicate = data.pagingParams.predicate;
            } else {
                this.page = 1;
                this.previousPage = 1;
                this.reverse = true;
                this.predicate = 'id';
            }
        });
    }

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ pShortenedURL }) => {

            if (pShortenedURL) {
                this.shortenedURL = pShortenedURL;
            } else {
                this.shortenedURL = new ShortenedURL();
                this.shortenedURL.generationAlgorithmType = AlgorithmShorteningURLEnum.GOOGLE_API;
            }
        });
        this.loadAll();
        this.registerChangeInShortenedURLS();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    loadAll() {

        if (this.userExtraInfoParent) {

            this.shortenedURLService
                .findByUserId(this.userExtraInfoParent.id,
                              {
                               page: this.page - 1,
                               size: this.itemsPerPage,
                               sort: this.sort()
                              }
                )
            .subscribe(
                (res: HttpResponse<IShortenedURL[]>) => this.paginateShortenedURLS(res.body, res.headers),
                (res: HttpErrorResponse) => this.onError(res.message)
            );

        }
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/shortened-url'], {
            queryParams: {
                page: this.page,
                size: this.itemsPerPage,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.router.navigate([
            '/shortened-url',
            {
                page: this.page,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        ]);
        this.loadAll();
    }

    trackId(index: number, item: IShortenedURL) {
        return item.id;
    }

    registerChangeInShortenedURLS() {
        this.eventSubscriber = this.eventManager.subscribe('shortenedURLListModification', response => this.loadAll())
                                                .add(this.eventManager.subscribe('userExtraInfoSelectedEvent', pEvent => this.handleUserExtraInfoRelationship(pEvent, 'SELECTED')))
                                                .add(this.eventManager.subscribe('userExtraInfoDeletedEvent', pEvent => this.handleUserExtraInfoRelationship(pEvent, 'DELETED')));
    }

    private handleUserExtraInfoRelationship(pEvent: any, pEventType: string): void {

        this.userExtraInfoParent = pEvent.content.UserExtraInfoSelected;
        this.shortenedURL = new ShortenedURL();
        this.shortenedURL.userId = this.userExtraInfoParent.id;
        this.loadAll();

        if (pEventType === 'SELECTED') {

            this.eventManager.broadcast({
                name: 'shortenedURLSelectedEvent',
                content: {'ShortenedURLSelected': (this.shortenedURLS && this.shortenedURLS.length > 0 ? this.shortenedURLS[0] : this.shortenedURL),
                          'mensagemEvento': 'ShortenedURL Selected'}
            });

        }

        if (pEventType === 'DELETED' && this.shortenedURLS && this.shortenedURLS.length > 0) {

            this.shortenedURLService.deleteByUserId(this.userExtraInfoParent.id).subscribe(response => {
                this.eventManager.broadcast({
                    name: 'shortenedURLListModification',
                    content: 'Deleted an shortenedURL'
                });
                this.loadAll();
            });
        }
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    protected paginateShortenedURLS(data: IShortenedURL[], headers: HttpHeaders) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = parseInt(headers.get('X-Total-Count'), 10);
        this.shortenedURLS = data;

        if (this.shortenedURLS && this.shortenedURLS.length > 0) {
            this.selectShortenedURL(this.shortenedURLS[0]);
        }

    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    public save(): void {
        this.isSaving = true;
        if (this.shortenedURL.id !== undefined) {
            this.subscribeToSaveResponse(this.shortenedURLService.update(this.shortenedURL));
        } else {
            this.shortenedURL.dateEncoding = moment();
            this.shortenedURL.dateLastAccess = moment();
            this.shortenedURL.isActive = true;
            this.shortenedURL.qtyCallsForRedirecting = 0;
            this.shortenedURL.qtyDecodingRequestsByAPI = 0;
            this.shortenedURL.userId = this.userExtraInfoParent.id;
            this.subscribeToSaveResponse(this.shortenedURLService.create(this.shortenedURL));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IShortenedURL>>) {
        result.subscribe((res: HttpResponse<IShortenedURL>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.shortenedURL = new ShortenedURL();
        this.shortenedURL.userId = this.userExtraInfoParent.id;
        this.loadAll();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    previousState() {
        window.history.back();
    }

    public selectShortenedURL(pSelectedItem: IShortenedURL): void {

        this.eventManager.broadcast({
            name: 'shortenedURLSelectedEvent',
            content: {'ShortenedURLSelected': pSelectedItem, 'mensagemEvento': 'ShortenedURL Selected'}
        });

    }

    public generateShortURL(pSelectedItem: IShortenedURL): void {

        this.keyTextEncodedPreGeneratedService.getNextEncodedKeyAvailable(pSelectedItem.generationAlgorithmType)
                                              .subscribe(pHttpResponse => {
            pSelectedItem.descriptionShortenedURL = pHttpResponse.body.simpleParameterAsString;
        });

    }

    public followShortURL(pSelectedItem: IShortenedURL): void {
       window.open(pSelectedItem.fullLinkToRedirect + this.createTagToAvoidBrowserCache(), '_blank');
       event.preventDefault();
       this.selectShortenedURL(pSelectedItem);
    }

    public deleteRecord(pSelectedItem: IShortenedURL, pIsShouldLoadAll: boolean): void {

        this.shortenedURLService.delete(pSelectedItem.id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'shortenedURLListModification',
                content: 'Deleted an shortenedURL'
            });

            this.eventManager.broadcast({
                name: 'shortenedURLDeletedEvent',
                content: {'ShortenedURLDeleted': pSelectedItem, 'mensagemEvento': 'ShortenedURL Deleted'}
            });

            if (pIsShouldLoadAll) {
                this.loadAll();
            }
        });
    }

    private createTagToAvoidBrowserCache(): string {

        let strTagResult: string = ';';
        // const chars = [...'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'];
        // and then just do:
        // strTagResult = [...Array(10)].map(i=>chars[Math.random()*chars.length|0]).join;
        strTagResult += Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);

        // strTagResult += new Date().toISOString();
        //  strTagResult += '-' + Math.random;

        return strTagResult;
    }

}
