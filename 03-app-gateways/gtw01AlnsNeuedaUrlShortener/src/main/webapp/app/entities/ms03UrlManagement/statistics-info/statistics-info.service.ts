import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT, DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IStatisticsInfo } from 'app/shared/model/ms03UrlManagement/statistics-info.model';

type EntityResponseType = HttpResponse<IStatisticsInfo>;
type EntityArrayResponseType = HttpResponse<IStatisticsInfo[]>;

@Injectable({ providedIn: 'root' })
export class StatisticsInfoService {
    public resourceUrl = SERVER_API_URL + 'ms03urlmanagement/api/statistics-infos';

    constructor(protected http: HttpClient) {}

    create(statisticsInfo: IStatisticsInfo): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(statisticsInfo);
        return this.http
            .post<IStatisticsInfo>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    update(statisticsInfo: IStatisticsInfo): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(statisticsInfo);
        return this.http
            .put<IStatisticsInfo>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http
            .get<IStatisticsInfo>(`${this.resourceUrl}/${id}`, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<IStatisticsInfo[]>(this.resourceUrl, { params: options, observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }

    findByShortenedURLId(pShortenedURLId: number, req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<IStatisticsInfo[]>(`${this.resourceUrl}/_search-by-shortenedurlid/${pShortenedURLId}`, { params: options, observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    protected convertDateFromClient(statisticsInfo: IStatisticsInfo): IStatisticsInfo {
        const copy: IStatisticsInfo = Object.assign({}, statisticsInfo, {
            dateAccess:
                statisticsInfo.dateAccess != null && statisticsInfo.dateAccess.isValid()
                    ? statisticsInfo.dateAccess.format(DATE_TIME_FORMAT)
                    : null
        });
        return copy;
    }

    protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
        if (res.body) {
            res.body.dateAccess = res.body.dateAccess != null ? moment(res.body.dateAccess) : null;
        }
        return res;
    }

    protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
        if (res.body) {
            res.body.forEach((statisticsInfo: IStatisticsInfo) => {
                statisticsInfo.dateAccess = statisticsInfo.dateAccess != null ? moment(statisticsInfo.dateAccess) : null;
            });
        }
        return res;
    }
}
