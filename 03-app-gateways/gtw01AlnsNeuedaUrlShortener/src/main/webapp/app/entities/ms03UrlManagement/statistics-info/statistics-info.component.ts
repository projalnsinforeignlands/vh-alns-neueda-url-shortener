import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { IStatisticsInfo } from 'app/shared/model/ms03UrlManagement/statistics-info.model';

import { ITEMS_PER_PAGE } from 'app/shared';
import { StatisticsInfoService } from './statistics-info.service';
import { IShortenedURL } from 'app/shared/model/ms03UrlManagement/shortened-url.model';
import { ShortenedURLService } from '../shortened-url';

@Component({
    selector: 'jhigtw-statistics-info',
    templateUrl: './statistics-info.component.html'
})
export class StatisticsInfoComponent implements OnInit, OnDestroy {
    statisticsInfosRegisteredList: IStatisticsInfo[];
    shortenedURLParent: IShortenedURL;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    routeData: any;
    links: any;
    totalItems: any;
    itemsPerPage: any;
    page: any = 1;
    predicate: any = 'dateAccess';
    previousPage: any = 1;
    reverse: any;

    constructor(
        protected statisticsInfoService: StatisticsInfoService,
        protected shortenedURLService: ShortenedURLService,
        protected parseLinks: JhiParseLinks,
        protected jhiAlertService: JhiAlertService,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe(data => {

            if (data && data.pagingParams) {
                this.page = data.pagingParams.page;
                this.previousPage = data.pagingParams.page;
                this.reverse = data.pagingParams.descending;
                this.predicate = data.pagingParams.predicate;
            } else {
                this.page = 1;
                this.previousPage = 1;
                this.reverse = true;
                this.predicate = 'dateAccess';
            }

        });
    }

    ngOnInit() {
        this.loadAll();
        this.registerChangeInStatisticsInfos();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    loadAll() {

        if (this.shortenedURLParent && this.shortenedURLParent.id) {

            this.statisticsInfoService
                .findByShortenedURLId(this.shortenedURLParent.id,
                                     {
                                      page: this.page - 1,
                                      size: this.itemsPerPage,
                                      sort: this.sort()
                                     }
                )
            .subscribe(
                (res: HttpResponse<IStatisticsInfo[]>) => this.paginateStatisticsInfos(res.body, res.headers),
                (res: HttpErrorResponse) => this.onError(res.message)
            );

        } else {

            this.statisticsInfosRegisteredList = null;

        }

    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/statistics-info'], {
            queryParams: {
                page: this.page,
                size: this.itemsPerPage,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.router.navigate([
            '/statistics-info',
            {
                page: this.page,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        ]);
        this.loadAll();
    }

    trackId(index: number, item: IStatisticsInfo) {
        return item.id;
    }

    registerChangeInStatisticsInfos() {
        this.eventSubscriber = this.eventManager.subscribe('statisticsInfoListModification', response => this.loadAll())
                                                .add(this.eventManager.subscribe('shortenedURLSelectedEvent', pEvent => this.handleShortenedURLRelationship(pEvent, 'SELECTED')))
                                                .add(this.eventManager.subscribe('shortenedURLDeletedEvent', pEvent => this.handleShortenedURLRelationship(pEvent, 'DELETED')));                                                
    }

    private handleShortenedURLRelationship(pEvent: any, pEventType: string): void {

        this.shortenedURLParent = pEvent.content.ShortenedURLSelected;
        // Update teh ShortenedURL because should be happened more clicks on it.
        if (this.shortenedURLParent && this.shortenedURLParent.id) {

            this.shortenedURLService.find(this.shortenedURLParent.id)
                                    .subscribe( (res: HttpResponse<IShortenedURL>) => this.shortenedURLParent = res.body,
                                                (res: HttpErrorResponse) => this.onError(res.message));
        }
        this.loadAll();

        if (pEventType == 'DELETED' && this.statisticsInfosRegisteredList && this.statisticsInfosRegisteredList.length > 0) {

            // The Frontend won't need to do anything when a ShortenedURL is deleted, 
            // because the children StatisticsInfo will be deleted at JPA/Hibernate level,
            // due "CascadeType.ALL on @OneToMany relationship."
            /*
            this.shortenedURLService.deleteByUserId(this.userExtraInfoParent.id).subscribe(response => {
                this.eventManager.broadcast({
                    name: 'shortenedURLListModification',
                    content: 'Deleted an shortenedURL'
                });
                this.loadAll();
            });
            */
        }
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    protected paginateStatisticsInfos(data: IStatisticsInfo[], headers: HttpHeaders) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = parseInt(headers.get('X-Total-Count'), 10);
        this.statisticsInfosRegisteredList = data;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
