import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { ShortenedURL } from 'app/shared/model/ms03UrlManagement/shortened-url.model';
import { ShortenedURLService } from './shortened-url.service';
import { ShortenedURLComponent } from './shortened-url.component';
import { IShortenedURL } from 'app/shared/model/ms03UrlManagement/shortened-url.model';

@Injectable({ providedIn: 'root' })
export class ShortenedURLResolve implements Resolve<IShortenedURL> {
    constructor(private service: ShortenedURLService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IShortenedURL> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<ShortenedURL>) => response.ok),
                map((shortenedURL: HttpResponse<ShortenedURL>) => shortenedURL.body)
            );
        }
        return of(new ShortenedURL());
    }
}

export const shortenedURLRoute: Routes = [
    {
        path: '',
        component: ShortenedURLComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams
        },
        data: {
            authorities: [''],
            defaultSort: 'id,asc',
            pageTitle: 'gtw01AlnsNeuedaUrlShortenerApp.ms03UrlManagementShortenedUrl.home.title'
        },
        canActivate: [UserRouteAccessService]
    } 
];
