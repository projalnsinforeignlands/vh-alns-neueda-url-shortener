import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { StatisticsInfo } from 'app/shared/model/ms03UrlManagement/statistics-info.model';
import { StatisticsInfoService } from './statistics-info.service';
import { StatisticsInfoComponent } from './statistics-info.component';
import { IStatisticsInfo } from 'app/shared/model/ms03UrlManagement/statistics-info.model';

@Injectable({ providedIn: 'root' })
export class StatisticsInfoResolve implements Resolve<IStatisticsInfo> {
    constructor(private service: StatisticsInfoService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IStatisticsInfo> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<StatisticsInfo>) => response.ok),
                map((statisticsInfo: HttpResponse<StatisticsInfo>) => statisticsInfo.body)
            );
        }
        return of(new StatisticsInfo());
    }
}

export const statisticsInfoRoute: Routes = [
    {
        path: '',
        component: StatisticsInfoComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams
        },
        data: {
            authorities: [''],
            defaultSort: 'id,asc',
            pageTitle: 'gtw01AlnsNeuedaUrlShortenerApp.ms03UrlManagementStatisticsInfo.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];
