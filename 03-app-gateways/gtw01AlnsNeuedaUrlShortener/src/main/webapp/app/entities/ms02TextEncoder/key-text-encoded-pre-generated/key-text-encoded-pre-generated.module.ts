import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { Gtw01AlnsNeuedaUrlShortenerSharedModule } from 'app/shared';
import {
    KeyTextEncodedPreGeneratedComponent,
    keyTextEncodedPreGeneratedRoute
} from './';

const ENTITY_STATES = [...keyTextEncodedPreGeneratedRoute];

@NgModule({
    imports: [Gtw01AlnsNeuedaUrlShortenerSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        // KeyTextEncodedPreGeneratedComponent
    ],
    entryComponents: [
        KeyTextEncodedPreGeneratedComponent
    ],
    providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class Ms02TextEncoderKeyTextEncodedPreGeneratedModule {
    constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
        this.languageHelper.language.subscribe((languageKey: string) => {
            if (languageKey !== undefined) {
                this.languageService.changeLanguage(languageKey);
            }
        });
    }
}
