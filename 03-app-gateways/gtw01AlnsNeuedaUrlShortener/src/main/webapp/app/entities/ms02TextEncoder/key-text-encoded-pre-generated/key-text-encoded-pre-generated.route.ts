import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { KeyTextEncodedPreGenerated } from 'app/shared/model/ms02TextEncoder/key-text-encoded-pre-generated.model';
import { KeyTextEncodedPreGeneratedService } from './key-text-encoded-pre-generated.service';
import { KeyTextEncodedPreGeneratedComponent } from './key-text-encoded-pre-generated.component';
import { IKeyTextEncodedPreGenerated } from 'app/shared/model/ms02TextEncoder/key-text-encoded-pre-generated.model';

@Injectable({ providedIn: 'root' })
export class KeyTextEncodedPreGeneratedResolve implements Resolve<IKeyTextEncodedPreGenerated> {
    constructor(private service: KeyTextEncodedPreGeneratedService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IKeyTextEncodedPreGenerated> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.query({'id': id}).pipe(
                filter((response: HttpResponse<KeyTextEncodedPreGenerated[]>) => response.ok),
                map((response: HttpResponse<KeyTextEncodedPreGenerated[]>) => response.body[0])
            );
        }
        return of(new KeyTextEncodedPreGenerated());
    }
}

export const keyTextEncodedPreGeneratedRoute: Routes = [
    {
        path: '',
        component: KeyTextEncodedPreGeneratedComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            defaultSort: 'id,asc',
            pageTitle: 'gtw01AlnsNeuedaUrlShortenerApp.ms02TextEncoderKeyTextEncodedPreGenerated.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
];
