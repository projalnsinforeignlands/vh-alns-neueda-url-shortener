import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { IKeyTextEncodedPreGenerated } from 'app/shared/model/ms02TextEncoder/key-text-encoded-pre-generated.model';

import { ITEMS_PER_PAGE } from 'app/shared';
import { KeyTextEncodedPreGeneratedService } from './key-text-encoded-pre-generated.service';

@Component({
    selector: 'jhigtw-key-text-encoded-pre-generated',
    templateUrl: './key-text-encoded-pre-generated.component.html'
})
export class KeyTextEncodedPreGeneratedComponent implements OnInit, OnDestroy {
    currentAccount: any;
    keyTextEncodedPreGenerateds: IKeyTextEncodedPreGenerated[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    routeData: any;
    links: any;
    totalItems: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;

    constructor(
        protected keyTextEncodedPreGeneratedService: KeyTextEncodedPreGeneratedService,
        protected parseLinks: JhiParseLinks,
        protected jhiAlertService: JhiAlertService,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe(data => {
            this.page = data.pagingParams.page;
            this.previousPage = data.pagingParams.page;
            this.reverse = data.pagingParams.ascending;
            this.predicate = data.pagingParams.predicate;
        });
    }

    loadAll() {
        this.keyTextEncodedPreGeneratedService
            .query({
                page: this.page - 1,
                size: this.itemsPerPage,
                sort: this.sort()
            })
            .subscribe(
                (res: HttpResponse<IKeyTextEncodedPreGenerated[]>) => this.paginateKeyTextEncodedPreGenerateds(res.body, res.headers),
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/key-text-encoded-pre-generated'], {
            queryParams: {
                page: this.page,
                size: this.itemsPerPage,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.router.navigate([
            '/key-text-encoded-pre-generated',
            {
                page: this.page,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        ]);
        this.loadAll();
    }

    ngOnInit() {
        this.loadAll();
        this.registerChangeInKeyTextEncodedPreGenerateds();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IKeyTextEncodedPreGenerated) {
        return item.id;
    }

    registerChangeInKeyTextEncodedPreGenerateds() {
        this.eventSubscriber = this.eventManager.subscribe('keyTextEncodedPreGeneratedListModification', response => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    protected paginateKeyTextEncodedPreGenerateds(data: IKeyTextEncodedPreGenerated[], headers: HttpHeaders) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = parseInt(headers.get('X-Total-Count'), 10);
        this.keyTextEncodedPreGenerateds = data;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
