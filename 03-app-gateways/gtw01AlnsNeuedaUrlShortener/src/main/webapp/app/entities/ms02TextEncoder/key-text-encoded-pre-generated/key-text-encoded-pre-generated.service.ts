import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IKeyTextEncodedPreGenerated } from 'app/shared/model/ms02TextEncoder/key-text-encoded-pre-generated.model';
import { ISimpleParametersWrapper } from 'app/shared/model/ms02TextEncoder/simple-parameters-wrapper.model';

type EntityResponseType = HttpResponse<IKeyTextEncodedPreGenerated>;
type EntityArrayResponseType = HttpResponse<IKeyTextEncodedPreGenerated[]>;

@Injectable({ providedIn: 'root' })
export class KeyTextEncodedPreGeneratedService {

    public resourceUrlOnlyPrefix = SERVER_API_URL + 'ms02textencoder/api';
    public resourceUrlOriginal = this.resourceUrlOnlyPrefix + '/key-text-encoded-pre-generateds';

    constructor(protected http: HttpClient) {}

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<IKeyTextEncodedPreGenerated[]>(this.resourceUrlOriginal, { params: options, observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }

    createChunkEncodedkeys(): Observable<EntityArrayResponseType> {
        return this.http
            .post<IKeyTextEncodedPreGenerated[]>(this.resourceUrlOnlyPrefix + '/create-chunk-encodedkeys', null, { observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }

    deleteOrInactivateAllEncodedKeys(pDontDeleteAllJustMarkInactive: boolean): Observable<HttpResponse<number>> {
        return this.http.delete<number>(`${this.resourceUrlOnlyPrefix + '/delete-or-inactivate-all-encoded-keys'}/${pDontDeleteAllJustMarkInactive}`, { observe: 'response' });
    }

    changeStatusEncodedKey(pEncodedKeyTarget: string, pStatusToChange: string):Observable<HttpResponse<number>> {

        return this.http.put<number>(`${this.resourceUrlOnlyPrefix + '/change-status-encoded-key'}/${pEncodedKeyTarget}/${pStatusToChange}`, null, { observe: 'response' });
    }

    getNextEncodedKeyAvailable(pAlgorithmTarget: string): Observable<HttpResponse<ISimpleParametersWrapper>> {

        return this.http.get<ISimpleParametersWrapper>(`${this.resourceUrlOnlyPrefix + '/next-encoded-key-available'}/${pAlgorithmTarget}`, { observe: 'response' });
    }

    protected convertDateFromClient(keyTextEncodedPreGenerated: IKeyTextEncodedPreGenerated): IKeyTextEncodedPreGenerated {
        const copy: IKeyTextEncodedPreGenerated = Object.assign({}, keyTextEncodedPreGenerated, {
            instantCreation:
                keyTextEncodedPreGenerated.instantCreation != null && keyTextEncodedPreGenerated.instantCreation.isValid()
                    ? keyTextEncodedPreGenerated.instantCreation.toJSON()
                    : null
        });
        return copy;
    }

    protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
        if (res.body) {
            res.body.instantCreation = res.body.instantCreation != null ? moment(res.body.instantCreation) : null;
        }
        return res;
    }

    protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
        if (res.body) {
            res.body.forEach((keyTextEncodedPreGenerated: IKeyTextEncodedPreGenerated) => {
                keyTextEncodedPreGenerated.instantCreation =
                    keyTextEncodedPreGenerated.instantCreation != null ? moment(keyTextEncodedPreGenerated.instantCreation) : null;
            });
        }
        return res;
    }
}
