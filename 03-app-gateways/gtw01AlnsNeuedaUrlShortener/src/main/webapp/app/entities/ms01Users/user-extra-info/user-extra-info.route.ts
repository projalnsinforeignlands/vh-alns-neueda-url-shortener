import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { UserExtraInfo } from 'app/shared/model/ms01Users/user-extra-info.model';
import { UserExtraInfoService } from './user-extra-info.service';
import { UserExtraInfoComponent } from './user-extra-info.component';
import { IUserExtraInfo } from 'app/shared/model/ms01Users/user-extra-info.model';

@Injectable({ providedIn: 'root' })
export class UserExtraInfoResolve implements Resolve<IUserExtraInfo> {
    constructor(private service: UserExtraInfoService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IUserExtraInfo> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<UserExtraInfo>) => response.ok),
                map((userExtraInfo: HttpResponse<UserExtraInfo>) => userExtraInfo.body)
            );
        }
        return of(new UserExtraInfo());
    }
}

export const userExtraInfoRoute: Routes = [
    {
        path: '',
        component: UserExtraInfoComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams
        },
        data: {
            authorities: [''],
            defaultSort: 'id,asc',
            pageTitle: 'gtw01AlnsNeuedaUrlShortenerApp.ms01UsersUserExtraInfo.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
];
