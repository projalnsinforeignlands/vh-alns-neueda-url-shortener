import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription, Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { IUserExtraInfo, UserExtraInfo } from 'app/shared/model/ms01Users/user-extra-info.model';

import { ITEMS_PER_PAGE } from 'app/shared';
import { UserExtraInfoService } from './user-extra-info.service';

@Component({
    selector: 'jhigtw-user-extra-info',
    templateUrl: './user-extra-info.component.html'
})
export class UserExtraInfoComponent implements OnInit, OnDestroy {

    userExtraInfo: IUserExtraInfo;
    isSaving: boolean;
    userExtraInfos: IUserExtraInfo[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    routeData: any;
    links: any;
    totalItems: any;
    itemsPerPage: any;
    page: any = 1;
    predicate: any = 'id';
    previousPage: any = 1;
    reverse: any = true;

    constructor(
        protected userExtraInfoService: UserExtraInfoService,
        protected parseLinks: JhiParseLinks,
        protected jhiAlertService: JhiAlertService,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected eventManager: JhiEventManager
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe(data => {

            if (data && data.pagingParams) {
                this.page = data.pagingParams.page;
                this.previousPage = data.pagingParams.page;
                this.reverse = data.pagingParams.ascending;
                this.predicate = data.pagingParams.predicate;
            } else {
                this.page = 1;
                this.previousPage = 1;
                this.reverse = true;
                this.predicate = 'id';
            }
        });
    }

    ngOnInit() {

        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ pUserExtraInfo }) => {

            if (pUserExtraInfo) {
                this.userExtraInfo = pUserExtraInfo;
            } else {
                this.userExtraInfo = new UserExtraInfo();
            }
        });
        this.loadAll();
        this.registerChangeInUserExtraInfos();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    loadAll() {
        this.userExtraInfoService
            .query({
                page: this.page - 1,
                size: this.itemsPerPage,
                sort: this.sort()
            })
            .subscribe(
                (res: HttpResponse<IUserExtraInfo[]>) => this.paginateUserExtraInfos(res.body, res.headers),
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/user-extra-info'], {
            queryParams: {
                page: this.page,
                size: this.itemsPerPage,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.router.navigate([
            '/user-extra-info',
            {
                page: this.page,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        ]);
        this.loadAll();
    }

    trackId(index: number, item: IUserExtraInfo) {
        return item.id;
    }

    registerChangeInUserExtraInfos() {
        this.eventSubscriber = this.eventManager.subscribe('userExtraInfoListModification', response => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    protected paginateUserExtraInfos(data: IUserExtraInfo[], headers: HttpHeaders) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = parseInt(headers.get('X-Total-Count'), 10);
        this.userExtraInfos = data;

        if (this.userExtraInfos && this.userExtraInfos.length > 0) {
            this.selectUser(this.userExtraInfos[this.userExtraInfos.length - 1]);
        }

    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    public save(): void {
        this.isSaving = true;
        if (this.userExtraInfo.id !== undefined) {
            this.subscribeToSaveResponse(this.userExtraInfoService.update(this.userExtraInfo));
        } else {
            this.subscribeToSaveResponse(this.userExtraInfoService.create(this.userExtraInfo));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IUserExtraInfo>>) {
        result.subscribe((res: HttpResponse<IUserExtraInfo>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.userExtraInfo = new UserExtraInfo();
        this.loadAll();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    previousState() {
        window.history.back();
    }

    public selectUser(pSelectedUser: IUserExtraInfo): void {

        this.eventManager.broadcast({
            name: 'userExtraInfoSelectedEvent',
            content: {'UserExtraInfoSelected': pSelectedUser, 'mensagemEvento': 'UserExtraInfo Selected'}
        });

    }

    public deleteRecord(pSelectedUser: IUserExtraInfo): void {
        this.userExtraInfoService.delete(pSelectedUser.id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'userExtraInfoListModification',
                content: 'Deleted an userExtraInfo'
            });

            this.eventManager.broadcast({
                name: 'userExtraInfoDeletedEvent',
                content: {'UserExtraInfoSelected': pSelectedUser, 'mensagemEvento': 'UserExtraInfo Deleted.'}
            });
    
            this.loadAll();
        });
    }

}
