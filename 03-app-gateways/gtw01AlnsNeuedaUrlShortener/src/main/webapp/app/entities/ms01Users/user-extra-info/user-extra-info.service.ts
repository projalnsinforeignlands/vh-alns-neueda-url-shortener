import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IUserExtraInfo } from 'app/shared/model/ms01Users/user-extra-info.model';

type EntityResponseType = HttpResponse<IUserExtraInfo>;
type EntityArrayResponseType = HttpResponse<IUserExtraInfo[]>;

@Injectable({ providedIn: 'root' })
export class UserExtraInfoService {
    public resourceUrl = SERVER_API_URL + 'ms01users/api/user-extra-infos';

    constructor(protected http: HttpClient) {}

    create(userExtraInfo: IUserExtraInfo): Observable<EntityResponseType> {
        return this.http.post<IUserExtraInfo>(this.resourceUrl, userExtraInfo, { observe: 'response' });
    }

    update(userExtraInfo: IUserExtraInfo): Observable<EntityResponseType> {
        return this.http.put<IUserExtraInfo>(this.resourceUrl, userExtraInfo, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IUserExtraInfo>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IUserExtraInfo[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
