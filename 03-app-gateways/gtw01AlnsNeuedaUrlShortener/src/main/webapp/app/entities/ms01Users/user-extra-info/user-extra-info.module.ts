import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { Gtw01AlnsNeuedaUrlShortenerSharedModule } from 'app/shared';
import {
    UserExtraInfoComponent,
    userExtraInfoRoute
} from './';

const ENTITY_STATES = [...userExtraInfoRoute];

@NgModule({
    imports: [Gtw01AlnsNeuedaUrlShortenerSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        // UserExtraInfoComponent
    ],
    entryComponents: [
        UserExtraInfoComponent
    ],
    providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class Ms01UsersUserExtraInfoModule {
    constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
        this.languageHelper.language.subscribe((languageKey: string) => {
            if (languageKey !== undefined) {
                this.languageService.changeLanguage(languageKey);
            }
        });
    }
}
