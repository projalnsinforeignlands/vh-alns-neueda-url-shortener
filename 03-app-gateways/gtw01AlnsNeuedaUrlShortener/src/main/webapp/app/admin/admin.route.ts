import { Routes } from '@angular/router';

import { docsRoute, gatewayRoute } from './';

import { UserRouteAccessService } from 'app/core';

const ADMIN_ROUTES = [docsRoute, gatewayRoute];

export const adminState: Routes = [
    {
        path: '',
        data: {
            authorities: []
        },
        canActivate: [UserRouteAccessService],
        children: ADMIN_ROUTES
    }
];
