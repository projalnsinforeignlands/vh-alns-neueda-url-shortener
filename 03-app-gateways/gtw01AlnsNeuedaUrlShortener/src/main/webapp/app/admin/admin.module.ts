import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';
import { Gtw01AlnsNeuedaUrlShortenerSharedModule } from 'app/shared';

import {
    adminState,
    JhigtwDocsComponent,
    JhigtwGatewayComponent
} from './';

@NgModule({
    imports: [
        Gtw01AlnsNeuedaUrlShortenerSharedModule,
        RouterModule.forChild(adminState)
    ],
    declarations: [
        JhigtwDocsComponent,
        JhigtwGatewayComponent
    ],
    providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
    entryComponents: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class Gtw01AlnsNeuedaUrlShortenerAdminModule {
    constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
        this.languageHelper.language.subscribe((languageKey: string) => {
            if (languageKey !== undefined) {
                this.languageService.changeLanguage(languageKey);
            }
        });
    }
}
