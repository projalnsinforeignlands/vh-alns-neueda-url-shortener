import { Route } from '@angular/router';

import { JhigtwGatewayComponent } from './gateway.component';

export const gatewayRoute: Route = {
    path: 'gateway',
    component: JhigtwGatewayComponent,
    data: {
        pageTitle: 'gateway.title'
    }
};
