import { Route } from '@angular/router';

import { JhigtwDocsComponent } from './docs.component';

export const docsRoute: Route = {
    path: 'docs',
    component: JhigtwDocsComponent,
    data: {
        pageTitle: 'global.menu.admin.apidocs'
    }
};
