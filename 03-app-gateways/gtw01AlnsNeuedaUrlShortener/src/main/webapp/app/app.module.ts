import './vendor.ts';

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgbDatepickerConfig } from '@ng-bootstrap/ng-bootstrap';
import { Ng2Webstorage } from 'ngx-webstorage';
import { NgJhipsterModule } from 'ng-jhipster';

import { ErrorHandlerInterceptor } from './blocks/interceptor/errorhandler.interceptor';
import { NotificationInterceptor } from './blocks/interceptor/notification.interceptor';
import { Gtw01AlnsNeuedaUrlShortenerSharedModule } from 'app/shared';
import { Gtw01AlnsNeuedaUrlShortenerCoreModule } from 'app/core';
import { Gtw01AlnsNeuedaUrlShortenerAppRoutingModule } from './app-routing.module';
import { Gtw01AlnsNeuedaUrlShortenerHomeModule } from './home/home.module';
import { Gtw01AlnsNeuedaUrlShortenerEntityModule } from './entities/entity.module';
import * as moment from 'moment';
import { JhigtwMainComponent, NavbarComponent, FooterComponent, PageRibbonComponent, ActiveMenuDirective, ErrorComponent } from './layouts';

@NgModule({
    imports: [
        BrowserModule,
        Ng2Webstorage.forRoot({ prefix: 'jhigtw', separator: '-' }),
        NgJhipsterModule.forRoot({
            // set below to true to make alerts look like toast
            alertAsToast: false,
            alertTimeout: 5000,
            i18nEnabled: true,
            defaultI18nLang: 'en'
        }),
        Gtw01AlnsNeuedaUrlShortenerSharedModule.forRoot(),
        Gtw01AlnsNeuedaUrlShortenerCoreModule,
        Gtw01AlnsNeuedaUrlShortenerHomeModule,
        Gtw01AlnsNeuedaUrlShortenerEntityModule,
        Gtw01AlnsNeuedaUrlShortenerAppRoutingModule
    ],
    declarations: [JhigtwMainComponent, NavbarComponent, ErrorComponent, PageRibbonComponent, ActiveMenuDirective, FooterComponent],
    providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: ErrorHandlerInterceptor,
            multi: true
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: NotificationInterceptor,
            multi: true
        }
    ], 
    bootstrap: [JhigtwMainComponent]
})
export class Gtw01AlnsNeuedaUrlShortenerAppModule {
    constructor(private dpConfig: NgbDatepickerConfig) {
        this.dpConfig.minDate = { year: moment().year() - 100, month: 1, day: 1 };
    }
}
