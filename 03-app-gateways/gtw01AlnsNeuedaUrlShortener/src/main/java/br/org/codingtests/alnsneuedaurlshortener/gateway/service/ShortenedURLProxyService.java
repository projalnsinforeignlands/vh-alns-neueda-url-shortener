package br.org.codingtests.alnsneuedaurlshortener.gateway.service;

import br.org.codingtests.alnsneuedaurlshortener.gateway.feignclients.dtos.StatisticsInfoDTO;

/**
 * Service Interface for managing interactions with MS03-URLManagement MicroService.
 */
public interface ShortenedURLProxyService {


    String searchLongURLByEncodedKey(String pShorterURL, StatisticsInfoDTO statisticsInfoDTO);

}




