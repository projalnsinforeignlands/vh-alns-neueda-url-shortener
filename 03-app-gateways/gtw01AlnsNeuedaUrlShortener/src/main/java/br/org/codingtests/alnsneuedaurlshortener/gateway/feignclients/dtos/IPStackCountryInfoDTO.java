package br.org.codingtests.alnsneuedaurlshortener.gateway.feignclients.dtos;

import java.io.Serializable;
import java.util.List;

/**
 * A DTO for the IPStack Info.
 */
public class IPStackCountryInfoDTO implements Serializable {

    private String ip = "177.79.30.111";
    private String type = "ipv4";
    private String continent_code = "SA";
    private String continent_name = "South America";
    private String country_code = "BR";
    private String country_name = "Brazil";
    private String region_code = "DF";
    private String region_name = "Federal District";
    private String city = "Bras\u00edlia";
    private String zip = "70640";
    private Double latitude = -15.7792;
    private Double longitude = -47.9341;
    private Location location;

    public IPStackCountryInfoDTO() {

    }


    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getContinent_code() {
        return continent_code;
    }

    public void setContinent_code(String continent_code) {
        this.continent_code = continent_code;
    }

    public String getContinent_name() {
        return continent_name;
    }

    public void setContinent_name(String continent_name) {
        this.continent_name = continent_name;
    }

    public String getCountry_code() {
        return country_code;
    }

    public void setCountry_code(String country_code) {
        this.country_code = country_code;
    }

    public String getCountry_name() {
        return country_name;
    }

    public void setCountry_name(String country_name) {
        this.country_name = country_name;
    }

    public String getRegion_code() {
        return region_code;
    }

    public void setRegion_code(String region_code) {
        this.region_code = region_code;
    }

    public String getRegion_name() {
        return region_name;
    }

    public void setRegion_name(String region_name) {
        this.region_name = region_name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }


    @Override
    public String toString() {
        return "IPStackCountryInfoDTO{" +
            "ip='" + ip + '\'' +
            ", type='" + type + '\'' +
            ", continent_code='" + continent_code + '\'' +
            ", continent_name='" + continent_name + '\'' +
            ", country_code='" + country_code + '\'' +
            ", country_name='" + country_name + '\'' +
            ", region_code='" + region_code + '\'' +
            ", region_name='" + region_name + '\'' +
            ", city='" + city + '\'' +
            ", zip='" + zip + '\'' +
            ", latitude=" + latitude +
            ", longitude=" + longitude +
            ", location=" + location +
            '}';
    }
}


class Location {

    private Long geoname_id = 3469058L;
    private String capital = "Brasilia";
    private List<Language> languages;
    private String country_flag = "http://assets.ipstack.com/flags/br.svg";
    private String country_flag_emoji = "\ud83c\udde7\ud83c\uddf7";
    private String country_flag_emoji_unicode = "U+1F1E7 U+1F1F7";
    private String calling_code = "55";
    private Boolean is_eu = false;


    public Location() {

    }

    public Long getGeoname_id() {
        return geoname_id;
    }

    public void setGeoname_id(Long geoname_id) {
        this.geoname_id = geoname_id;
    }

    public String getCapital() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }

    public List<Language> getLanguages() {
        return languages;
    }

    public void setLanguages(List<Language> languages) {
        this.languages = languages;
    }

    public String getCountry_flag() {
        return country_flag;
    }

    public void setCountry_flag(String country_flag) {
        this.country_flag = country_flag;
    }

    public String getCountry_flag_emoji() {
        return country_flag_emoji;
    }

    public void setCountry_flag_emoji(String country_flag_emoji) {
        this.country_flag_emoji = country_flag_emoji;
    }

    public String getCountry_flag_emoji_unicode() {
        return country_flag_emoji_unicode;
    }

    public void setCountry_flag_emoji_unicode(String country_flag_emoji_unicode) {
        this.country_flag_emoji_unicode = country_flag_emoji_unicode;
    }

    public String getCalling_code() {
        return calling_code;
    }

    public void setCalling_code(String calling_code) {
        this.calling_code = calling_code;
    }

    public Boolean getIs_eu() {
        return is_eu;
    }

    public void setIs_eu(Boolean is_eu) {
        this.is_eu = is_eu;
    }


    @Override
    public String toString() {
        return "Location{" +
            "geoname_id=" + geoname_id +
            ", capital='" + capital + '\'' +
            ", languages=" + languages +
            ", country_flag='" + country_flag + '\'' +
            ", country_flag_emoji='" + country_flag_emoji + '\'' +
            ", country_flag_emoji_unicode='" + country_flag_emoji_unicode + '\'' +
            ", calling_code='" + calling_code + '\'' +
            ", is_eu=" + is_eu +
            '}';
    }
}


class Language {

    private String code = "pt";
    private String name = "Portuguese";
    private String nativeName = "Portugues";

    public Language() {
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNativeName() {
        return nativeName;
    }

    public void setNativeName(String nativeName) {
        this.nativeName = nativeName;
    }


    @Override
    public String toString() {
        return "Language{" +
            "code='" + code + '\'' +
            ", name='" + name + '\'' +
            ", nativeName='" + nativeName + '\'' +
            '}';
    }
}
