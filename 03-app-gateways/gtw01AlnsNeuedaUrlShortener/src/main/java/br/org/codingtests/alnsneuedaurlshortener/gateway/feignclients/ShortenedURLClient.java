package br.org.codingtests.alnsneuedaurlshortener.gateway.feignclients;


import br.org.codingtests.alnsneuedaurlshortener.gateway.feignclients.dtos.ShortenedURLDTO;
import br.org.codingtests.alnsneuedaurlshortener.gateway.feignclients.dtos.StatisticsInfoDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(serviceId = "ms03urlmanagement")
public interface ShortenedURLClient {

    @RequestMapping(method = RequestMethod.POST, value = "/api/shortened-urls/_search-by-encodedkey/{pEncodedKey}")
    ShortenedURLDTO searchLongURLByEncodedKey(@PathVariable("pEncodedKey") String pEncodedKey,
                                              @RequestBody StatisticsInfoDTO pStatisticsInfoDTO);

}
