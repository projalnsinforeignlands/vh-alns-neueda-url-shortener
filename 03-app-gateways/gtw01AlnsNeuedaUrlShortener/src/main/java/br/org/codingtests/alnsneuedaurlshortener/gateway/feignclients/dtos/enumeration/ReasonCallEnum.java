package br.org.codingtests.alnsneuedaurlshortener.gateway.feignclients.dtos.enumeration;

/**
 * The ReasonCallEnum enumeration.
 */
public enum ReasonCallEnum {
    TO_REDIRECT, JUST_DECODING
}
