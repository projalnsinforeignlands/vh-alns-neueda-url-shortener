package br.org.codingtests.alnsneuedaurlshortener.gateway.feignclients.dtos.enumeration;

/**
 * The AlgorithmShorteningURLEnum enumeration.
 */
public enum AlgorithmShorteningURLEnum {
    BASE64_UUID,
    GOOGLE_API
}
