package br.org.codingtests.alnsneuedaurlshortener.gateway.feignclients;


import br.org.codingtests.alnsneuedaurlshortener.gateway.feignclients.dtos.IPStackCountryInfoDTO;
import org.springframework.stereotype.Service;


@Service
public class IPStackCountryInfoFallbackClientImpl implements IPStackCountryInfoClient {

    public IPStackCountryInfoDTO searchCountryInfoByIP(String pIPAddressCaller,
                                                       String pChaveCadastroIPStack) {


        IPStackCountryInfoDTO ipStackCountryInfoDTO = null;

        ipStackCountryInfoDTO = new IPStackCountryInfoDTO();

        ipStackCountryInfoDTO.setCountry_code("LH");

        return ipStackCountryInfoDTO;
    }

}
