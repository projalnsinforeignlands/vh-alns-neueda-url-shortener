package br.org.codingtests.alnsneuedaurlshortener.gateway.feignclients;


import br.org.codingtests.alnsneuedaurlshortener.gateway.feignclients.dtos.IPStackCountryInfoDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name = "IPStackCountryInfoClient", fallback = IPStackCountryInfoFallbackClientImpl.class, url = "http://api.ipstack.com")
public interface IPStackCountryInfoClient {

    @RequestMapping(method = RequestMethod.GET, value = "/{pIPAddressCaller}?access_key={pChaveCadastroIPStack}")
    IPStackCountryInfoDTO searchCountryInfoByIP(@PathVariable("pIPAddressCaller") String pIPAddressCaller,
                                                @PathVariable("pChaveCadastroIPStack") String pChaveCadastroIPStack);

}
