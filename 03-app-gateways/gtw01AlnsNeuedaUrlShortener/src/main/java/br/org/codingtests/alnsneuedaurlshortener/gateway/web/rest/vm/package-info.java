/**
 * View Models used by Spring MVC REST controllers.
 */
package br.org.codingtests.alnsneuedaurlshortener.gateway.web.rest.vm;
