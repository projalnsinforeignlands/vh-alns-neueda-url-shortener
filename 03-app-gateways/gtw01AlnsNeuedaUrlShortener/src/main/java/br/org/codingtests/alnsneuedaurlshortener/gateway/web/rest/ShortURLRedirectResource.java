package br.org.codingtests.alnsneuedaurlshortener.gateway.web.rest;

import br.org.codingtests.alnsneuedaurlshortener.gateway.feignclients.IPStackCountryInfoClient;
import br.org.codingtests.alnsneuedaurlshortener.gateway.feignclients.dtos.IPStackCountryInfoDTO;
import br.org.codingtests.alnsneuedaurlshortener.gateway.feignclients.dtos.StatisticsInfoDTO;
import br.org.codingtests.alnsneuedaurlshortener.gateway.feignclients.dtos.enumeration.ReasonCallEnum;
import br.org.codingtests.alnsneuedaurlshortener.gateway.service.ShortenedURLProxyService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;

/**
 * REST controller for managing Redirection to Long URL from Short URL.
 */
@RestController
@RequestMapping("/short")
public class ShortURLRedirectResource {

    private final Logger log = LoggerFactory.getLogger(ShortURLRedirectResource.class);

    private static final String NUM_KEY_IPSTACK_REGISTRAR = "4efebde956e3795b83697d00aa5ad7fd";


    private ShortenedURLProxyService shortenedURLProxyService;

    private IPStackCountryInfoClient ipStackCountryInfoClient;


    public ShortURLRedirectResource(ShortenedURLProxyService pShortenedURLProxyService,
                                    IPStackCountryInfoClient pIPStackCountryInfoClient) {

        this.shortenedURLProxyService = pShortenedURLProxyService;
        this.ipStackCountryInfoClient = pIPStackCountryInfoClient;
    }


    /**
     * Redirect a short URL to original URL.
     *
     * @param pShorterURL short URL unique key.
     */
    @ApiOperation(value = "Redirect a short URL to original URL", nickname = "redirectURL", notes = "Redirect a short URL to original URL")
    @ApiResponses(value = {
        @ApiResponse(code = 201, message = "URL Created"),
        @ApiResponse(code = 301, message = "Redirect to original URL"),
        @ApiResponse(code = 404, message = "The key was not found") })
    @RequestMapping(value = "/{pShorterURL}", method = RequestMethod.GET)
    public ResponseEntity<Void> redirectURL(@ApiParam(value = "Short URL key", required = true)
                                            @PathVariable("pShorterURL") String pShorterURL,
                                            HttpServletRequest pHttpRequest) {

        ResponseEntity<Void> responseResult = null;
        String fullCompleteURL = null;
        StatisticsInfoDTO statisticsInfoDTO = null;

        statisticsInfoDTO = collectCurrentRequestInfo(pShorterURL, pHttpRequest);

        fullCompleteURL = this.shortenedURLProxyService.searchLongURLByEncodedKey(pShorterURL, statisticsInfoDTO);

        if (StringUtils.isEmpty(fullCompleteURL)) {
            fullCompleteURL = "http://localhost:8080/404.html";
        }

        responseResult = ResponseEntity.status(HttpStatus.MOVED_PERMANENTLY)
                                       .header(HttpHeaders.LOCATION, fullCompleteURL)
                                       .build();

        return responseResult;
    }

    private StatisticsInfoDTO collectCurrentRequestInfo(String pShorterURL, HttpServletRequest pHttpRequest) {

        StatisticsInfoDTO statisticsInfoDTO = null;

        statisticsInfoDTO = new StatisticsInfoDTO();

        statisticsInfoDTO.setDateAccess(LocalDateTime.now());
        statisticsInfoDTO.setReasonCallDoneType(ReasonCallEnum.TO_REDIRECT);

        if (pHttpRequest.getHeader("X-Forwarded-For") != null){
            statisticsInfoDTO.setNumberIPAddressCaller(pHttpRequest.getHeader("X-Forwarded-For"));
        } else {
            statisticsInfoDTO.setNumberIPAddressCaller(pHttpRequest.getRemoteAddr());
        }

        statisticsInfoDTO.setCountryCallerCode(tryToGetCountryInfo(pHttpRequest, statisticsInfoDTO.getNumberIPAddressCaller()));

        return statisticsInfoDTO;
    }

    private String tryToGetCountryInfo(HttpServletRequest pHttpRequest, String pIPAddressTarget) {

        String strResult = "LH";

        IPStackCountryInfoDTO ipStackCountryInfoDTO = null;

        ipStackCountryInfoDTO = this.ipStackCountryInfoClient.searchCountryInfoByIP(pIPAddressTarget, NUM_KEY_IPSTACK_REGISTRAR);

        log.debug("==> " + ipStackCountryInfoDTO);

        if (ipStackCountryInfoDTO != null && !StringUtils.isEmpty(ipStackCountryInfoDTO.getCountry_code())) {

            strResult = ipStackCountryInfoDTO.getCountry_code();

        }

        return strResult;
    }


}
