package br.org.codingtests.alnsneuedaurlshortener.gateway.service.impl;

import br.org.codingtests.alnsneuedaurlshortener.gateway.feignclients.ShortenedURLClient;
import br.org.codingtests.alnsneuedaurlshortener.gateway.feignclients.dtos.ShortenedURLDTO;
import br.org.codingtests.alnsneuedaurlshortener.gateway.feignclients.dtos.StatisticsInfoDTO;
import br.org.codingtests.alnsneuedaurlshortener.gateway.service.ShortenedURLProxyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 * Service Implementation for managing interactions with MS03-URLManagement MicroService.
 */
@Service
public class ShortenedURLProxyServiceImpl implements ShortenedURLProxyService {

    private ShortenedURLClient shortenerURLClient;

    @Autowired
    public ShortenedURLProxyServiceImpl(ShortenedURLClient pShortenerURLClient) {

        this.shortenerURLClient = pShortenerURLClient;

    }


    @Override
    public String searchLongURLByEncodedKey(String pShorterURL, StatisticsInfoDTO pStatisticsInfoDTO) {

        String strResult = null;
        ShortenedURLDTO shortenedURLDTO = null;

        shortenedURLDTO = shortenerURLClient.searchLongURLByEncodedKey(pShorterURL, pStatisticsInfoDTO);

        if (shortenedURLDTO != null && !StringUtils.isEmpty(shortenedURLDTO.getDescriptionLongURL())) {

            strResult = shortenedURLDTO.getDescriptionLongURL();

        }


        return strResult;
    }
}




