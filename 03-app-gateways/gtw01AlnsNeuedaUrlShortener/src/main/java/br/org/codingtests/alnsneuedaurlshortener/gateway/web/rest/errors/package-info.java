/**
 * Specific errors used with Zalando's "problem-spring-web" library.
 *
 * More information on https://github.com/zalando/problem-spring-web
 */
package br.org.codingtests.alnsneuedaurlshortener.gateway.web.rest.errors;
