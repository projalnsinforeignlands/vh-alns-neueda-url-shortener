/* tslint:disable max-line-length */
import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { take, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { StatisticsInfoService } from 'app/entities/ms03UrlManagement/statistics-info/statistics-info.service';
import { IStatisticsInfo, StatisticsInfo, ReasonCallEnum } from 'app/shared/model/ms03UrlManagement/statistics-info.model';

describe('Service Tests', () => {
    describe('StatisticsInfo Service', () => {
        let injector: TestBed;
        let service: StatisticsInfoService;
        let httpMock: HttpTestingController;
        let elemDefault: IStatisticsInfo;
        let currentDate: moment.Moment;
        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [HttpClientTestingModule]
            });
            injector = getTestBed();
            service = injector.get(StatisticsInfoService);
            httpMock = injector.get(HttpTestingController);
            currentDate = moment();

            elemDefault = new StatisticsInfo(0, ReasonCallEnum.TO_REDIRECT, currentDate, 'AAAAAAA', 'AAAAAAA');
        });

        describe('Service methods', async () => {
            it('should find an element', async () => {
                const returnedFromService = Object.assign(
                    {
                        dateAccess: currentDate.format(DATE_FORMAT)
                    },
                    elemDefault
                );
                service
                    .find(123)
                    .pipe(take(1))
                    .subscribe(resp => expect(resp).toMatchObject({ body: elemDefault }));

                const req = httpMock.expectOne({ method: 'GET' });
                req.flush(JSON.stringify(returnedFromService));
            });

            it('should create a StatisticsInfo', async () => {
                const returnedFromService = Object.assign(
                    {
                        id: 0,
                        dateAccess: currentDate.format(DATE_FORMAT)
                    },
                    elemDefault
                );
                const expected = Object.assign(
                    {
                        dateAccess: currentDate
                    },
                    returnedFromService
                );
                service
                    .create(new StatisticsInfo(null))
                    .pipe(take(1))
                    .subscribe(resp => expect(resp).toMatchObject({ body: expected }));
                const req = httpMock.expectOne({ method: 'POST' });
                req.flush(JSON.stringify(returnedFromService));
            });

            it('should update a StatisticsInfo', async () => {
                const returnedFromService = Object.assign(
                    {
                        reasonCallDoneType: 'BBBBBB',
                        dateAccess: currentDate.format(DATE_FORMAT),
                        numberIPAddressCaller: 'BBBBBB',
                        countryCallerCode: 'BBBBBB'
                    },
                    elemDefault
                );

                const expected = Object.assign(
                    {
                        dateAccess: currentDate
                    },
                    returnedFromService
                );
                service
                    .update(expected)
                    .pipe(take(1))
                    .subscribe(resp => expect(resp).toMatchObject({ body: expected }));
                const req = httpMock.expectOne({ method: 'PUT' });
                req.flush(JSON.stringify(returnedFromService));
            });

            it('should return a list of StatisticsInfo', async () => {
                const returnedFromService = Object.assign(
                    {
                        reasonCallDoneType: 'BBBBBB',
                        dateAccess: currentDate.format(DATE_FORMAT),
                        numberIPAddressCaller: 'BBBBBB',
                        countryCallerCode: 'BBBBBB'
                    },
                    elemDefault
                );
                const expected = Object.assign(
                    {
                        dateAccess: currentDate
                    },
                    returnedFromService
                );
                service
                    .query(expected)
                    .pipe(
                        take(1),
                        map(resp => resp.body)
                    )
                    .subscribe(body => expect(body).toContainEqual(expected));
                const req = httpMock.expectOne({ method: 'GET' });
                req.flush(JSON.stringify([returnedFromService]));
                httpMock.verify();
            });

            it('should delete a StatisticsInfo', async () => {
                const rxPromise = service.delete(123).subscribe(resp => expect(resp.ok));

                const req = httpMock.expectOne({ method: 'DELETE' });
                req.flush({ status: 200 });
            });
        });

        afterEach(() => {
            httpMock.verify();
        });
    });
});
