/* tslint:disable max-line-length */
import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { take, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { ShortenedURLService } from 'app/entities/ms03UrlManagement/shortened-url/shortened-url.service';
import { IShortenedURL, ShortenedURL, AlgorithmShorteningURLEnum } from 'app/shared/model/ms03UrlManagement/shortened-url.model';

describe('Service Tests', () => {
    describe('ShortenedURL Service', () => {
        let injector: TestBed;
        let service: ShortenedURLService;
        let httpMock: HttpTestingController;
        let elemDefault: IShortenedURL;
        let currentDate: moment.Moment;
        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [HttpClientTestingModule]
            });
            injector = getTestBed();
            service = injector.get(ShortenedURLService);
            httpMock = injector.get(HttpTestingController);
            currentDate = moment();

            elemDefault = new ShortenedURL(
                0,
                0,
                'AAAAAAA',
                'AAAAAAA',
                AlgorithmShorteningURLEnum.BASE64_UUID,
                currentDate,
                currentDate,
                false,
                "http://localhost:8080/xptoytr",
                0,
                0
            );
        });

        describe('Service methods', async () => {
            it('should find an element', async () => {
                const returnedFromService = Object.assign(
                    {
                        dateEncoding: currentDate.format(DATE_FORMAT),
                        dateLastAccess: currentDate.format(DATE_FORMAT)
                    },
                    elemDefault
                );
                service
                    .find(123)
                    .pipe(take(1))
                    .subscribe(resp => expect(resp).toMatchObject({ body: elemDefault }));

                const req = httpMock.expectOne({ method: 'GET' });
                req.flush(JSON.stringify(returnedFromService));
            });

            it('should create a ShortenedURL', async () => {
                const returnedFromService = Object.assign(
                    {
                        id: 0,
                        dateEncoding: currentDate.format(DATE_FORMAT),
                        dateLastAccess: currentDate.format(DATE_FORMAT)
                    },
                    elemDefault
                );
                const expected = Object.assign(
                    {
                        dateEncoding: currentDate,
                        dateLastAccess: currentDate
                    },
                    returnedFromService
                );
                service
                    .create(new ShortenedURL(null))
                    .pipe(take(1))
                    .subscribe(resp => expect(resp).toMatchObject({ body: expected }));
                const req = httpMock.expectOne({ method: 'POST' });
                req.flush(JSON.stringify(returnedFromService));
            });

            it('should update a ShortenedURL', async () => {
                const returnedFromService = Object.assign(
                    {
                        userId: 1,
                        descriptionLongURL: 'BBBBBB',
                        descriptionShortenedURL: 'BBBBBB',
                        generationAlgorithmType: 'BBBBBB',
                        dateEncoding: currentDate.format(DATE_FORMAT),
                        dateLastAccess: currentDate.format(DATE_FORMAT),
                        isActive: true,
                        qtyDecodingRequestsByAPI: 1,
                        qtyCallsForRedirecting: 1
                    },
                    elemDefault
                );

                const expected = Object.assign(
                    {
                        dateEncoding: currentDate,
                        dateLastAccess: currentDate
                    },
                    returnedFromService
                );
                service
                    .update(expected)
                    .pipe(take(1))
                    .subscribe(resp => expect(resp).toMatchObject({ body: expected }));
                const req = httpMock.expectOne({ method: 'PUT' });
                req.flush(JSON.stringify(returnedFromService));
            });

            it('should return a list of ShortenedURL', async () => {
                const returnedFromService = Object.assign(
                    {
                        userId: 1,
                        descriptionLongURL: 'BBBBBB',
                        descriptionShortenedURL: 'BBBBBB',
                        generationAlgorithmType: 'BBBBBB',
                        dateEncoding: currentDate.format(DATE_FORMAT),
                        dateLastAccess: currentDate.format(DATE_FORMAT),
                        isActive: true,
                        qtyDecodingRequestsByAPI: 1,
                        qtyCallsForRedirecting: 1
                    },
                    elemDefault
                );
                const expected = Object.assign(
                    {
                        dateEncoding: currentDate,
                        dateLastAccess: currentDate
                    },
                    returnedFromService
                );
                service
                    .query(expected)
                    .pipe(
                        take(1),
                        map(resp => resp.body)
                    )
                    .subscribe(body => expect(body).toContainEqual(expected));
                const req = httpMock.expectOne({ method: 'GET' });
                req.flush(JSON.stringify([returnedFromService]));
                httpMock.verify();
            });

            it('should delete a ShortenedURL', async () => {
                const rxPromise = service.delete(123).subscribe(resp => expect(resp.ok));

                const req = httpMock.expectOne({ method: 'DELETE' });
                req.flush({ status: 200 });
            });
        });

        afterEach(() => {
            httpMock.verify();
        });
    });
});
