#!/bin/bash

clear

cd docker-compose

echo "Stopping containers for MicroServices with Docker Compose..."
docker-compose stop app-ms01-users app-ms02-textencoder app-ms03-urlmanagement 


echo "\n\n\n\n\n\n\n Containers for MicroServices Stopped successfully! \n\n\n\n\n\n"
 