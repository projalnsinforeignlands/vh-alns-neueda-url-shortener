#!/bin/bash

clear

cd docker-compose

echo "Stopping containers App-Infra-Basica with Docker Compose..."
docker-compose stop app-infra-phpmyadmin app-infra-eureka-registry db-mysql-server db-mongodb-server

echo "\n\n\n\n\n\n\n Only Basic Infra Stopped successfully! \n\n\n\n\n\n"
 
