# Project VH-ALNS-Neueda-ShortenerURL

## Motivation
This is project started when a receive a coding challenge for a position in an awesome company in Europe. 

The initial goal was to create a solution to make long URLs shorter, based on the statement below:

```
Most of us are familiar with seeing URLs like bit.ly or t.co on our Twitter or Facebook feeds. These are examples of shortened URLs, which are a short alias or pointer to a longer page link. For example, I can send you the shortened URL http://bit.ly/SaaYw5 that will forward you to a very long Google URL with search results on how to iron a shirt.
```

At a high level, the URL shortener works by taking a long URL and applying a hashing algorithm to spit out a shorter version of the URL and stores them in a database for later lookup.

- long URL -> short URL
- short URL -> long URL

# The Solution that I Implemented

The idea was to have a Sample Shortener URL web application to demonstrate how to build web applications using Java + Spring Boot. 

But as soon as I started to implement, I thought about spurring this project, so as to make it a little more challenging. So I used an approach based on the MicroServices architecture, using the following design, as shown in the figure below, which shows a high-level overview of the solution:

#### URL Shortener Architecture Overview

![](https://bitbucket.org/projalnsinforeignlands/vh-alns-neueda-url-shortener/raw/master/03-app-gateways/gtw01AlnsNeuedaUrlShortener/src/main/webapp/content/images/for-docs/architecture-overview-01.png)

* The idea is to just simulate an environment with MicroServices, even though the problem is quite simple and you would not need it. But it was part of one more challenge, when making work all together the components of MicroService Architecture in a containerized environment;

* I organized the solution in 3 modules, in which each one is a MicroService focused on providing specific functionalities for the larger solution. These MicroServices are:

 - **MS01- Users**: 
    - Provides user data maintenance (in a very simplified way). Stores data in a specific MySQL Database;

 - **MS02-Text Encoders**: 
 
   - This is a Spring Boot App responsible for generating 7 letters unique keys;
   - The key generation process is asynchronous and scheduled by Spring Scheduler. Encoded keys are generated every 15 seconds and storing them in a collection of documents in MongoDB. These keys are encoded using two types of algorithms:
      - One that calls the Google URL shortening API externally ;
      - One that internally calculates a random Strings using the Java UUID class.
    - Another important note is that the scheduler only generates more keys if the numbers of available keys are less than **chunk.size * 1,25**.

 - **MS03-UrlManagement**: 
 
    - Provides the maintenance of URLs encoded, per User, and their statistics. It interacts with the other MicroServices, calling their APIs. Stores data in a specific MySQL Database;

 - **GTW01-AlnsNeuedaShortenedURLApp**:
     - This is a Spring Boot App that is a "Gateway", serving as a router to others MicroServices;
	 - The Frontend Angular 7 App is here. Access it at: http://localhost:8080 :
![](https://bitbucket.org/projalnsinforeignlands/vh-alns-neueda-url-shortener/raw/master/03-app-gateways/gtw01AlnsNeuedaUrlShortener/src/main/webapp/content/images/for-docs/gtw01-main-app-screenshot-01.png =200x200)
	 - The URL for redirecting to a longer URL based on Shorter URL is here, in the following pattern:
	    - http://localhost:8080/short/{key-with-7-chars}

     - The APIs from all 3 MicroServices plus from the Gateway are documented using **Swagger** and can be accessed [here](http://localhost:8080/swagger-ui.html "http://localhost:8080/swagger-ui.html") or through the menu option "Utilities => Swagger UI" at the navigation bar on the main UI screen:
![](https://bitbucket.org/projalnsinforeignlands/vh-alns-neueda-url-shortener/raw/master/03-app-gateways/gtw01AlnsNeuedaUrlShortener/src/main/webapp/content/images/for-docs/swagger-ui-screenshot-01.png)

## How to Execute the Application? 
To deploy and execute this application in your machine, do the following:

1. Make sure you have the following requirements installed:
  - Linux (Preferably, Ubuntu 18.04);
  - Git;
  - Java 8 (JDK 8);
  - Apache Maven 3.2.1+;
  - Docker v-18.09+;
  - Docker Compose v-1.23+.
1. Git clone this project: 
``` 
  git clone https://scout23DF@bitbucket.org/projalnsinforeignlands/vh-alns-neueda-url-shortener.git
```  
1. Enter the new project folder: ** cd vh-alns-neueda-url-shortener **;
1. Run this bash script to build the projects, download the Docker images, build the containers and startup them:
   - ** sh all-build-and-deploy.sh **
     - If this is the first time you are running this command, this will take a while, so go have a coffee, take a break to rest and then read down the technical details of this solution.
1.  If all was successfully, open your browser and point it to: 
   - http://localhost:8080
   - You should see some data sample already inserted using a init script called when the MySQL Docker container was being started;
   - Click on any User on left panel. See at the center panel if show his/her Shortened URL;
   - Click on any Shortened URL to see If it has some statistical info, at right panel;
   - Make your own tests, creating users and their Shortened URL. Then click on the "Redirect To" button on each Shortened URL to see If it's longer URL is open in other borwser tab. Then see If the Statistics Info are updated.
   - If all these steps worked fine for you, so many things happened under the covers. Know them on next sections.
1. If you want:
   - To stop and destroy all Docker Containers, run:
      - ** sh all-destroy-containers.sh **
   - Only to stop the Gateway app and its container, run:
      - ** sh only-stop-4-gtw01.sh **
   - Only to stop the MicroServices apps and their conteiners, run:
      - ** sh only-stop-3-msrvc.sh **   
   - Only to stop the infrasctructure components and theirs containers, run:
      - ** sh only-stop-1-infra-basica.sh **
   - There are others bash scripts at the project root, which each name is self-explanatory.

## Technological Details

## Components

# Spring Microservices

> Example of a microservice architecture using Spring Cloud

## Overview

The architecture is composed by four services:

- `discovery-service`: Service Discovery Server created with **Eureka**
- `api-gateway`: API Gateway created with **Zuul** that uses the `discovery-service` to send the requests to the services. It uses **Ribbon** as Load Balancer

#### [UNDER CONSTRUCTION]


# How to use the application? 

#### [UNDER CONSTRUCTION]
