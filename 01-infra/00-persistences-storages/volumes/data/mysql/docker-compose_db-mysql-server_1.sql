-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: docker-compose_db-mysql-server_1
-- Generation Time: 12-Mar-2019 às 23:59
-- Versão do servidor: 5.7.20
-- versão do PHP: 7.2.14

SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_alns_neueda_urlshortener`
--
DROP DATABASE IF EXISTS `db_alns_neueda_urlshortener`;
CREATE DATABASE IF NOT EXISTS `db_alns_neueda_urlshortener` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE db_alns_neueda_urlshortener;
--
-- Database: `db_ms01_users`
--
DROP DATABASE IF EXISTS `db_ms01_users`;
CREATE DATABASE IF NOT EXISTS `db_ms01_users` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE db_ms01_users;

-- --------------------------------------------------------

--
-- Estrutura da tabela `databasechangelog`
--

CREATE TABLE `databasechangelog` (
  `ID` varchar(255) NOT NULL,
  `AUTHOR` varchar(255) NOT NULL,
  `FILENAME` varchar(255) NOT NULL,
  `DATEEXECUTED` datetime NOT NULL,
  `ORDEREXECUTED` int(11) NOT NULL,
  `EXECTYPE` varchar(10) NOT NULL,
  `MD5SUM` varchar(35) DEFAULT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `COMMENTS` varchar(255) DEFAULT NULL,
  `TAG` varchar(255) DEFAULT NULL,
  `LIQUIBASE` varchar(20) DEFAULT NULL,
  `CONTEXTS` varchar(255) DEFAULT NULL,
  `LABELS` varchar(255) DEFAULT NULL,
  `DEPLOYMENT_ID` varchar(10) DEFAULT NULL
) ;

--
-- Extraindo dados da tabela `databasechangelog`
--

INSERT INTO `databasechangelog` (`ID`, `AUTHOR`, `FILENAME`, `DATEEXECUTED`, `ORDEREXECUTED`, `EXECTYPE`, `MD5SUM`, `DESCRIPTION`, `COMMENTS`, `TAG`, `LIQUIBASE`, `CONTEXTS`, `LABELS`, `DEPLOYMENT_ID`) VALUES
('00000000000001', 'jhipster', 'config/liquibase/changelog/00000000000000_initial_schema.xml', '2019-03-11 23:28:49', 1, 'EXECUTED', '7:b977cf863d1f2a294378fd91352d78e5', 'createTable tableName=jhims1_persistent_audit_event; createTable tableName=jhims1_persistent_audit_evt_data; addPrimaryKey tableName=jhims1_persistent_audit_evt_data; createIndex indexName=idx_persistent_audit_event, tableName=jhims1_persistent_au...', '', NULL, '3.5.4', NULL, NULL, '2346928613'),
('20190311023816-1', 'jhipster', 'config/liquibase/changelog/20190311023816_added_entity_UserExtraInfo.xml', '2019-03-11 23:28:49', 2, 'EXECUTED', '7:68859465040150a6e0576ce7f3c6030a', 'createTable tableName=user_extra_info', '', NULL, '3.5.4', NULL, NULL, '2346928613'),
('1552434350404-1', 'andre.nascimento (generated)', 'config/liquibase/changelog/20190312204429_added_columns_user_to_userextrainfo.xml', '2019-03-12 23:55:44', 3, 'EXECUTED', '7:6792e272d3614f84d24fc4091325b17a', 'addColumn tableName=user_extra_info', '', NULL, '3.5.4', NULL, NULL, '2434944011'),
('1552434350404-2', 'andre.nascimento (generated)', 'config/liquibase/changelog/20190312204429_added_columns_user_to_userextrainfo.xml', '2019-03-12 23:55:44', 4, 'EXECUTED', '7:2b206351a430001576b1aa08e2b1ffe2', 'addColumn tableName=user_extra_info', '', NULL, '3.5.4', NULL, NULL, '2434944011'),
('1552434350404-3', 'andre.nascimento (generated)', 'config/liquibase/changelog/20190312204429_added_columns_user_to_userextrainfo.xml', '2019-03-12 23:55:44', 5, 'EXECUTED', '7:94ba2d774a614458dd408bba27e40b7a', 'addUniqueConstraint constraintName=UC_USER_EXTRA_INFODS_USER_LOGIN_UAA_COL, tableName=user_extra_info', '', NULL, '3.5.4', NULL, NULL, '2434944011'),
('1552434350404-4', 'andre.nascimento (generated)', 'config/liquibase/changelog/20190312204429_added_columns_user_to_userextrainfo.xml', '2019-03-12 23:55:44', 6, 'EXECUTED', '7:4047306dd855f458802eb546c78f6fff', 'addUniqueConstraint constraintName=UC_USER_EXTRA_INFOUSER_UAA_ID_COL, tableName=user_extra_info', '', NULL, '3.5.4', NULL, NULL, '2434944011');

-- --------------------------------------------------------

--
-- Estrutura da tabela `databasechangeloglock`
--

CREATE TABLE `databasechangeloglock` (
  `ID` int(11) NOT NULL,
  `LOCKED` bit(1) NOT NULL,
  `LOCKGRANTED` datetime DEFAULT NULL,
  `LOCKEDBY` varchar(255) DEFAULT NULL
) ;

--
-- Extraindo dados da tabela `databasechangeloglock`
--

INSERT INTO `databasechangeloglock` (`ID`, `LOCKED`, `LOCKGRANTED`, `LOCKEDBY`) VALUES
(1, b'0', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `jhims1_persistent_audit_event`
--

CREATE TABLE `jhims1_persistent_audit_event` (
  `event_id` bigint(20) NOT NULL,
  `principal` varchar(50) NOT NULL,
  `event_date` timestamp NULL DEFAULT NULL,
  `event_type` varchar(255) DEFAULT NULL
) ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `jhims1_persistent_audit_evt_data`
--

CREATE TABLE `jhims1_persistent_audit_evt_data` (
  `event_id` bigint(20) NOT NULL,
  `name` varchar(150) NOT NULL,
  `value` varchar(255) DEFAULT NULL
) ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `user_extra_info`
--

CREATE TABLE `user_extra_info` (
  `id` bigint(20) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `work_email` varchar(255) NOT NULL,
  `phone_number` varchar(255) NOT NULL,
  `address_line_1` varchar(255) NOT NULL,
  `address_line_2` varchar(255) DEFAULT NULL,
  `city` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `gross_salary` decimal(10,2) NOT NULL,
  `image` longblob,
  `image_content_type` varchar(255) DEFAULT NULL,
  `ds_user_login_uaa` varchar(255) NOT NULL,
  `user_uaa_id` bigint(20) NOT NULL
) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `databasechangeloglock`
--
ALTER TABLE `databasechangeloglock`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `jhims1_persistent_audit_event`
--
ALTER TABLE `jhims1_persistent_audit_event`
  ADD PRIMARY KEY (`event_id`),
  ADD KEY `idx_persistent_audit_event` (`principal`,`event_date`);

--
-- Indexes for table `jhims1_persistent_audit_evt_data`
--
ALTER TABLE `jhims1_persistent_audit_evt_data`
  ADD PRIMARY KEY (`event_id`,`name`),
  ADD KEY `idx_persistent_audit_evt_data` (`event_id`);

--
-- Indexes for table `user_extra_info`
--
ALTER TABLE `user_extra_info`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UC_USER_EXTRA_INFODS_USER_LOGIN_UAA_COL` (`ds_user_login_uaa`),
  ADD UNIQUE KEY `UC_USER_EXTRA_INFOUSER_UAA_ID_COL` (`user_uaa_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `jhims1_persistent_audit_event`
--
ALTER TABLE `jhims1_persistent_audit_event`
  MODIFY `event_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_extra_info`
--
ALTER TABLE `user_extra_info`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `jhims1_persistent_audit_evt_data`
--
ALTER TABLE `jhims1_persistent_audit_evt_data`
  ADD CONSTRAINT `fk_evt_pers_audit_evt_data` FOREIGN KEY (`event_id`) REFERENCES `jhims1_persistent_audit_event` (`event_id`);
--
-- Database: `db_ms03_urlmanagement`
--
DROP DATABASE IF EXISTS `db_ms03_urlmanagement`;
CREATE DATABASE IF NOT EXISTS `db_ms03_urlmanagement` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE db_ms03_urlmanagement;

-- --------------------------------------------------------

--
-- Estrutura da tabela `databasechangelog`
--

CREATE TABLE `databasechangelog` (
  `ID` varchar(255) NOT NULL,
  `AUTHOR` varchar(255) NOT NULL,
  `FILENAME` varchar(255) NOT NULL,
  `DATEEXECUTED` datetime NOT NULL,
  `ORDEREXECUTED` int(11) NOT NULL,
  `EXECTYPE` varchar(10) NOT NULL,
  `MD5SUM` varchar(35) DEFAULT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `COMMENTS` varchar(255) DEFAULT NULL,
  `TAG` varchar(255) DEFAULT NULL,
  `LIQUIBASE` varchar(20) DEFAULT NULL,
  `CONTEXTS` varchar(255) DEFAULT NULL,
  `LABELS` varchar(255) DEFAULT NULL,
  `DEPLOYMENT_ID` varchar(10) DEFAULT NULL
) ;

--
-- Extraindo dados da tabela `databasechangelog`
--

INSERT INTO `databasechangelog` (`ID`, `AUTHOR`, `FILENAME`, `DATEEXECUTED`, `ORDEREXECUTED`, `EXECTYPE`, `MD5SUM`, `DESCRIPTION`, `COMMENTS`, `TAG`, `LIQUIBASE`, `CONTEXTS`, `LABELS`, `DEPLOYMENT_ID`) VALUES
('00000000000001', 'jhipster', 'config/liquibase/changelog/00000000000000_initial_schema.xml', '2019-03-11 23:44:57', 1, 'EXECUTED', '7:3fc487dfb545db145189f31a9ce17aa3', 'createTable tableName=jhims3_persistent_audit_event; createTable tableName=jhims3_persistent_audit_evt_data; addPrimaryKey tableName=jhims3_persistent_audit_evt_data; createIndex indexName=idx_persistent_audit_event, tableName=jhims3_persistent_au...', '', NULL, '3.5.4', NULL, NULL, '2347897068'),
('20190311023818-1', 'jhipster', 'config/liquibase/changelog/20190311023818_added_entity_ShortenedURL.xml', '2019-03-11 23:44:57', 2, 'EXECUTED', '7:6673892ec831d18d82dc73f1f630f1c5', 'createTable tableName=shortened_url', '', NULL, '3.5.4', NULL, NULL, '2347897068'),
('20190311023819-1', 'jhipster', 'config/liquibase/changelog/20190311023819_added_entity_StatisticsInfo.xml', '2019-03-11 23:44:57', 3, 'EXECUTED', '7:f7b59d3dc6187ef72f384065086e9b86', 'createTable tableName=statistics_info', '', NULL, '3.5.4', NULL, NULL, '2347897068'),
('20190311023819-2', 'jhipster', 'config/liquibase/changelog/20190311023819_added_entity_constraints_StatisticsInfo.xml', '2019-03-11 23:44:58', 4, 'EXECUTED', '7:e64e7354d1582296c070541b9f3151cc', 'addForeignKeyConstraint baseTableName=statistics_info, constraintName=fk_statistics_info_shortenedurl_id, referencedTableName=shortened_url', '', NULL, '3.5.4', NULL, NULL, '2347897068');

-- --------------------------------------------------------

--
-- Estrutura da tabela `databasechangeloglock`
--

CREATE TABLE `databasechangeloglock` (
  `ID` int(11) NOT NULL,
  `LOCKED` bit(1) NOT NULL,
  `LOCKGRANTED` datetime DEFAULT NULL,
  `LOCKEDBY` varchar(255) DEFAULT NULL
) ;

--
-- Extraindo dados da tabela `databasechangeloglock`
--

INSERT INTO `databasechangeloglock` (`ID`, `LOCKED`, `LOCKGRANTED`, `LOCKEDBY`) VALUES
(1, b'0', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `jhims3_persistent_audit_event`
--

CREATE TABLE `jhims3_persistent_audit_event` (
  `event_id` bigint(20) NOT NULL,
  `principal` varchar(50) NOT NULL,
  `event_date` timestamp NULL DEFAULT NULL,
  `event_type` varchar(255) DEFAULT NULL
) ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `jhims3_persistent_audit_evt_data`
--

CREATE TABLE `jhims3_persistent_audit_evt_data` (
  `event_id` bigint(20) NOT NULL,
  `name` varchar(150) NOT NULL,
  `value` varchar(255) DEFAULT NULL
) ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `shortened_url`
--

CREATE TABLE `shortened_url` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `description_long_url` varchar(800) NOT NULL,
  `description_shortened_url` varchar(40) DEFAULT NULL,
  `generation_algorithm_type` varchar(255) NOT NULL,
  `date_encoding` date NOT NULL,
  `date_last_access` date DEFAULT NULL,
  `is_active` bit(1) NOT NULL,
  `qty_decoding_requests_by_api` int(11) DEFAULT NULL,
  `qty_calls_for_redirecting` int(11) DEFAULT NULL
) ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `statistics_info`
--

CREATE TABLE `statistics_info` (
  `id` bigint(20) NOT NULL,
  `reason_call_done_type` varchar(255) NOT NULL,
  `date_access` date NOT NULL,
  `number_ip_address_caller` varchar(20) DEFAULT NULL,
  `country_caller_code` varchar(2) DEFAULT NULL,
  `shortenedurl_id` bigint(20) NOT NULL
) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `databasechangeloglock`
--
ALTER TABLE `databasechangeloglock`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `jhims3_persistent_audit_event`
--
ALTER TABLE `jhims3_persistent_audit_event`
  ADD PRIMARY KEY (`event_id`),
  ADD KEY `idx_persistent_audit_event` (`principal`,`event_date`);

--
-- Indexes for table `jhims3_persistent_audit_evt_data`
--
ALTER TABLE `jhims3_persistent_audit_evt_data`
  ADD PRIMARY KEY (`event_id`,`name`),
  ADD KEY `idx_persistent_audit_evt_data` (`event_id`);

--
-- Indexes for table `shortened_url`
--
ALTER TABLE `shortened_url`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `statistics_info`
--
ALTER TABLE `statistics_info`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_statistics_info_shortenedurl_id` (`shortenedurl_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `jhims3_persistent_audit_event`
--
ALTER TABLE `jhims3_persistent_audit_event`
  MODIFY `event_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `shortened_url`
--
ALTER TABLE `shortened_url`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `statistics_info`
--
ALTER TABLE `statistics_info`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `jhims3_persistent_audit_evt_data`
--
ALTER TABLE `jhims3_persistent_audit_evt_data`
  ADD CONSTRAINT `fk_evt_pers_audit_evt_data` FOREIGN KEY (`event_id`) REFERENCES `jhims3_persistent_audit_event` (`event_id`);

--
-- Limitadores para a tabela `statistics_info`
--
ALTER TABLE `statistics_info`
  ADD CONSTRAINT `fk_statistics_info_shortenedurl_id` FOREIGN KEY (`shortenedurl_id`) REFERENCES `shortened_url` (`id`);
--
-- Database: `db_uaa`
--
DROP DATABASE IF EXISTS `db_uaa`;
CREATE DATABASE IF NOT EXISTS `db_uaa` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE db_uaa;

-- --------------------------------------------------------

--
-- Estrutura da tabela `databasechangelog`
--

CREATE TABLE `databasechangelog` (
  `ID` varchar(255) NOT NULL,
  `AUTHOR` varchar(255) NOT NULL,
  `FILENAME` varchar(255) NOT NULL,
  `DATEEXECUTED` datetime NOT NULL,
  `ORDEREXECUTED` int(11) NOT NULL,
  `EXECTYPE` varchar(10) NOT NULL,
  `MD5SUM` varchar(35) DEFAULT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `COMMENTS` varchar(255) DEFAULT NULL,
  `TAG` varchar(255) DEFAULT NULL,
  `LIQUIBASE` varchar(20) DEFAULT NULL,
  `CONTEXTS` varchar(255) DEFAULT NULL,
  `LABELS` varchar(255) DEFAULT NULL,
  `DEPLOYMENT_ID` varchar(10) DEFAULT NULL
) ;

--
-- Extraindo dados da tabela `databasechangelog`
--

INSERT INTO `databasechangelog` (`ID`, `AUTHOR`, `FILENAME`, `DATEEXECUTED`, `ORDEREXECUTED`, `EXECTYPE`, `MD5SUM`, `DESCRIPTION`, `COMMENTS`, `TAG`, `LIQUIBASE`, `CONTEXTS`, `LABELS`, `DEPLOYMENT_ID`) VALUES
('00000000000001', 'jhipster', 'config/liquibase/changelog/00000000000000_initial_schema.xml', '2019-03-11 23:25:37', 1, 'EXECUTED', '7:d1eccf2d2597526eb6691123e74b6649', 'createTable tableName=jhiuaa_user; createTable tableName=jhiuaa_authority; createTable tableName=jhiuaa_user_authority; addPrimaryKey tableName=jhiuaa_user_authority; addForeignKeyConstraint baseTableName=jhiuaa_user_authority, constraintName=fk_a...', '', NULL, '3.5.4', NULL, NULL, '2346735888');

-- --------------------------------------------------------

--
-- Estrutura da tabela `databasechangeloglock`
--

CREATE TABLE `databasechangeloglock` (
  `ID` int(11) NOT NULL,
  `LOCKED` bit(1) NOT NULL,
  `LOCKGRANTED` datetime DEFAULT NULL,
  `LOCKEDBY` varchar(255) DEFAULT NULL
) ;

--
-- Extraindo dados da tabela `databasechangeloglock`
--

INSERT INTO `databasechangeloglock` (`ID`, `LOCKED`, `LOCKGRANTED`, `LOCKEDBY`) VALUES
(1, b'0', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `jhiuaa_authority`
--

CREATE TABLE `jhiuaa_authority` (
  `name` varchar(50) NOT NULL
) ;

--
-- Extraindo dados da tabela `jhiuaa_authority`
--

INSERT INTO `jhiuaa_authority` (`name`) VALUES
('ROLE_ADMIN'),
('ROLE_USER');

-- --------------------------------------------------------

--
-- Estrutura da tabela `jhiuaa_persistent_audit_event`
--

CREATE TABLE `jhiuaa_persistent_audit_event` (
  `event_id` bigint(20) NOT NULL,
  `principal` varchar(50) NOT NULL,
  `event_date` timestamp NULL DEFAULT NULL,
  `event_type` varchar(255) DEFAULT NULL
) ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `jhiuaa_persistent_audit_evt_data`
--

CREATE TABLE `jhiuaa_persistent_audit_evt_data` (
  `event_id` bigint(20) NOT NULL,
  `name` varchar(150) NOT NULL,
  `value` varchar(255) DEFAULT NULL
) ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `jhiuaa_user`
--

CREATE TABLE `jhiuaa_user` (
  `id` bigint(20) NOT NULL,
  `login` varchar(50) NOT NULL,
  `password_hash` varchar(60) NOT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `email` varchar(191) DEFAULT NULL,
  `image_url` varchar(256) DEFAULT NULL,
  `activated` bit(1) NOT NULL,
  `lang_key` varchar(6) DEFAULT NULL,
  `activation_key` varchar(20) DEFAULT NULL,
  `reset_key` varchar(20) DEFAULT NULL,
  `created_by` varchar(50) NOT NULL,
  `created_date` timestamp NULL DEFAULT NULL,
  `reset_date` timestamp NULL DEFAULT NULL,
  `last_modified_by` varchar(50) DEFAULT NULL,
  `last_modified_date` timestamp NULL DEFAULT NULL
) ;

--
-- Extraindo dados da tabela `jhiuaa_user`
--

INSERT INTO `jhiuaa_user` (`id`, `login`, `password_hash`, `first_name`, `last_name`, `email`, `image_url`, `activated`, `lang_key`, `activation_key`, `reset_key`, `created_by`, `created_date`, `reset_date`, `last_modified_by`, `last_modified_date`) VALUES
(1, 'system', '$2a$10$mE.qmcV0mFU5NcKh73TZx.z4ueI/.bDWbj0T1BYyqP481kGGarKLG', 'System', 'System', 'system@localhost', '', b'1', 'en', NULL, NULL, 'system', NULL, NULL, 'system', NULL),
(2, 'anonymoususer', '$2a$10$j8S5d7Sr7.8VTOYNviDPOeWX8KcYILUVJBsYV83Y5NtECayypx9lO', 'Anonymous', 'User', 'anonymous@localhost', '', b'1', 'en', NULL, NULL, 'system', NULL, NULL, 'system', NULL),
(3, 'admin', '$2a$10$gSAhZrxMllrbgj/kkK9UceBPpChGWJA7SYIb1Mqo.n5aNLq1/oRrC', 'Administrator', 'Administrator', 'admin@localhost', '', b'1', 'en', NULL, NULL, 'system', NULL, NULL, 'system', NULL),
(4, 'user', '$2a$10$VEjxo0jq2YG9Rbk2HmX9S.k1uZBGYUHdUcid3g/vfiEl7lwWgOH/K', 'User', 'User', 'user@localhost', '', b'1', 'en', NULL, NULL, 'system', NULL, NULL, 'system', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `jhiuaa_user_authority`
--

CREATE TABLE `jhiuaa_user_authority` (
  `user_id` bigint(20) NOT NULL,
  `authority_name` varchar(50) NOT NULL
) ;

--
-- Extraindo dados da tabela `jhiuaa_user_authority`
--

INSERT INTO `jhiuaa_user_authority` (`user_id`, `authority_name`) VALUES
(1, 'ROLE_ADMIN'),
(3, 'ROLE_ADMIN'),
(1, 'ROLE_USER'),
(3, 'ROLE_USER'),
(4, 'ROLE_USER');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `databasechangeloglock`
--
ALTER TABLE `databasechangeloglock`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `jhiuaa_authority`
--
ALTER TABLE `jhiuaa_authority`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `jhiuaa_persistent_audit_event`
--
ALTER TABLE `jhiuaa_persistent_audit_event`
  ADD PRIMARY KEY (`event_id`),
  ADD KEY `idx_persistent_audit_event` (`principal`,`event_date`);

--
-- Indexes for table `jhiuaa_persistent_audit_evt_data`
--
ALTER TABLE `jhiuaa_persistent_audit_evt_data`
  ADD PRIMARY KEY (`event_id`,`name`),
  ADD KEY `idx_persistent_audit_evt_data` (`event_id`);

--
-- Indexes for table `jhiuaa_user`
--
ALTER TABLE `jhiuaa_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_user_login` (`login`),
  ADD UNIQUE KEY `ux_user_email` (`email`);

--
-- Indexes for table `jhiuaa_user_authority`
--
ALTER TABLE `jhiuaa_user_authority`
  ADD PRIMARY KEY (`user_id`,`authority_name`),
  ADD KEY `fk_authority_name` (`authority_name`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `jhiuaa_persistent_audit_event`
--
ALTER TABLE `jhiuaa_persistent_audit_event`
  MODIFY `event_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jhiuaa_user`
--
ALTER TABLE `jhiuaa_user`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `jhiuaa_persistent_audit_evt_data`
--
ALTER TABLE `jhiuaa_persistent_audit_evt_data`
  ADD CONSTRAINT `fk_evt_pers_audit_evt_data` FOREIGN KEY (`event_id`) REFERENCES `jhiuaa_persistent_audit_event` (`event_id`);

--
-- Limitadores para a tabela `jhiuaa_user_authority`
--
ALTER TABLE `jhiuaa_user_authority`
  ADD CONSTRAINT `fk_authority_name` FOREIGN KEY (`authority_name`) REFERENCES `jhiuaa_authority` (`name`),
  ADD CONSTRAINT `fk_user_id` FOREIGN KEY (`user_id`) REFERENCES `jhiuaa_user` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
