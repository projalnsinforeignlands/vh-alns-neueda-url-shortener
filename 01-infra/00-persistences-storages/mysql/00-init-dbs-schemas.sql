-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: docker-compose_db-mysql-server_1
-- Generation Time: 12-Mar-2019 às 23:59
-- Versão do servidor: 5.7.20
-- versão do PHP: 7.2.14

SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_ms01_users`
--
DROP DATABASE IF EXISTS `db_ms01_users`;
CREATE DATABASE IF NOT EXISTS `db_ms01_users` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE db_ms01_users;

-- --------------------------------------------------------

--
-- Estrutura da tabela `user_extra_info`
--

CREATE TABLE `tb_user_extra_info` (
                                 `id` bigint(20) NOT NULL,
                                 `ds_username_login` varchar(255) NOT NULL,
                                 `ds_fullname` varchar(255) NULL,
                                 `ds_main_email` varchar(255) NOT NULL,
                                 `ds_geographic_localization` varchar(255) NULL
) ;



--
-- Extraindo dados da tabela `tb_user_extra_info`
--

INSERT INTO `tb_user_extra_info` (`id`, `ds_username_login`, `ds_fullname`, `ds_main_email`, `ds_geographic_localization`) VALUES
(1, 'admin01', 'Administrador #01 - Company Stuff', 'admin@neueda.com.br', 'Athlone / Ireland'),
(2, 'bono.u2', 'U2 Band', 'bono.vocals@u2.mus.ie', 'Dublin / Ireland'),
(3, 'freddie.mercury', 'Queen Band', 'freddie.mercury@queen.mus.uk', 'Londres / UK'),
(4, 'paul.mccartney', 'Paul McCartney', 'paul.mccartney@thebeatles.mus.uk', 'Liverpool / UK');


--
-- Indexes for dumped tables
--

--
-- Indexes for table `user_extra_info`
--
ALTER TABLE `tb_user_extra_info`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UC_USER_EXTRA_INFODS_USERNAME_LOGIN_COL` (`ds_username_login`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `user_extra_info`
--
ALTER TABLE `tb_user_extra_info`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--



--
-- Database: `db_ms03_urlmanagement`
--
DROP DATABASE IF EXISTS `db_ms03_urlmanagement`;
CREATE DATABASE IF NOT EXISTS `db_ms03_urlmanagement` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE db_ms03_urlmanagement;

-- --------------------------------------------------------

--
-- Estrutura da tabela `shortened_url`
--

CREATE TABLE `tb_shortened_url` (
                               `id` bigint(20) NOT NULL,
                               `user_id` bigint(20) NOT NULL,
                               `des_long_url` varchar(800) NOT NULL,
                               `des_shortened_url` varchar(40) NOT NULL,
                               `typ_generation_algorithm` varchar(255) NOT NULL,
                               `dth_encoding` datetime NOT NULL,
                               `dth_last_access` datetime DEFAULT NULL,
                               `ind_active` bit(1) NOT NULL,
                               `qty_decoding_requests_by_api` int(11) DEFAULT NULL,
                               `qty_calls_for_redirecting` int(11) DEFAULT NULL
) ;

--
-- Extraindo dados da tabela `tb_shortened_url`
--

INSERT INTO `tb_shortened_url` (`id`, `user_id`, `des_long_url`, `des_shortened_url`, `typ_generation_algorithm`, `dth_encoding`, `dth_last_access`, `ind_active`, `qty_decoding_requests_by_api`, `qty_calls_for_redirecting`) VALUES
(1, 1, 'https://www.youtube.com/watch?v=N3K8PjFOhy4&list=PLGxZ4Rq3BOBrgumpzz-l8kFMw2DLERdxi&index=1', 'gceyrtK', 'GOOGLE_API', '2019-03-26 20:26:00', '2019-03-26 20:41:15', b'1', 0, 301),
(3, 1, 'https://www.youtube.com/watch?v=N3K8PjFOhy4&list=PLGxZ4Rq3BOBrgumpzz-l8kFMw2DLERdxi&index=1', 'g2DyzWp', 'GOOGLE_API', '2019-03-26 20:26:00', '2019-03-26 20:26:00', b'1', 0, 105),
(4, 1, 'https://www.youtube.com/watch?v=32WHZ93Rkvo', 'ZmQ5OTk', 'BASE64_UUID', '2019-03-26 20:46:00', '2019-03-26 20:46:00', b'1', 0, 250),
(5, 2, 'https://www.youtube.com/watch?v=oSVDLGnGbD0', 'gY3UMiq', 'GOOGLE_API', '2019-03-26 21:01:00', '2019-03-26 21:05:37', b'1', 0, 211),
(6, 2, 'https://www.youtube.com/watch?v=AarQjCUPDro', 'YTVjNDY', 'BASE64_UUID', '2019-03-26 21:02:00', '2019-03-26 21:06:12', b'1', 0, 158),
(7, 3, 'https://dzone.com/articles/spring-boot-a-work-of-art?edition=457195&utm_source=Daily%20Digest&utm_medium=email&utm_campaign=Daily%20Digest%202019-03-02', 'NjljOTI', 'BASE64_UUID', '2019-03-26 21:09:00', '2019-03-26 21:09:00', b'1', 0, 510),
(8, 3, 'https://www.devmedia.com.br/curso/react/2127', 'gHdxLk2', 'GOOGLE_API', '2019-03-26 21:09:00', '2019-03-26 21:09:00', b'1', 0, 108),
(9, 3, 'https://ipstack.com/documentation', 'ggKUj43', 'GOOGLE_API', '2019-03-26 21:09:00', '2019-03-26 21:09:00', b'1', 0, 59),
(10, 3, 'https://dzone.com/articles/create-a-production-grade-java-microservice-archit?edition=457221&utm_source=Daily%20Digest&utm_medium=email&utm_campaign=Daily%20Digest%202019-03-06', 'MmE0NWU', 'BASE64_UUID', '2019-03-26 21:09:00', '2019-03-26 21:09:00', b'1', 0, 0),
(11, 4, 'https://www.udemy.com/cart/subscribe/course/2256658/', 'gB2kDgi', 'GOOGLE_API', '2019-03-26 21:10:00', '2019-03-26 21:10:00', b'1', 0, 18),
(12, 4, 'https://app.revelo.com.br/signup/candidates/andre-luiz-do-nascimento-sousa/experiences?locale=pt-BR&utm_campaign=referralbeta&utm_content=shared&utm_medium=referral&utm_source=ambassador&utm_nooverride=1', 'Y2FkZDA', 'BASE64_UUID', '2019-03-26 21:10:00', '2019-03-26 21:13:28', b'1', 0, 202),
(13, 4, 'https://www.facebook.com/groups/flutterbr/', 'gtP5zJ5', 'GOOGLE_API', '2019-03-26 21:11:00', '2019-03-26 21:12:58', b'1', 0, 115);


-- --------------------------------------------------------

--
-- Estrutura da tabela `statistics_info`
--

CREATE TABLE `tb_statistics_info` (
                                 `id` bigint(20) NOT NULL,
                                 `typ_reason_call_done` varchar(255) NOT NULL,
                                 `dth_access` datetime NOT NULL,
                                 `num_ip_address_caller` varchar(20) DEFAULT NULL,
                                 `cod_country_caller` varchar(2) DEFAULT NULL,
                                 `shortenedurl_id` bigint(20) NOT NULL
) ;

--
-- Extraindo dados da tabela `tb_statistics_info`
--

INSERT INTO `tb_statistics_info` (`id`, `typ_reason_call_done`, `dth_access`, `num_ip_address_caller`, `cod_country_caller`, `shortenedurl_id`) VALUES
(1, 'TO_REDIRECT', '2019-03-26 20:27:35', '0:0:0:0:0:0:0:1', 'BR', 1),
(2, 'TO_REDIRECT', '2019-03-26 20:37:39', '0:0:0:0:0:0:0:1', 'LH', 1),
(3, 'TO_REDIRECT', '2019-03-26 20:41:14', '192.168.126.1', 'LH', 1),
(4, 'TO_REDIRECT', '2019-03-26 21:04:51', '192.168.126.1', 'BR', 5),
(5, 'TO_REDIRECT', '2019-03-26 21:05:16', '192.168.126.1', 'BR', 5),
(6, 'TO_REDIRECT', '2019-03-26 21:06:12', '192.168.126.1', 'LH', 6),
(7, 'TO_REDIRECT', '2019-03-26 21:12:12', '192.168.126.1', 'BR', 12),
(8, 'TO_REDIRECT', '2019-03-26 21:12:37', '192.168.126.1', 'BR', 13),
(9, 'TO_REDIRECT', '2019-03-26 21:13:07', '192.168.126.1', 'BR', 12);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `shortened_url`
--
ALTER TABLE `tb_shortened_url`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UX_SHORTENEDURL_DESSHORTENEDURL_COL` (`des_shortened_url`);

--
-- Indexes for table `statistics_info`
--
ALTER TABLE `tb_statistics_info`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_statistics_info_shortenedurl_id` (`shortenedurl_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `shortened_url`
--
ALTER TABLE `tb_shortened_url`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `statistics_info`
--
ALTER TABLE `tb_statistics_info`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `statistics_info`
--
ALTER TABLE `tb_statistics_info`
  ADD CONSTRAINT `fk_statistics_info_shortenedurl_id` FOREIGN KEY (`shortenedurl_id`) REFERENCES `tb_shortened_url` (`id`);


SET PASSWORD FOR 'root' = PASSWORD('1a88a1');

CREATE USER 'dbaMaster01'@'localhost' IDENTIFIED BY '1a88a1';
CREATE USER 'dbaMaster01'@'%' IDENTIFIED BY '1a88a1';
GRANT ALL PRIVILEGES ON *.* TO 'dbaMaster01'@'%' WITH GRANT OPTION;
GRANT ALL PRIVILEGES ON *.* TO 'dbaMaster01'@'localhost' WITH GRANT OPTION;

FLUSH PRIVILEGES;

COMMIT;


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
