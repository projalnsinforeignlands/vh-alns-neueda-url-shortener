#!/bin/bash

cd docker-compose

echo "Stopping all containers with Docker Compose..."
docker-compose down

cd ..

echo "Cleaning and Repackaging MS01-Users App..."
mvn -DskipTests clean package -f ./02-microservices/ms01Users/pom.xml

echo "Cleaning and Repackaging MS02-TextEncoder App..."
mvn -DskipTests clean package -f ./02-microservices/ms02TextEncoder/pom.xml

echo "Cleaning and Repackaging MS03-Url-Management App..."
mvn -DskipTests clean package -f ./02-microservices/ms03UrlManagement/pom.xml

echo "Cleaning and Repackaging Gtw1-Alns-Neueda-Url-Shortener App..."
mvn -DskipTests clean package -f ./03-app-gateways/gtw01AlnsNeuedaUrlShortener/pom.xml


cd docker-compose

echo "Starting all containers with Docker Compose..."
docker-compose build && docker-compose up -d

echo "\n\n\n\n\n\n\n Access Swagger UI from: http://localhost:8080/swagger-ui.html \n\n\n\n\n\n"