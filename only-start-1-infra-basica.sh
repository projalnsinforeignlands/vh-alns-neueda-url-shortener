#!/bin/bash

clear

cd docker-compose

echo "Starting Only Containers of Basic Infra with Docker Compose..."
docker-compose build app-infra-phpmyadmin app-infra-eureka-registry db-mongodb-server && docker-compose up -d app-infra-phpmyadmin app-infra-eureka-registry db-mongodb-server

echo "\n\n\n\n\n\n\n Only Basic Infra Started successfully! \n\n\n\n\n\n"
 
