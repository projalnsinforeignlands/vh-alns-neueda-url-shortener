#!/bin/bash

clear

cd docker-compose

echo "Stopping containers for Gateway App with Docker Compose..."
docker-compose stop app-gw01-alnsneuedaurlshortener 


echo "\n\n\n\n\n\n\n Containers for Gateway App Stopped successfully! \n\n\n\n\n\n"
 