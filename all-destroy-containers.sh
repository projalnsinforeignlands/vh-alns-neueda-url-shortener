#!/bin/bash

cd docker-compose

echo "Remove all containers with Docker Compose..."
docker-compose down

cd ..

echo "\n\n\n\n\n\n\n All containers created by Docker-Compose were stopped and removed. \n\n\n\n\n\n"