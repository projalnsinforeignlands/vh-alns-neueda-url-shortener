package br.org.codingtests.alnsneuedaurlshortener.ms02textencoder.domain;


import br.org.codingtests.alnsneuedaurlshortener.ms02textencoder.domain.enumeration.AlgorithmShorteningURLEnum;
import br.org.codingtests.alnsneuedaurlshortener.ms02textencoder.domain.enumeration.KeyStatusEnum;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * KeyTextEncodedPreGenerated entity.
 * Stores texts encoded generated in a batch way.
 * @author Andre Nascimento
 */
@NoArgsConstructor
@Document(collection = "key_text_encoded_pre_generated")
public class KeyTextEncodedPreGenerated implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    private String id;

    @Field("instant_creation")
    private Instant instantCreation;

    @Field("status")
    private KeyStatusEnum status;

    @Field("typ_generation_algorithm")
    private AlgorithmShorteningURLEnum generationAlgorithmType;


    public KeyTextEncodedPreGenerated() {

    }


    public KeyTextEncodedPreGenerated(String pKeyGenerated,
                                      Instant pCreationInstant,
                                      KeyStatusEnum pStatus,
                                      AlgorithmShorteningURLEnum pAlgorithmUsed) {

        this.id = pKeyGenerated;
        this.instantCreation = pCreationInstant;
        this.status = pStatus;
        this.generationAlgorithmType = pAlgorithmUsed;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Instant getInstantCreation() {
        return instantCreation;
    }

    public void setInstantCreation(Instant instantCreation) {
        this.instantCreation = instantCreation;
    }

    public KeyStatusEnum getStatus() {
        return status;
    }

    public void setStatus(KeyStatusEnum status) {
        this.status = status;
    }

    public AlgorithmShorteningURLEnum getGenerationAlgorithmType() {
        return generationAlgorithmType;
    }

    public void setGenerationAlgorithmType(AlgorithmShorteningURLEnum generationAlgorithmType) {
        this.generationAlgorithmType = generationAlgorithmType;
    }

    public KeyTextEncodedPreGenerated instantCreation(Instant instantCreation) {
        this.instantCreation = instantCreation;
        return this;
    }

    public KeyTextEncodedPreGenerated status(KeyStatusEnum status) {
        this.status = status;
        return this;
    }

    public KeyTextEncodedPreGenerated generationAlgorithmType(AlgorithmShorteningURLEnum generationAlgorithmType) {
        this.generationAlgorithmType = generationAlgorithmType;
        return this;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        KeyTextEncodedPreGenerated that = (KeyTextEncodedPreGenerated) o;
        return Objects.equals(getId(), that.getId()) &&
                Objects.equals(getInstantCreation(), that.getInstantCreation()) &&
                getStatus() == that.getStatus() &&
                getGenerationAlgorithmType() == that.getGenerationAlgorithmType();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getInstantCreation(), getStatus(), getGenerationAlgorithmType());
    }

    @Override
    public String toString() {
        return "KeyTextEncodedPreGenerated{" +
                "id='" + id + '\'' +
                ", instantCreation=" + instantCreation +
                ", status=" + status +
                ", generationAlgorithmType=" + generationAlgorithmType +
                '}';
    }


}


