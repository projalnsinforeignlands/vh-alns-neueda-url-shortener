/**
 * View Models used by Spring MVC REST controllers.
 */
package br.org.codingtests.alnsneuedaurlshortener.ms02textencoder.web.rest.vm;
