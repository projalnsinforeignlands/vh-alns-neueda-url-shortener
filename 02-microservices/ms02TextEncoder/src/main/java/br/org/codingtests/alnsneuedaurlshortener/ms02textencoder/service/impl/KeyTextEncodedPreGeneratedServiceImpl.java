package br.org.codingtests.alnsneuedaurlshortener.ms02textencoder.service.impl;

import br.org.codingtests.alnsneuedaurlshortener.ms02textencoder.domain.KeyTextEncodedPreGenerated;
import br.org.codingtests.alnsneuedaurlshortener.ms02textencoder.domain.enumeration.AlgorithmShorteningURLEnum;
import br.org.codingtests.alnsneuedaurlshortener.ms02textencoder.domain.enumeration.KeyStatusEnum;
import br.org.codingtests.alnsneuedaurlshortener.ms02textencoder.repository.KeyTextEncodedPreGeneratedRepository;
import br.org.codingtests.alnsneuedaurlshortener.ms02textencoder.service.KeyTextEncodedPreGeneratedService;
import br.org.codingtests.alnsneuedaurlshortener.ms02textencoder.service.algorithms.IMakeShortURLAlgorithm;
import br.org.codingtests.alnsneuedaurlshortener.ms02textencoder.service.dto.KeyTextEncodedPreGeneratedDTO;
import br.org.codingtests.alnsneuedaurlshortener.ms02textencoder.service.mapper.KeyTextEncodedPreGeneratedMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Service Implementation for managing KeyTextEncodedPreGenerated.
 */
@Service
public class KeyTextEncodedPreGeneratedServiceImpl implements KeyTextEncodedPreGeneratedService {

    private final Logger log = LoggerFactory.getLogger(KeyTextEncodedPreGeneratedServiceImpl.class);

    private final KeyTextEncodedPreGeneratedRepository keyTextEncodedPreGeneratedRepository;

    private final KeyTextEncodedPreGeneratedMapper keyTextEncodedPreGeneratedMapper;

    @Autowired
    @Qualifier("URLShortenerByGoogleAPI")
    private IMakeShortURLAlgorithm urlShortenerByGoogleAPI;

    @Autowired
    @Qualifier("URLShortenerByBase64UUID")
    private IMakeShortURLAlgorithm urlShortenerByBase64Uuid;

    public KeyTextEncodedPreGeneratedServiceImpl(KeyTextEncodedPreGeneratedRepository keyTextEncodedPreGeneratedRepository, KeyTextEncodedPreGeneratedMapper keyTextEncodedPreGeneratedMapper) {
        this.keyTextEncodedPreGeneratedRepository = keyTextEncodedPreGeneratedRepository;
        this.keyTextEncodedPreGeneratedMapper = keyTextEncodedPreGeneratedMapper;
    }


    @Scheduled(fixedRate = 15000)
    public void generateEncodedKeysInBatch() {

        List<KeyTextEncodedPreGeneratedDTO> generatedEncodedKeysList = null;

        log.info("==> Starting generation of Encoded Keys in Batch");

        generatedEncodedKeysList = this.createAndSaveKeyEncodedUsingDifferentAlgorithms();

        if (!StringUtils.isEmpty(generatedEncodedKeysList) && generatedEncodedKeysList.size() > 0) {

            log.info("m=generateEncodedKeysInBatch, KeyTextEncodedPreGenerated chunk saved with the following keys: {}", generatedEncodedKeysList);

        } else {

            log.info("m=generateEncodedKeysInBatch, Is not necessary to generate more keys for a while.");

        }
    }


    @Override
    public List<KeyTextEncodedPreGeneratedDTO> createAndSaveKeyEncodedUsingDifferentAlgorithms() {

        List<KeyTextEncodedPreGenerated> generatedEncodedKeysList = null;
        final List<KeyTextEncodedPreGenerated> encodedKeysToSaveList = new ArrayList<>();;
        AlgorithmShorteningURLEnum[] algorithmsToUseArray = {AlgorithmShorteningURLEnum.BASE64_UUID, AlgorithmShorteningURLEnum.GOOGLE_API};
        Integer[] chunksSizesPerAlgorithm = {5, 5};
        double loadFactorIndex = 0.25d;

        int i = 0;

        for (AlgorithmShorteningURLEnum oneAlgorithm : algorithmsToUseArray) {

            if (shouldGenerateMoreKeys(oneAlgorithm, chunksSizesPerAlgorithm[i], loadFactorIndex)) {

                log.info("m=createAndSaveKeyEncodedUsingDifferentAlgorithms, Generating key chunk with size {} using the Algorithm: ", chunksSizesPerAlgorithm[i], oneAlgorithm);

                IntStream.rangeClosed(0, chunksSizesPerAlgorithm[i])
                        .forEach(idxRange -> encodedKeysToSaveList.add(new KeyTextEncodedPreGenerated(getOrGenerateEncodedKeyByAlgorithm(oneAlgorithm),
                                        Instant.now(),
                                        KeyStatusEnum.AVAILABLE,
                                        oneAlgorithm)
                                )
                        );
            }
        }

        generatedEncodedKeysList = keyTextEncodedPreGeneratedRepository.saveAll(encodedKeysToSaveList);

        return generatedEncodedKeysList.stream().map(keyTextEncodedPreGeneratedMapper::toDto).collect(Collectors.toList());
    }


    private String getOrGenerateEncodedKeyByAlgorithm(AlgorithmShorteningURLEnum pAlgorithmToBeUsed) {

        String strResult = null;

        if (pAlgorithmToBeUsed != null) {

            switch (pAlgorithmToBeUsed) {

                case GOOGLE_API:
                    strResult = urlShortenerByGoogleAPI.generateOnlyShortURL();
                    strResult = "g" + strResult.substring(strResult.lastIndexOf("/") + 1);
                    break;
                default:
                    strResult = urlShortenerByBase64Uuid.generateOnlyShortURL();
                    break;
            }
        }


        return strResult;
    }



    private boolean shouldGenerateMoreKeys(AlgorithmShorteningURLEnum pOneAlgorithm, Integer pChunkSize, double pLoadFactorIndex) {

        boolean bolResult = true;
        Long qtdKeysAvailableByAlgorithm = 0L;

        qtdKeysAvailableByAlgorithm = keyTextEncodedPreGeneratedRepository.countByGenerationAlgorithmTypeAndStatus(pOneAlgorithm, KeyStatusEnum.AVAILABLE);

        if (qtdKeysAvailableByAlgorithm > 0) {
            bolResult = (qtdKeysAvailableByAlgorithm < ( pChunkSize * (1 + pLoadFactorIndex)));
        }

        return bolResult;

    }


    @Override
    public String obtainAndReserveNextEncodedKeyAvailableByAlgorithm(AlgorithmShorteningURLEnum pAlgorithmTarget) {

        String strResult = null;
        KeyTextEncodedPreGenerated nextKeyTextEncodedAvailable = null;

        nextKeyTextEncodedAvailable = keyTextEncodedPreGeneratedRepository.findFirstByGenerationAlgorithmTypeAndStatusOrderByInstantCreationAsc(pAlgorithmTarget, KeyStatusEnum.AVAILABLE);

        if (nextKeyTextEncodedAvailable != null) {
            strResult = nextKeyTextEncodedAvailable.getId();

            nextKeyTextEncodedAvailable.setStatus(KeyStatusEnum.RESERVED);

            nextKeyTextEncodedAvailable = keyTextEncodedPreGeneratedRepository.save(nextKeyTextEncodedAvailable);
        }

        return strResult;
    }


    @Override
    public KeyTextEncodedPreGeneratedDTO changeStatusEncodedKey(String pEncodedKeyTarget, KeyStatusEnum pKeyStatusEnum) {

        KeyTextEncodedPreGeneratedDTO objResult = null;
        KeyTextEncodedPreGenerated keyTextEncodedTarget = null;
        Optional<KeyTextEncodedPreGenerated> optKeyTextEncodedTarget = null;

        if (!StringUtils.isEmpty(pEncodedKeyTarget)) {

            optKeyTextEncodedTarget = keyTextEncodedPreGeneratedRepository.findById(pEncodedKeyTarget.trim());

            optKeyTextEncodedTarget.ifPresent( pEntityKeyTextFound -> {

                pEntityKeyTextFound.setStatus(pKeyStatusEnum);
                pEntityKeyTextFound = keyTextEncodedPreGeneratedRepository.save(pEntityKeyTextFound);

            });

            objResult = optKeyTextEncodedTarget.map(keyTextEncodedPreGeneratedMapper::toDto).get();

        }

        return objResult;
    }


    @Override
    public Integer deleteAllOrMarkAllAsInactive(Boolean pDontDeleteAllJustMarkInactive) {

        Integer intResult = 0;
        List<KeyTextEncodedPreGenerated> keysTextEncodedsList = null;

        KeyTextEncodedPreGenerated keyTextEncodedTarget = null;
        Optional<KeyTextEncodedPreGenerated> optKeyTextEncodedTarget = null;

        if (pDontDeleteAllJustMarkInactive) {

            keysTextEncodedsList = keyTextEncodedPreGeneratedRepository.findAll();

            if (!CollectionUtils.isEmpty(keysTextEncodedsList)) {

                keysTextEncodedsList.forEach(pEntityKeyTextFound -> pEntityKeyTextFound.setStatus(KeyStatusEnum.INACTIVE));

                keysTextEncodedsList = keyTextEncodedPreGeneratedRepository.saveAll(keysTextEncodedsList);
            }
            intResult = 1;

        } else {

            keyTextEncodedPreGeneratedRepository.deleteAll();
            intResult = 1;

        }

        return intResult;
    }


    /**
     * Get all the keyTextEncodedPreGenerateds.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    public Page<KeyTextEncodedPreGeneratedDTO> findAll(Pageable pageable) {
        log.debug("Request to get all KeyTextEncodedPreGenerateds");
        return keyTextEncodedPreGeneratedRepository.findAll(pageable)
            .map(keyTextEncodedPreGeneratedMapper::toDto);
    }


}
