package br.org.codingtests.alnsneuedaurlshortener.ms02textencoder.domain.enumeration;

/**
 * The KeyStatusEnum enumeration.
 */
public enum KeyStatusEnum {
    AVAILABLE, RESERVED, IN_USE, INACTIVE
}
