package br.org.codingtests.alnsneuedaurlshortener.ms02textencoder.repository;

import br.org.codingtests.alnsneuedaurlshortener.ms02textencoder.domain.KeyTextEncodedPreGenerated;
import br.org.codingtests.alnsneuedaurlshortener.ms02textencoder.domain.enumeration.AlgorithmShorteningURLEnum;
import br.org.codingtests.alnsneuedaurlshortener.ms02textencoder.domain.enumeration.KeyStatusEnum;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;


/**
 * Spring Data MongoDB repository for the KeyTextEncodedPreGenerated entity.
 */
@SuppressWarnings("unused")
@Repository
public interface KeyTextEncodedPreGeneratedRepository extends MongoRepository<KeyTextEncodedPreGenerated, String> {

    public Long countByGenerationAlgorithmTypeAndStatus(AlgorithmShorteningURLEnum pAlgorithmTarget, KeyStatusEnum pKeyStatus);

    public KeyTextEncodedPreGenerated findFirstByGenerationAlgorithmTypeAndStatusOrderByInstantCreationAsc(AlgorithmShorteningURLEnum pAlgorithmTarget, KeyStatusEnum pKeyStatus);

}
