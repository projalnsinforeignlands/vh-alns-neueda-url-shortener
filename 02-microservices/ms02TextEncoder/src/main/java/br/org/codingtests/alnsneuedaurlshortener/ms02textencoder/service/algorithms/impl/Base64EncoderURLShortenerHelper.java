package br.org.codingtests.alnsneuedaurlshortener.ms02textencoder.service.algorithms.impl;

import br.org.codingtests.alnsneuedaurlshortener.ms02textencoder.service.algorithms.IMakeShortURLAlgorithm;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.UUID;

/**
 * URL Shortener using Base64 Encoder style
 */
@Component("URLShortenerByBase64UUID")
@Slf4j
public class Base64EncoderURLShortenerHelper implements IMakeShortURLAlgorithm {

    private final Logger log = LoggerFactory.getLogger(Base64EncoderURLShortenerHelper.class);


    public Base64EncoderURLShortenerHelper() {

    }


    @SuppressWarnings("unused")
    @Override
    public String makeShortURLFromLongURL(String pLongURL) {

        String strResult = null;

        strResult = Base64.getEncoder()
                          .encodeToString(UUID.fromString(pLongURL).randomUUID()
                          .toString()
                          .getBytes(StandardCharsets.UTF_8))
                          .substring(0, 7);

        log.debug("==> Base64 + UUID Encoded: From this Long URL '" + pLongURL + "' was generated this ShortURL: '" + strResult + "'");

        return strResult;

    }


    @SuppressWarnings("unused")
    @Override
    public String generateOnlyShortURL() {

        String strResult = null;

        strResult = Base64.getEncoder()
                .encodeToString(UUID.randomUUID()
                        .toString()
                        .getBytes(StandardCharsets.UTF_8))
                .substring(0, 7);

        log.debug("==> Base64 + UUID Encoded: It was generated this Enconded Key: '" + strResult + "'");

        return strResult;

    }

}
