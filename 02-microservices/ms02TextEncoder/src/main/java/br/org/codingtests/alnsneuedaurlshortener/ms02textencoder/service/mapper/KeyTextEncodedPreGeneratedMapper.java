package br.org.codingtests.alnsneuedaurlshortener.ms02textencoder.service.mapper;

import br.org.codingtests.alnsneuedaurlshortener.ms02textencoder.domain.*;
import br.org.codingtests.alnsneuedaurlshortener.ms02textencoder.service.dto.KeyTextEncodedPreGeneratedDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity KeyTextEncodedPreGenerated and its DTO KeyTextEncodedPreGeneratedDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface KeyTextEncodedPreGeneratedMapper extends EntityMapper<KeyTextEncodedPreGeneratedDTO, KeyTextEncodedPreGenerated> {



    default KeyTextEncodedPreGenerated fromId(String id) {
        if (id == null) {
            return null;
        }
        KeyTextEncodedPreGenerated keyTextEncodedPreGenerated = new KeyTextEncodedPreGenerated();
        keyTextEncodedPreGenerated.setId(id);
        return keyTextEncodedPreGenerated;
    }
}
