package br.org.codingtests.alnsneuedaurlshortener.ms02textencoder.service.algorithms.impl;

import br.org.codingtests.alnsneuedaurlshortener.ms02textencoder.service.algorithms.IMakeShortURLAlgorithm;
import com.github.scribejava.apis.GoogleApi20;
import com.github.scribejava.core.builder.ServiceBuilder;
import com.github.scribejava.core.model.OAuthRequest;
import com.github.scribejava.core.model.Response;
import com.github.scribejava.core.model.Verb;
import com.github.scribejava.core.oauth.OAuthService;
import com.google.common.reflect.TypeToken;
import com.google.gson.GsonBuilder;
import lombok.extern.slf4j.Slf4j;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.lang.reflect.Type;
import java.util.Map;
import java.util.UUID;

/**
 * URL Shortener using Google API Shortener
 */
@Component("URLShortenerByGoogleAPI")
@Slf4j
public class GoogleURLShortenerHelper implements IMakeShortURLAlgorithm {

    private final Logger log = LoggerFactory.getLogger(GoogleURLShortenerHelper.class);


    public GoogleURLShortenerHelper() {

    }


    @SuppressWarnings("unused")
    @Override
    public String makeShortURLFromLongURL(String pLongURL) {
    	
        OAuthService oAuthService = null;
        OAuthRequest oAuthRequest = null;
        JSONObject jsonObject = null;
        Response response = null;
        Type typeOfMap = null;
        Map<String, String> responseMap = null;
        String shortURLResult = null;
        String longURLToBeShortened = null;

        try {

            if (!StringUtils.isEmpty(pLongURL)) {
                longURLToBeShortened = pLongURL;
            } else {
                longURLToBeShortened = "http://localhost:8080/" + UUID.randomUUID();
            }

            oAuthService = new ServiceBuilder().apiKey("anonymous")
                    .apiSecret("anonymous")
                    .scope("https://www.googleapis.com/auth/urlshortener")
                    .build(GoogleApi20.instance());

            oAuthRequest = new OAuthRequest(Verb.POST, "https://www.googleapis.com/urlshortener/v1/url?key=AIzaSyCNrZGy8oLPkQxzf6xQxqyTKjXCjCH4ZYw", oAuthService);
            oAuthRequest.addHeader("Content-Type", "application/json");

            //String json = "{\"longUrl\": \"http://"+longUrl+"/\"}";
            jsonObject = new JSONObject();
            jsonObject.put("longUrl", longURLToBeShortened);
            oAuthRequest.addPayload(jsonObject.toString());
            response = oAuthRequest.send();
            typeOfMap = new TypeToken<Map<String, String>>() {
            }.getType();
            responseMap = new GsonBuilder().create().fromJson(response.getBody(), typeOfMap);
            shortURLResult = responseMap.get("id");

            log.debug("==> GoogleURLShortenerAPI Encoded: From this Long URL '" + pLongURL + "' was generated this ShortURL: '" + shortURLResult + "'");


        } catch (Exception e) {
            log.error("==> Fail when generating ShortURL using Google API", e);
            shortURLResult = "[Fail when using Google API]";
            throw new RuntimeException(e);
        }

        return shortURLResult;
 
    }


    @SuppressWarnings("unused")
    @Override
    public String generateOnlyShortURL() {

        String shortURLResult = null;
        String longURLToBeShortened = null;

        longURLToBeShortened = "http://localhost:8080/" + UUID.randomUUID();

        shortURLResult = this.makeShortURLFromLongURL(longURLToBeShortened);

        return shortURLResult;

    }

}
