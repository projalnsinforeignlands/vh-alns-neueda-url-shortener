package br.org.codingtests.alnsneuedaurlshortener.ms02textencoder.service.dto;

import io.swagger.annotations.ApiModel;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Just a DTO to wrapper simple and basic parameters in Request or Response.
 */
@ApiModel(description = "Just a DTO to wrapper simple and basic parameters in Request or Response. @author Andre Nascimento")
public class SimpleParametersWrapperDTO implements Serializable {

    private String  id;
    private Integer simpleParameterAsInteger;
    private String  simpleParameterAsString;
    private LocalDate simpleParameterAsDate;
    private Double  simpleParameterAsDouble;
    private Map<String, String> complexParametersAsMap;
    private List<String> complexParametersAsList;

    public SimpleParametersWrapperDTO() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getSimpleParameterAsInteger() {
        return simpleParameterAsInteger;
    }

    public void setSimpleParameterAsInteger(Integer simpleParameterAsInteger) {
        this.simpleParameterAsInteger = simpleParameterAsInteger;
    }

    public String getSimpleParameterAsString() {
        return simpleParameterAsString;
    }

    public void setSimpleParameterAsString(String simpleParameterAsString) {
        this.simpleParameterAsString = simpleParameterAsString;
    }

    public LocalDate getSimpleParameterAsDate() {
        return simpleParameterAsDate;
    }

    public void setSimpleParameterAsDate(LocalDate simpleParameterAsDate) {
        this.simpleParameterAsDate = simpleParameterAsDate;
    }

    public Double getSimpleParameterAsDouble() {
        return simpleParameterAsDouble;
    }

    public void setSimpleParameterAsDouble(Double simpleParameterAsDouble) {
        this.simpleParameterAsDouble = simpleParameterAsDouble;
    }

    public Map<String, String> getComplexParametersAsMap() {
        return complexParametersAsMap;
    }

    public void setComplexParametersAsMap(Map<String, String> complexParametersAsMap) {
        this.complexParametersAsMap = complexParametersAsMap;
    }

    public List<String> getComplexParametersAsList() {
        return complexParametersAsList;
    }

    public void setComplexParametersAsList(List<String> complexParametersAsList) {
        this.complexParametersAsList = complexParametersAsList;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SimpleParametersWrapperDTO that = (SimpleParametersWrapperDTO) o;
        return Objects.equals(getId(), that.getId()) &&
                Objects.equals(getSimpleParameterAsInteger(), that.getSimpleParameterAsInteger()) &&
                Objects.equals(getSimpleParameterAsString(), that.getSimpleParameterAsString()) &&
                Objects.equals(getSimpleParameterAsDate(), that.getSimpleParameterAsDate()) &&
                Objects.equals(getSimpleParameterAsDouble(), that.getSimpleParameterAsDouble()) &&
                Objects.equals(getComplexParametersAsMap(), that.getComplexParametersAsMap()) &&
                Objects.equals(getComplexParametersAsList(), that.getComplexParametersAsList());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getSimpleParameterAsInteger(), getSimpleParameterAsString(), getSimpleParameterAsDate(), getSimpleParameterAsDouble(), getComplexParametersAsMap(), getComplexParametersAsList());
    }


    @Override
    public String toString() {
        return "SimpleParametersWrapperDTO{" +
                "id='" + id + '\'' +
                ", simpleParameterAsInteger=" + simpleParameterAsInteger +
                ", simpleParameterAsString='" + simpleParameterAsString + '\'' +
                ", simpleParameterAsDate=" + simpleParameterAsDate +
                ", simpleParameterAsDouble=" + simpleParameterAsDouble +
                ", complexParametersAsMap=" + complexParametersAsMap +
                ", complexParametersAsList=" + complexParametersAsList +
                '}';
    }
}
