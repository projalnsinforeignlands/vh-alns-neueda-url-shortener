package br.org.codingtests.alnsneuedaurlshortener.ms02textencoder.domain.enumeration;

/**
 * The AlgorithmShorteningURLEnum enumeration.
 */
public enum AlgorithmShorteningURLEnum {
    BASE64_UUID,
    GOOGLE_API
}
