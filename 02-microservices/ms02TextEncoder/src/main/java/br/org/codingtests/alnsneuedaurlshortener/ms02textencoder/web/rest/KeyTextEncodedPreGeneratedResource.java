package br.org.codingtests.alnsneuedaurlshortener.ms02textencoder.web.rest;

import br.org.codingtests.alnsneuedaurlshortener.ms02textencoder.domain.enumeration.AlgorithmShorteningURLEnum;
import br.org.codingtests.alnsneuedaurlshortener.ms02textencoder.domain.enumeration.KeyStatusEnum;
import br.org.codingtests.alnsneuedaurlshortener.ms02textencoder.service.KeyTextEncodedPreGeneratedService;
import br.org.codingtests.alnsneuedaurlshortener.ms02textencoder.service.dto.KeyTextEncodedPreGeneratedDTO;
import br.org.codingtests.alnsneuedaurlshortener.ms02textencoder.service.dto.SimpleParametersWrapperDTO;
import br.org.codingtests.alnsneuedaurlshortener.ms02textencoder.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * REST controller for managing KeyTextEncodedPreGenerated.
 */
@RestController
@RequestMapping("/api")
public class KeyTextEncodedPreGeneratedResource {

    private final Logger log = LoggerFactory.getLogger(KeyTextEncodedPreGeneratedResource.class);

    private static final String ENTITY_NAME = "ms02TextEncoderKeyTextEncodedPreGenerated";

    private final KeyTextEncodedPreGeneratedService keyTextEncodedPreGeneratedService;

    public KeyTextEncodedPreGeneratedResource(KeyTextEncodedPreGeneratedService keyTextEncodedPreGeneratedService) {
        this.keyTextEncodedPreGeneratedService = keyTextEncodedPreGeneratedService;
    }



    /**
     * Creates and saves sincronously a chunk of @{@link br.org.codingtests.alnsneuedaurlshortener.ms02textencoder.domain.KeyTextEncodedPreGenerated} entities,
     * using all the Algorithms availables. All the created Keys will be saved in the MongoDB.
     * In success case, API will return all KeyTextEncodedPreGenerated created and saved.
     *
    * @return 201 Status Code shorted url
     */
    @ApiOperation(value = "Creates and saves sinchronously a chunk of KeyTextEncodedPreGenerated entities", nickname = "createAndSaveKeyEncodedUsingDifferentAlgorithms", notes = "All the created Keys will be saved in the MongoDB")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "KeyTextEncodedPreGenerated Createds and Saved"),
            @ApiResponse(code = 400, message = "Invalid input, object invalid"),
            @ApiResponse(code = 404, message = "The key was not found") })
    @PostMapping(value = "/create-chunk-encodedkeys")
    public ResponseEntity<List<KeyTextEncodedPreGeneratedDTO>> createAndSaveKeyEncodedUsingDifferentAlgorithms() {

        List<KeyTextEncodedPreGeneratedDTO> resultList = null;

        log.debug("REST post to trigger the Creation and Saving a chunk of KeyTextEncodedPreGenerated entities");

        resultList = keyTextEncodedPreGeneratedService.createAndSaveKeyEncodedUsingDifferentAlgorithms();

        return ResponseEntity.ok(resultList);

    }


    /**
     * Obtains the next Encoded Key by Algorithm informed, and chages it's state to "RESERVED".
     *
     * @return SimpleParametersWrapperDTO The Encoded Key available and 200 Status Code
     */
    @ApiOperation(value = "Obtains the next Encoded Key by Algorithm informed, and chages it's state to 'RESERVED'",
               nickname = "obtainAndReserveNextEncodedKeyAvailableByAlgorithm",
                  notes = "Obtains the next Encoded Key by Algorithm informed, and chages it's state to 'RESERVED'")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Next Encoded Text available"),
            @ApiResponse(code = 400, message = "Invalid input, object invalid"),
            @ApiResponse(code = 404, message = "The key was not found") })
    @GetMapping(value = "/next-encoded-key-available/{pAlgorithmTarget}", produces = { "application/json" })
    public ResponseEntity<SimpleParametersWrapperDTO> obtainAndReserveNextEncodedKeyAvailableByAlgorithm(@PathVariable String pAlgorithmTarget) {

        String encodedKeyFound = null;
        SimpleParametersWrapperDTO simpleParamsDTO = new SimpleParametersWrapperDTO();

        log.debug("REST request to obtain and reserve the next Encoded Key Available By Algorithm");

        encodedKeyFound = keyTextEncodedPreGeneratedService.obtainAndReserveNextEncodedKeyAvailableByAlgorithm(AlgorithmShorteningURLEnum.valueOf(pAlgorithmTarget));
        simpleParamsDTO.setSimpleParameterAsString(encodedKeyFound);

        return ResponseEntity.ok().body(simpleParamsDTO);

    }


    /**
     * Changes the KeyTextEncodedPreGenerated state.
     *
     * @return The Encoded Key available and 201 Status Code
     */
    @ApiOperation(value = "Changes the KeyTextEncodedPreGenerated state", nickname = "changeStatusEncodedKey", notes = "Changes the KeyTextEncodedPreGenerated state to 'IN_USE'")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "The state was changed"),
            @ApiResponse(code = 400, message = "Invalid input, object invalid"),
            @ApiResponse(code = 404, message = "The key was not found") })
    @PutMapping(value = "/change-status-encoded-key/{pEncodedKeyTarget}/{pStatusToChange}")
    public ResponseEntity<KeyTextEncodedPreGeneratedDTO> changeStatusEncodedKey(@PathVariable String pEncodedKeyTarget, @PathVariable String pStatusToChange) {

        KeyTextEncodedPreGeneratedDTO objResult = null;

        log.debug("REST request to change the KeyTextEncodedPreGenerated state to : '" + pStatusToChange + "'");

        objResult = keyTextEncodedPreGeneratedService.changeStatusEncodedKey(pEncodedKeyTarget, KeyStatusEnum.valueOf(pStatusToChange));

        return ResponseEntity.ok( (objResult == null ? (new KeyTextEncodedPreGeneratedDTO()) : objResult));

    }


    /**
     * Delete all KeyTextEncodedPreGenerateds or only changes it's states to 'INACTIVE'.
     *
     * @return The Encoded Key available and 201 Status Code
     */
    @ApiOperation(value = "Delete all KeyTextEncodedPreGenerateds or only changes it's states to 'INACTIVE'", nickname = "deleteAllOrMarkAllAsInactive", notes = "Delete all KeyTextEncodedPreGenerateds or only changes it's states to 'INACTIVE'")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "The operation as successfull"),
            @ApiResponse(code = 400, message = "Invalid input, object invalid"),
            @ApiResponse(code = 404, message = "The key was not found") })
    @DeleteMapping(value = "/delete-or-inactivate-all-encoded-keys/{pDontDeleteAllJustMarkInactive}")
    public ResponseEntity<Integer> deleteAllOrMarkAllAsInactive(@PathVariable Boolean pDontDeleteAllJustMarkInactive) {

        Integer intResult = null;

        log.debug("REST request to delete all KeyTextEncodedPreGenerateds or only changes it's states to 'INACTIVE'");

        intResult = keyTextEncodedPreGeneratedService.deleteAllOrMarkAllAsInactive(pDontDeleteAllJustMarkInactive);

        return ResponseEntity.ok(intResult);

    }


    /**
     * GET  /key-text-encoded-pre-generateds : get all the keyTextEncodedPreGenerateds.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of keyTextEncodedPreGenerateds in body
     */
    @GetMapping("/key-text-encoded-pre-generateds")
    public ResponseEntity<List<KeyTextEncodedPreGeneratedDTO>> getAllKeyTextEncodedPreGenerateds(Pageable pageable) {
        log.debug("REST request to get a page of KeyTextEncodedPreGenerateds");
        Page<KeyTextEncodedPreGeneratedDTO> page = keyTextEncodedPreGeneratedService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/key-text-encoded-pre-generateds");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

}
