package br.org.codingtests.alnsneuedaurlshortener.ms02textencoder.service.algorithms;

/**
 * Common interface for URL Shortener algorithms to pluggable.
 */
public interface IMakeShortURLAlgorithm {

    public String generateOnlyShortURL();

    public String makeShortURLFromLongURL(String pLongURL);

}
