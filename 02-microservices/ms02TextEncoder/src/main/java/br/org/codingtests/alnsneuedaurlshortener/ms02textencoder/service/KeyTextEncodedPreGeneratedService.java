package br.org.codingtests.alnsneuedaurlshortener.ms02textencoder.service;

import br.org.codingtests.alnsneuedaurlshortener.ms02textencoder.domain.enumeration.AlgorithmShorteningURLEnum;
import br.org.codingtests.alnsneuedaurlshortener.ms02textencoder.domain.enumeration.KeyStatusEnum;
import br.org.codingtests.alnsneuedaurlshortener.ms02textencoder.service.dto.KeyTextEncodedPreGeneratedDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Service Interface for managing KeyTextEncodedPreGenerated.
 */
public interface KeyTextEncodedPreGeneratedService {


    /**
     * Get all the keyTextEncodedPreGenerateds.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<KeyTextEncodedPreGeneratedDTO> findAll(Pageable pageable);


    List<KeyTextEncodedPreGeneratedDTO> createAndSaveKeyEncodedUsingDifferentAlgorithms();


    String obtainAndReserveNextEncodedKeyAvailableByAlgorithm(AlgorithmShorteningURLEnum pAlgorithmTarget);


    KeyTextEncodedPreGeneratedDTO changeStatusEncodedKey(String pEncodedKeyTarget, KeyStatusEnum pKeyStatusEnum);


    Integer deleteAllOrMarkAllAsInactive(Boolean pDontDeleteAllJustMarkInactive);

}




