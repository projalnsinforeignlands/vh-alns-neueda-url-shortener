# ===================================================================
# Spring Boot configuration for the "prod" profile.
#
# This configuration overrides the application.yml file.
#
# More information on profiles: https://www.jhipster.tech/profiles/
# More information on configuration properties: https://www.jhipster.tech/common-application-properties/
# ===================================================================

# ===================================================================
# Standard Spring Boot properties.
# Full reference is available at:
# http://docs.spring.io/spring-boot/docs/current/reference/html/common-application-properties.html
# ===================================================================

server:
    port: 8082
    servlet:
        session:
            cookie:
                http-only: true
    compression:
        enabled: true
        mime-types: text/html,text/xml,text/plain,text/css, application/javascript, application/json
        min-response-size: 1024
    # ===================================================================
    # Activate this profile to enable TLS and HTTP/2.
    #
    # JHipster has generated a self-signed certificate, which will be used to encrypt traffic.
    # As your browser will not understand this certificate, you will need to import it.
    #
    # Another (easiest) solution with Chrome is to enable the "allow-insecure-localhost" flag
    # at chrome://flags/#allow-insecure-localhost
    # ===================================================================
    # ssl:
    #    key-store: classpath:config/tls/keystore.p12
    #    key-store-password: password
    #    key-store-type: PKCS12
    #    key-alias: selfsigned
    #    ciphers: TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256, TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384, TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA, TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA, TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA256, TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA384, TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256, TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384, TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA, TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA, TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA256, TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA384, TLS_DHE_RSA_WITH_AES_128_GCM_SHA256, TLS_DHE_RSA_WITH_AES_256_GCM_SHA384, TLS_DHE_RSA_WITH_AES_128_CBC_SHA, TLS_DHE_RSA_WITH_AES_256_CBC_SHA, TLS_DHE_RSA_WITH_AES_128_CBC_SHA256, TLS_DHE_RSA_WITH_AES_256_CBC_SHA256
    #    enabled-protocols: TLSv1.2

spring:
    application:
        name: ms02TextEncoder

    data:
        mongodb:
            uri: mongodb://localhost:27017
            database: DBTextEncoder

    profiles:
        active: prod
        include:
            - swagger
            # Uncomment to activate TLS for the dev profile
            #- tls
    messages:
        basename: i18n/messages
        cache-duration: PT1S # 1 second, see the ISO 8601 standard

    devtools:
        restart:
            enabled: false
        livereload:
            enabled: false

    jackson:
        serialization:
            indent-output: true

logging:
    level:
        ROOT: INFO
        br.org.codingtests.alnsneuedaurlshortener.ms02textencoder: INFO
        io.github.jhipster: INFO


eureka:
    client:
        enabled: true
        service-url:
            defaultZone: http://admin:${jhipster.registry.password}@localhost:8761/eureka/
        healthcheck:
            enabled: true
        fetch-registry: true
        register-with-eureka: true
        instance-info-replication-interval-seconds: 10
        registry-fetch-interval-seconds: 10
    instance:
        appname: ms02textencoder
        prefer-ip-address: true
        instanceId: ms02textencoder:${spring.application.instance-id:${random.value}}
        lease-renewal-interval-in-seconds: 5
        lease-expiration-duration-in-seconds: 10
        status-page-url-path: ${management.endpoints.web.base-path}/info
        health-check-url-path: ${management.endpoints.web.base-path}/health
        metadata-map:
            zone: primary # This is needed for the load balancer
            profile: ${spring.profiles.active}
            version: ${info.project.version:}
            git-version: ${git.commit.id.describe:}
            git-commit: ${git.commit.id.abbrev:}
            git-branch: ${git.branch:}

    # ribbon:
    #     eureka:
    #       enabled: true
# feign:
#     hystrix:
#         enabled: true
#     client:
#         config:
#             default:
#                 connectTimeout: 5000
#                 readTimeout: 5000

# See https://github.com/Netflix/Hystrix/wiki/Configuration
hystrix:
    command:
        default:
            execution:
                isolation:
                    strategy: SEMAPHORE
    # See https://github.com/spring-cloud/spring-cloud-netflix/issues/1330
    #                    thread:
    #                        timeoutInMilliseconds: 10000
    shareSecurityContext: true

# Properties to be exposed on the /info management endpoint
info:
    # Comma separated list of profiles that will trigger the ribbon to show
    display-ribbon-on-profiles: "prod"

# ===================================================================
# JHipster specific properties
#
# Full reference is available at: https://www.jhipster.tech/common-application-properties/
# ===================================================================

jhipster:
    http:
        version: V_1_1 # To use HTTP/2 you will need SSL support (see above the "server.ssl" configuration)
        cache: # Used by the CachingHttpHeadersFilter
            timeToLiveInDays: 1461
    cache: # Cache configuration
        ehcache: # Ehcache configuration
            time-to-live-seconds: 3600 # By default objects stay 1 hour in the cache
            max-entries: 1000 # Number of objects in each cache entry
    metrics:
        logs: # Reports metrics in the logs
            enabled: false
            report-frequency: 60 # in seconds
    logging:
        logstash: # Forward logs to logstash over a socket, used by LoggingConfiguration
            enabled: false
            host: localhost
            port: 5000
            queue-size: 512
    async:
        core-pool-size: 2
        max-pool-size: 50
        queue-capacity: 10000
        # By default CORS is disabled. Uncomment to enable.
        #cors:
        #allowed-origins: "*"
        #allowed-methods: "*"
        #allowed-headers: "*"
        #exposed-headers: "Authorization,Link,X-Total-Count"
        #allow-credentials: true
        #max-age: 1800
    swagger:
        default-include-pattern: /api/.*
        title: VH-ALNS-Neueda-ShorterURLAPI
        description: CodingTest for Neueda '::' Java '::' By Andre Nascimento '::' API documentation
        version: 0.0.1
        terms-of-service-url:
        contact-name:
        contact-url:
        contact-email:
        license:
        license-url:

management:
    endpoints:
        web:
            base-path: /management

# ===================================================================
# Application specific properties
# Add your own application properties here, see the ApplicationProperties class
# to have type-safe configuration, like in the JHipsterProperties above
#
# More documentation is available at:
# https://www.jhipster.tech/common-application-properties/
# ===================================================================

# application:
