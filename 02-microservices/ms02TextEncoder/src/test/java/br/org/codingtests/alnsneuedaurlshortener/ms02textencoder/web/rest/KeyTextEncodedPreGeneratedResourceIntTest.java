package br.org.codingtests.alnsneuedaurlshortener.ms02textencoder.web.rest;

import br.org.codingtests.alnsneuedaurlshortener.ms02textencoder.Ms02TextEncoderApp;
import br.org.codingtests.alnsneuedaurlshortener.ms02textencoder.domain.KeyTextEncodedPreGenerated;
import br.org.codingtests.alnsneuedaurlshortener.ms02textencoder.domain.enumeration.KeyStatusEnum;
import br.org.codingtests.alnsneuedaurlshortener.ms02textencoder.repository.KeyTextEncodedPreGeneratedRepository;
import br.org.codingtests.alnsneuedaurlshortener.ms02textencoder.service.KeyTextEncodedPreGeneratedService;
import br.org.codingtests.alnsneuedaurlshortener.ms02textencoder.service.dto.KeyTextEncodedPreGeneratedDTO;
import br.org.codingtests.alnsneuedaurlshortener.ms02textencoder.service.mapper.KeyTextEncodedPreGeneratedMapper;
import br.org.codingtests.alnsneuedaurlshortener.ms02textencoder.web.rest.errors.ExceptionTranslator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.validation.Validator;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static br.org.codingtests.alnsneuedaurlshortener.ms02textencoder.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
/**
 * Test class for the KeyTextEncodedPreGeneratedResource REST controller.
 *
 * @see KeyTextEncodedPreGeneratedResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {Ms02TextEncoderApp.class})
public class KeyTextEncodedPreGeneratedResourceIntTest {

    private static final Instant DEFAULT_INSTANT_CREATION = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_INSTANT_CREATION = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final KeyStatusEnum DEFAULT_STATUS = KeyStatusEnum.AVAILABLE;
    private static final KeyStatusEnum UPDATED_STATUS = KeyStatusEnum.RESERVED;

    @Autowired
    private KeyTextEncodedPreGeneratedRepository keyTextEncodedPreGeneratedRepository;

    @Autowired
    private KeyTextEncodedPreGeneratedMapper keyTextEncodedPreGeneratedMapper;

    @Autowired
    private KeyTextEncodedPreGeneratedService keyTextEncodedPreGeneratedService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private Validator validator;

    private MockMvc restKeyTextEncodedPreGeneratedMockMvc;

    private KeyTextEncodedPreGenerated keyTextEncodedPreGenerated;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final KeyTextEncodedPreGeneratedResource keyTextEncodedPreGeneratedResource = new KeyTextEncodedPreGeneratedResource(keyTextEncodedPreGeneratedService);
        this.restKeyTextEncodedPreGeneratedMockMvc = MockMvcBuilders.standaloneSetup(keyTextEncodedPreGeneratedResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static KeyTextEncodedPreGenerated createEntity() {
        KeyTextEncodedPreGenerated keyTextEncodedPreGenerated = new KeyTextEncodedPreGenerated()
            .instantCreation(DEFAULT_INSTANT_CREATION)
            .status(DEFAULT_STATUS);
        return keyTextEncodedPreGenerated;
    }

    @Before
    public void initTest() {
        keyTextEncodedPreGeneratedRepository.deleteAll();
        keyTextEncodedPreGenerated = createEntity();
    }

    @Test
    public void createKeyTextEncodedPreGenerated() throws Exception {
        int databaseSizeBeforeCreate = keyTextEncodedPreGeneratedRepository.findAll().size();

        // Create the KeyTextEncodedPreGenerated
        KeyTextEncodedPreGeneratedDTO keyTextEncodedPreGeneratedDTO = keyTextEncodedPreGeneratedMapper.toDto(keyTextEncodedPreGenerated);
        restKeyTextEncodedPreGeneratedMockMvc.perform(post("/api/key-text-encoded-pre-generateds")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(keyTextEncodedPreGeneratedDTO)))
            .andExpect(status().isCreated());

        // Validate the KeyTextEncodedPreGenerated in the database
        List<KeyTextEncodedPreGenerated> keyTextEncodedPreGeneratedList = keyTextEncodedPreGeneratedRepository.findAll();
        assertThat(keyTextEncodedPreGeneratedList).hasSize(databaseSizeBeforeCreate + 1);
        KeyTextEncodedPreGenerated testKeyTextEncodedPreGenerated = keyTextEncodedPreGeneratedList.get(keyTextEncodedPreGeneratedList.size() - 1);
        assertThat(testKeyTextEncodedPreGenerated.getInstantCreation()).isEqualTo(DEFAULT_INSTANT_CREATION);
        assertThat(testKeyTextEncodedPreGenerated.getStatus()).isEqualTo(DEFAULT_STATUS);
    }

    @Test
    public void createKeyTextEncodedPreGeneratedWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = keyTextEncodedPreGeneratedRepository.findAll().size();

        // Create the KeyTextEncodedPreGenerated with an existing ID
        keyTextEncodedPreGenerated.setId("existing_id");
        KeyTextEncodedPreGeneratedDTO keyTextEncodedPreGeneratedDTO = keyTextEncodedPreGeneratedMapper.toDto(keyTextEncodedPreGenerated);

        // An entity with an existing ID cannot be created, so this API call must fail
        restKeyTextEncodedPreGeneratedMockMvc.perform(post("/api/key-text-encoded-pre-generateds")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(keyTextEncodedPreGeneratedDTO)))
            .andExpect(status().isBadRequest());

        // Validate the KeyTextEncodedPreGenerated in the database
        List<KeyTextEncodedPreGenerated> keyTextEncodedPreGeneratedList = keyTextEncodedPreGeneratedRepository.findAll();
        assertThat(keyTextEncodedPreGeneratedList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    public void getAllKeyTextEncodedPreGenerateds() throws Exception {
        // Initialize the database
        keyTextEncodedPreGeneratedRepository.save(keyTextEncodedPreGenerated);

        // Get all the keyTextEncodedPreGeneratedList
        restKeyTextEncodedPreGeneratedMockMvc.perform(get("/api/key-text-encoded-pre-generateds?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(keyTextEncodedPreGenerated.getId())))
            .andExpect(jsonPath("$.[*].instantCreation").value(hasItem(DEFAULT_INSTANT_CREATION.toString())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())));
    }
    
    @Test
    public void getKeyTextEncodedPreGenerated() throws Exception {
        // Initialize the database
        keyTextEncodedPreGeneratedRepository.save(keyTextEncodedPreGenerated);

        // Get the keyTextEncodedPreGenerated
        restKeyTextEncodedPreGeneratedMockMvc.perform(get("/api/key-text-encoded-pre-generateds/{id}", keyTextEncodedPreGenerated.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(keyTextEncodedPreGenerated.getId()))
            .andExpect(jsonPath("$.instantCreation").value(DEFAULT_INSTANT_CREATION.toString()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()));
    }

    @Test
    public void getNonExistingKeyTextEncodedPreGenerated() throws Exception {
        // Get the keyTextEncodedPreGenerated
        restKeyTextEncodedPreGeneratedMockMvc.perform(get("/api/key-text-encoded-pre-generateds/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateKeyTextEncodedPreGenerated() throws Exception {
        // Initialize the database
        keyTextEncodedPreGeneratedRepository.save(keyTextEncodedPreGenerated);

        int databaseSizeBeforeUpdate = keyTextEncodedPreGeneratedRepository.findAll().size();

        // Update the keyTextEncodedPreGenerated
        KeyTextEncodedPreGenerated updatedKeyTextEncodedPreGenerated = keyTextEncodedPreGeneratedRepository.findById(keyTextEncodedPreGenerated.getId()).get();
        updatedKeyTextEncodedPreGenerated
            .instantCreation(UPDATED_INSTANT_CREATION)
            .status(UPDATED_STATUS);
        KeyTextEncodedPreGeneratedDTO keyTextEncodedPreGeneratedDTO = keyTextEncodedPreGeneratedMapper.toDto(updatedKeyTextEncodedPreGenerated);

        restKeyTextEncodedPreGeneratedMockMvc.perform(put("/api/key-text-encoded-pre-generateds")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(keyTextEncodedPreGeneratedDTO)))
            .andExpect(status().isOk());

        // Validate the KeyTextEncodedPreGenerated in the database
        List<KeyTextEncodedPreGenerated> keyTextEncodedPreGeneratedList = keyTextEncodedPreGeneratedRepository.findAll();
        assertThat(keyTextEncodedPreGeneratedList).hasSize(databaseSizeBeforeUpdate);
        KeyTextEncodedPreGenerated testKeyTextEncodedPreGenerated = keyTextEncodedPreGeneratedList.get(keyTextEncodedPreGeneratedList.size() - 1);
        assertThat(testKeyTextEncodedPreGenerated.getInstantCreation()).isEqualTo(UPDATED_INSTANT_CREATION);
        assertThat(testKeyTextEncodedPreGenerated.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    public void updateNonExistingKeyTextEncodedPreGenerated() throws Exception {
        int databaseSizeBeforeUpdate = keyTextEncodedPreGeneratedRepository.findAll().size();

        // Create the KeyTextEncodedPreGenerated
        KeyTextEncodedPreGeneratedDTO keyTextEncodedPreGeneratedDTO = keyTextEncodedPreGeneratedMapper.toDto(keyTextEncodedPreGenerated);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restKeyTextEncodedPreGeneratedMockMvc.perform(put("/api/key-text-encoded-pre-generateds")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(keyTextEncodedPreGeneratedDTO)))
            .andExpect(status().isBadRequest());

        // Validate the KeyTextEncodedPreGenerated in the database
        List<KeyTextEncodedPreGenerated> keyTextEncodedPreGeneratedList = keyTextEncodedPreGeneratedRepository.findAll();
        assertThat(keyTextEncodedPreGeneratedList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    public void deleteKeyTextEncodedPreGenerated() throws Exception {
        // Initialize the database
        keyTextEncodedPreGeneratedRepository.save(keyTextEncodedPreGenerated);

        int databaseSizeBeforeDelete = keyTextEncodedPreGeneratedRepository.findAll().size();

        // Delete the keyTextEncodedPreGenerated
        restKeyTextEncodedPreGeneratedMockMvc.perform(delete("/api/key-text-encoded-pre-generateds/{id}", keyTextEncodedPreGenerated.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<KeyTextEncodedPreGenerated> keyTextEncodedPreGeneratedList = keyTextEncodedPreGeneratedRepository.findAll();
        assertThat(keyTextEncodedPreGeneratedList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(KeyTextEncodedPreGenerated.class);
        KeyTextEncodedPreGenerated keyTextEncodedPreGenerated1 = new KeyTextEncodedPreGenerated();
        keyTextEncodedPreGenerated1.setId("id1");
        KeyTextEncodedPreGenerated keyTextEncodedPreGenerated2 = new KeyTextEncodedPreGenerated();
        keyTextEncodedPreGenerated2.setId(keyTextEncodedPreGenerated1.getId());
        assertThat(keyTextEncodedPreGenerated1).isEqualTo(keyTextEncodedPreGenerated2);
        keyTextEncodedPreGenerated2.setId("id2");
        assertThat(keyTextEncodedPreGenerated1).isNotEqualTo(keyTextEncodedPreGenerated2);
        keyTextEncodedPreGenerated1.setId(null);
        assertThat(keyTextEncodedPreGenerated1).isNotEqualTo(keyTextEncodedPreGenerated2);
    }

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(KeyTextEncodedPreGeneratedDTO.class);
        KeyTextEncodedPreGeneratedDTO keyTextEncodedPreGeneratedDTO1 = new KeyTextEncodedPreGeneratedDTO();
        keyTextEncodedPreGeneratedDTO1.setId("id1");
        KeyTextEncodedPreGeneratedDTO keyTextEncodedPreGeneratedDTO2 = new KeyTextEncodedPreGeneratedDTO();
        assertThat(keyTextEncodedPreGeneratedDTO1).isNotEqualTo(keyTextEncodedPreGeneratedDTO2);
        keyTextEncodedPreGeneratedDTO2.setId(keyTextEncodedPreGeneratedDTO1.getId());
        assertThat(keyTextEncodedPreGeneratedDTO1).isEqualTo(keyTextEncodedPreGeneratedDTO2);
        keyTextEncodedPreGeneratedDTO2.setId("id2");
        assertThat(keyTextEncodedPreGeneratedDTO1).isNotEqualTo(keyTextEncodedPreGeneratedDTO2);
        keyTextEncodedPreGeneratedDTO1.setId(null);
        assertThat(keyTextEncodedPreGeneratedDTO1).isNotEqualTo(keyTextEncodedPreGeneratedDTO2);
    }
}
