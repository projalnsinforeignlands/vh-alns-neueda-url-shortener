package br.org.codingtests.alnsneuedaurlshortener.ms03urlmanagement.repository.feignclients;


import br.org.codingtests.alnsneuedaurlshortener.ms03urlmanagement.repository.feignclients.dtos.KeyTextEncodedPreGeneratedDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(serviceId = "ms02textencoder")
public interface KeyTextEncoderPreGeneratedClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/api/change-status-encoded-key/{pEncodedKeyTarget}/{pStatusToChange}")
    KeyTextEncodedPreGeneratedDTO changeStatusEncodedKey(@PathVariable("pEncodedKeyTarget") String pEncodedKeyTarget, @PathVariable("pStatusToChange") String pStatusToChange);

}
