package br.org.codingtests.alnsneuedaurlshortener.ms03urlmanagement.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Properties specific to Ms 01 Users.
 * <p>
 * Properties are configured in the application.yml file.
 * See {@link io.github.jhipster.config.JHipsterProperties} for a good example.
 */
@ConfigurationProperties(prefix = "application", ignoreUnknownFields = false)
public class ApplicationProperties {

    private final Config config = new Config();

    public Config getConfig() {
        return config;
    }


    public static class Config {

        private final RedirectTo redirectTo = new RedirectTo();


        public RedirectTo getRedirectTo() {
            return redirectTo;
        }


        public static class RedirectTo {

            private String hostnameToRedirect = "http://localhost";
            private int portNumberToRedirect = 8080;
            private String contextPathNameToRedirect = "/short";

            public String getHostnameToRedirect() {
                return hostnameToRedirect;
            }
            public void setHostnameToRedirect(String hostnameToRedirect) {
                this.hostnameToRedirect = hostnameToRedirect;
            }

            public int getPortNumberToRedirect() {
                return portNumberToRedirect;
            }
            public void setPortNumberToRedirect(int portNumberToRedirect) {
                this.portNumberToRedirect = portNumberToRedirect;
            }

            public String getContextPathNameToRedirect() {
                return contextPathNameToRedirect;
            }
            public void setContextPathNameToRedirect(String contextPathNameToRedirect) {
                this.contextPathNameToRedirect = contextPathNameToRedirect;
            }
        }

    }


}
