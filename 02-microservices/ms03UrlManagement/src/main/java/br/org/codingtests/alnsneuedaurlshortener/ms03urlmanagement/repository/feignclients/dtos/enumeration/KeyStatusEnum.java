package br.org.codingtests.alnsneuedaurlshortener.ms03urlmanagement.repository.feignclients.dtos.enumeration;

/**
 * The KeyStatusEnum enumeration.
 */
public enum KeyStatusEnum {
    AVAILABLE, RESERVED, IN_USE, INACTIVE
}
