package br.org.codingtests.alnsneuedaurlshortener.ms03urlmanagement.repository;

import br.org.codingtests.alnsneuedaurlshortener.ms03urlmanagement.domain.StatisticsInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the StatisticsInfo entity.
 */

@Repository
public interface StatisticsInfoRepository extends JpaRepository<StatisticsInfo, Long> {

}
