package br.org.codingtests.alnsneuedaurlshortener.ms03urlmanagement.domain.enumeration;

/**
 * The AlgorithmShorteningURLEnum enumeration.
 */
public enum AlgorithmShorteningURLEnum {
    BASE64_UUID,
    GOOGLE_API
}
