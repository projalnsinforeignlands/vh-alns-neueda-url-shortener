package br.org.codingtests.alnsneuedaurlshortener.ms03urlmanagement.repository.feignclients.dtos;

import br.org.codingtests.alnsneuedaurlshortener.ms03urlmanagement.domain.enumeration.AlgorithmShorteningURLEnum;
import br.org.codingtests.alnsneuedaurlshortener.ms03urlmanagement.repository.feignclients.dtos.enumeration.KeyStatusEnum;
import io.swagger.annotations.ApiModel;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A DTO for the KeyTextEncodedPreGenerated entity.
 */
@ApiModel(description = "KeyTextEncodedPreGenerated entity. Stores texts encoded generated in a batch way. @author Andre Nascimento")
public class KeyTextEncodedPreGeneratedDTO implements Serializable {

    private String id;

    private Instant instantCreation;

    private KeyStatusEnum status;

    private AlgorithmShorteningURLEnum generationAlgorithmType;



    public KeyTextEncodedPreGeneratedDTO() {

    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public Instant getInstantCreation() {
        return instantCreation;
    }
    public void setInstantCreation(Instant instantCreation) {
        this.instantCreation = instantCreation;
    }

    public KeyStatusEnum getStatus() {
        return status;
    }
    public void setStatus(KeyStatusEnum status) {
        this.status = status;
    }

    public AlgorithmShorteningURLEnum getGenerationAlgorithmType() {
        return generationAlgorithmType;
    }
    public void setGenerationAlgorithmType(AlgorithmShorteningURLEnum generationAlgorithmType) {
        this.generationAlgorithmType = generationAlgorithmType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        KeyTextEncodedPreGeneratedDTO keyTextEncodedPreGeneratedDTO = (KeyTextEncodedPreGeneratedDTO) o;
        if (keyTextEncodedPreGeneratedDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), keyTextEncodedPreGeneratedDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "KeyTextEncodedPreGeneratedDTO{" +
            "id=" + getId() +
            ", instantCreation='" + getInstantCreation() + "'" +
            ", status='" + getStatus() + "'" +
            "}";
    }
}
