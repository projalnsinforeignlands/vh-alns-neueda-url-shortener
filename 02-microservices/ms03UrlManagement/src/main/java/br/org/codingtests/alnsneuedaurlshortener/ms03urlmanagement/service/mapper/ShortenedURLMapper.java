package br.org.codingtests.alnsneuedaurlshortener.ms03urlmanagement.service.mapper;

import br.org.codingtests.alnsneuedaurlshortener.ms03urlmanagement.domain.ShortenedURL;
import br.org.codingtests.alnsneuedaurlshortener.ms03urlmanagement.service.dto.ShortenedURLDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity ShortenedURL and its DTO ShortenedURLDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ShortenedURLMapper extends EntityMapper<ShortenedURLDTO, ShortenedURL> {


    @Mapping(target = "statisticsInfosList", ignore = true)
    ShortenedURL toEntity(ShortenedURLDTO shortenedURLDTO);

    default ShortenedURL fromId(Long id) {
        if (id == null) {
            return null;
        }
        ShortenedURL shortenedURL = new ShortenedURL();
        shortenedURL.setId(id);
        return shortenedURL;
    }
}
