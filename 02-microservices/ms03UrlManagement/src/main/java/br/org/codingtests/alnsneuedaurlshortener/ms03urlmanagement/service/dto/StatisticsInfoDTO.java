package br.org.codingtests.alnsneuedaurlshortener.ms03urlmanagement.service.dto;

import br.org.codingtests.alnsneuedaurlshortener.ms03urlmanagement.domain.enumeration.ReasonCallEnum;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

/**
 * A DTO for the StatisticsInfo entity.
 */
public class StatisticsInfoDTO implements Serializable {

    private Long id;

    @NotNull
    private ReasonCallEnum reasonCallDoneType;

    @NotNull
    private LocalDateTime dateAccess;

    @Size(max = 20)
    private String numberIPAddressCaller;

    @Size(max = 2)
    private String countryCallerCode;

    private Long shortenedURLId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ReasonCallEnum getReasonCallDoneType() {
        return reasonCallDoneType;
    }

    public void setReasonCallDoneType(ReasonCallEnum reasonCallDoneType) {
        this.reasonCallDoneType = reasonCallDoneType;
    }

    public LocalDateTime getDateAccess() {
        return dateAccess;
    }

    public void setDateAccess(LocalDateTime dateAccess) {
        this.dateAccess = dateAccess;
    }

    public String getNumberIPAddressCaller() {
        return numberIPAddressCaller;
    }

    public void setNumberIPAddressCaller(String numberIPAddressCaller) {
        this.numberIPAddressCaller = numberIPAddressCaller;
    }

    public String getCountryCallerCode() {
        return countryCallerCode;
    }

    public void setCountryCallerCode(String countryCallerCode) {
        this.countryCallerCode = countryCallerCode;
    }

    public Long getShortenedURLId() {
        return shortenedURLId;
    }

    public void setShortenedURLId(Long shortenedURLId) {
        this.shortenedURLId = shortenedURLId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        StatisticsInfoDTO statisticsInfoDTO = (StatisticsInfoDTO) o;
        if (statisticsInfoDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), statisticsInfoDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "StatisticsInfoDTO{" +
            "id=" + getId() +
            ", reasonCallDoneType='" + getReasonCallDoneType() + "'" +
            ", dateAccess='" + getDateAccess() + "'" +
            ", numberIPAddressCaller='" + getNumberIPAddressCaller() + "'" +
            ", countryCallerCode='" + getCountryCallerCode() + "'" +
            ", shortenedURL=" + getShortenedURLId() +
            "}";
    }
}
