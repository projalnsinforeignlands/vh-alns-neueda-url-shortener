package br.org.codingtests.alnsneuedaurlshortener.ms03urlmanagement.web.rest;

import br.org.codingtests.alnsneuedaurlshortener.ms03urlmanagement.service.StatisticsInfoService;
import br.org.codingtests.alnsneuedaurlshortener.ms03urlmanagement.service.dto.StatisticsInfoDTO;
import br.org.codingtests.alnsneuedaurlshortener.ms03urlmanagement.web.rest.errors.BadRequestAlertException;
import br.org.codingtests.alnsneuedaurlshortener.ms03urlmanagement.web.rest.util.HeaderUtil;
import br.org.codingtests.alnsneuedaurlshortener.ms03urlmanagement.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import io.micrometer.core.annotation.Timed;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing StatisticsInfo.
 */
@RestController
@RequestMapping("/api")
public class StatisticsInfoResource {

    private final Logger log = LoggerFactory.getLogger(StatisticsInfoResource.class);

    private static final String ENTITY_NAME = "statisticsInfo";

    private final StatisticsInfoService statisticsInfoService;

    public StatisticsInfoResource(StatisticsInfoService statisticsInfoService) {
        this.statisticsInfoService = statisticsInfoService;
    }

    /**
     * POST  /statistics-infos : Create a new statisticsInfo.
     *
     * @param statisticsInfoDTO the statisticsInfoDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new statisticsInfoDTO, or with status 400 (Bad Request) if the statisticsInfo has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/statistics-infos")
    @Timed
    public ResponseEntity<StatisticsInfoDTO> createStatisticsInfo(@Valid @RequestBody StatisticsInfoDTO statisticsInfoDTO) throws URISyntaxException {
        log.debug("REST request to save StatisticsInfo : {}", statisticsInfoDTO);
        if (statisticsInfoDTO.getId() != null) {
            throw new BadRequestAlertException("A new statisticsInfo cannot already have an ID", ENTITY_NAME, "idexists");
        }
        StatisticsInfoDTO result = statisticsInfoService.save(statisticsInfoDTO);
        return ResponseEntity.created(new URI("/api/statistics-infos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /statistics-infos : Updates an existing statisticsInfo.
     *
     * @param statisticsInfoDTO the statisticsInfoDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated statisticsInfoDTO,
     * or with status 400 (Bad Request) if the statisticsInfoDTO is not valid,
     * or with status 500 (Internal Server Error) if the statisticsInfoDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/statistics-infos")
    @Timed
    public ResponseEntity<StatisticsInfoDTO> updateStatisticsInfo(@Valid @RequestBody StatisticsInfoDTO statisticsInfoDTO) throws URISyntaxException {
        log.debug("REST request to update StatisticsInfo : {}", statisticsInfoDTO);
        if (statisticsInfoDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        StatisticsInfoDTO result = statisticsInfoService.save(statisticsInfoDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, statisticsInfoDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /statistics-infos : get all the statisticsInfos.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of statisticsInfos in body
     */
    @GetMapping("/statistics-infos")
    @Timed
    public ResponseEntity<List<StatisticsInfoDTO>> getAllStatisticsInfos(Pageable pageable) {
        log.debug("REST request to get a page of StatisticsInfos");
        Page<StatisticsInfoDTO> page = statisticsInfoService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/statistics-infos");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /statistics-infos/:id : get the "id" statisticsInfo.
     *
     * @param id the id of the statisticsInfoDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the statisticsInfoDTO, or with status 404 (Not Found)
     */
    @GetMapping("/statistics-infos/{id}")
    @Timed
    public ResponseEntity<StatisticsInfoDTO> getStatisticsInfo(@PathVariable Long id) {
        log.debug("REST request to get StatisticsInfo : {}", id);
        Optional<StatisticsInfoDTO> statisticsInfoDTO = statisticsInfoService.findOne(id);
        return ResponseUtil.wrapOrNotFound(statisticsInfoDTO);
    }

    /**
     * DELETE  /statistics-infos/:id : delete the "id" statisticsInfo.
     *
     * @param id the id of the statisticsInfoDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/statistics-infos/{id}")
    @Timed
    public ResponseEntity<Void> deleteStatisticsInfo(@PathVariable Long id) {
        log.debug("REST request to delete StatisticsInfo : {}", id);
        statisticsInfoService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }


    /**
     * GET  /shortened-urls : get the shortenedURLS by UserId.
     *
     * @param pPageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of shortenedURLS in body
     */
    @GetMapping("/statistics-infos/_search-by-shortenedurlid/{pShortenedURLId}")
    public ResponseEntity<List<StatisticsInfoDTO>> searchByShortenedURLId(@PathVariable("pShortenedURLId") Long pShortenedURLId, Pageable pPageable) {

        List<StatisticsInfoDTO> resultList = null;

        log.debug("REST Request to Search the StatisticsInfos by ShortenedURL.Id: " + pShortenedURLId);

        Page<StatisticsInfoDTO> page = statisticsInfoService.searchByShortenerURLId(pShortenedURLId, pPageable);

        resultList = page.getContent();

        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/statistics-infos/_search-by-shortenedurlid/{pShortenedURLId}");

        return ResponseEntity.ok().headers(headers).body(resultList);

    }




}
