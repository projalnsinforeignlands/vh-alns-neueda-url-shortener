package br.org.codingtests.alnsneuedaurlshortener.ms03urlmanagement.repository;

import br.org.codingtests.alnsneuedaurlshortener.ms03urlmanagement.domain.ShortenedURL;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


/**
 * Spring Data  repository for the ShortenedURL entity.
 */

@Repository
public interface ShortenedURLRepository extends JpaRepository<ShortenedURL, Long> {

    Optional<ShortenedURL> findFirstByDescriptionShortenedURL(String pEncodedKey);



}
