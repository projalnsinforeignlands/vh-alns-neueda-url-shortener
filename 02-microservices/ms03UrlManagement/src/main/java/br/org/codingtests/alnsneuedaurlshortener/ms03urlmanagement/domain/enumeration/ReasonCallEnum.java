package br.org.codingtests.alnsneuedaurlshortener.ms03urlmanagement.domain.enumeration;

/**
 * The ReasonCallEnum enumeration.
 */
public enum ReasonCallEnum {
    TO_REDIRECT, JUST_DECODING
}
