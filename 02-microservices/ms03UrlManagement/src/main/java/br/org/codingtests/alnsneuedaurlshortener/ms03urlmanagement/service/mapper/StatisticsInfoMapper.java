package br.org.codingtests.alnsneuedaurlshortener.ms03urlmanagement.service.mapper;

import br.org.codingtests.alnsneuedaurlshortener.ms03urlmanagement.domain.StatisticsInfo;
import br.org.codingtests.alnsneuedaurlshortener.ms03urlmanagement.service.dto.StatisticsInfoDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity StatisticsInfo and its DTO StatisticsInfoDTO.
 */
@Mapper(componentModel = "spring", uses = {ShortenedURLMapper.class})
public interface StatisticsInfoMapper extends EntityMapper<StatisticsInfoDTO, StatisticsInfo> {

    @Mapping(source = "shortenedURL.id", target = "shortenedURLId")
    StatisticsInfoDTO toDto(StatisticsInfo statisticsInfo);

    @Mapping(source = "shortenedURLId", target = "shortenedURL")
    StatisticsInfo toEntity(StatisticsInfoDTO statisticsInfoDTO);

    default StatisticsInfo fromId(Long id) {
        if (id == null) {
            return null;
        }
        StatisticsInfo statisticsInfo = new StatisticsInfo();
        statisticsInfo.setId(id);
        return statisticsInfo;
    }
}
