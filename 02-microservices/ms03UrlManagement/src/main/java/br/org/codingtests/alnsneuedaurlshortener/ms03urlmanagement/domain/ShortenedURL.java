package br.org.codingtests.alnsneuedaurlshortener.ms03urlmanagement.domain;

import br.org.codingtests.alnsneuedaurlshortener.ms03urlmanagement.domain.enumeration.AlgorithmShorteningURLEnum;
import io.swagger.annotations.ApiModel;
import org.hibernate.validator.constraints.URL;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * URLShortened entity.
 * Stores the long URL address translated to short
 * @author Andre Nascimento
 */
@ApiModel(description = "URLShortened entity. Stores the long URL address translated to short @author Andre Nascimento")
@Entity
@Table(name = "TB_SHORTENED_URL")
public class ShortenedURL implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "USER_ID", nullable = false)
    private Long userId;

    @NotNull
    @URL
    @Size(min = 10, max = 800)
    @Column(name = "DES_LONG_URL", length = 800, nullable = false)
    private String descriptionLongURL;

    @Size(max = 40)
    @Column(name = "DES_SHORTENED_URL", length = 40, unique = true, nullable = false)
    private String descriptionShortenedURL;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "TYP_GENERATION_ALGORITHM", nullable = false)
    private AlgorithmShorteningURLEnum generationAlgorithmType;

    @NotNull
    @Column(name = "DTH_ENCODING", nullable = false)
    private LocalDateTime dateEncoding;

    @Column(name = "DTH_LAST_ACCESS")
    private LocalDateTime dateLastAccess;

    @NotNull
    @Column(name = "IND_ACTIVE", nullable = false)
    private Boolean isActive;

    @Column(name = "QTY_DECODING_REQUESTS_BY_API")
    private Integer qtyDecodingRequestsByAPI;

    @Column(name = "QTY_CALLS_FOR_REDIRECTING")
    private Integer qtyCallsForRedirecting;

    @OneToMany(mappedBy = "shortenedURL", cascade = CascadeType.ALL)
    private List<StatisticsInfo> statisticsInfosList = new ArrayList<>();
    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    
    
    public ShortenedURL() {    	
    	
    }
    
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public ShortenedURL userId(Long userId) {
        this.userId = userId;
        return this;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getDescriptionLongURL() {
        return descriptionLongURL;
    }

    public ShortenedURL descriptionLongURL(String descriptionLongURL) {
        this.descriptionLongURL = descriptionLongURL;
        return this;
    }

    public void setDescriptionLongURL(String descriptionLongURL) {
        this.descriptionLongURL = descriptionLongURL;
    }

    public String getDescriptionShortenedURL() {
        return descriptionShortenedURL;
    }

    public ShortenedURL descriptionShortenedURL(String descriptionShortenedURL) {
        this.descriptionShortenedURL = descriptionShortenedURL;
        return this;
    }

    public void setDescriptionShortenedURL(String descriptionShortenedURL) {
        this.descriptionShortenedURL = descriptionShortenedURL;
    }

    public AlgorithmShorteningURLEnum getGenerationAlgorithmType() {
        return generationAlgorithmType;
    }

    public ShortenedURL generationAlgorithmType(AlgorithmShorteningURLEnum generationAlgorithmType) {
        this.generationAlgorithmType = generationAlgorithmType;
        return this;
    }

    public void setGenerationAlgorithmType(AlgorithmShorteningURLEnum generationAlgorithmType) {
        this.generationAlgorithmType = generationAlgorithmType;
    }

    public LocalDateTime getDateEncoding() {
        return dateEncoding;
    }

    public ShortenedURL dateEncoding(LocalDateTime dateEncoding) {
        this.dateEncoding = dateEncoding;
        return this;
    }

    public void setDateEncoding(LocalDateTime dateEncoding) {
        this.dateEncoding = dateEncoding;
    }

    public LocalDateTime getDateLastAccess() {
        return dateLastAccess;
    }

    public ShortenedURL dateLastAccess(LocalDateTime dateLastAccess) {
        this.dateLastAccess = dateLastAccess;
        return this;
    }

    public void setDateLastAccess(LocalDateTime dateLastAccess) {
        this.dateLastAccess = dateLastAccess;
    }

    public Boolean isIsActive() {
        return isActive;
    }

    public ShortenedURL isActive(Boolean isActive) {
        this.isActive = isActive;
        return this;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Integer getQtyDecodingRequestsByAPI() {
        return qtyDecodingRequestsByAPI;
    }

    public ShortenedURL qtyDecodingRequestsByAPI(Integer qtyDecodingRequestsByAPI) {
        this.qtyDecodingRequestsByAPI = qtyDecodingRequestsByAPI;
        return this;
    }

    public void setQtyDecodingRequestsByAPI(Integer qtyDecodingRequestsByAPI) {
        this.qtyDecodingRequestsByAPI = qtyDecodingRequestsByAPI;
    }

    public Integer getQtyCallsForRedirecting() {
        return qtyCallsForRedirecting;
    }

    public ShortenedURL qtyCallsForRedirecting(Integer qtyCallsForRedirecting) {
        this.qtyCallsForRedirecting = qtyCallsForRedirecting;
        return this;
    }

    public void setQtyCallsForRedirecting(Integer qtyCallsForRedirecting) {
        this.qtyCallsForRedirecting = qtyCallsForRedirecting;
    }

    public List<StatisticsInfo> getStatisticsInfosList() {
        return statisticsInfosList;
    }

    public ShortenedURL statisticsInfos(List<StatisticsInfo> statisticsInfos) {
        this.statisticsInfosList = statisticsInfos;
        return this;
    }

    public ShortenedURL addStatisticsInfo(StatisticsInfo statisticsInfo) {
        this.statisticsInfosList.add(statisticsInfo);
        statisticsInfo.setShortenedURL(this);
        return this;
    }

    public ShortenedURL removeStatisticsInfo(StatisticsInfo statisticsInfo) {
        this.statisticsInfosList.remove(statisticsInfo);
        statisticsInfo.setShortenedURL(null);
        return this;
    }

    public void setStatisticsInfosList(List<StatisticsInfo> statisticsInfos) {
        this.statisticsInfosList = statisticsInfos;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ShortenedURL shortenedURL = (ShortenedURL) o;
        if (shortenedURL.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), shortenedURL.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ShortenedURL{" +
            "id=" + getId() +
            ", userId=" + getUserId() +
            ", descriptionLongURL='" + getDescriptionLongURL() + "'" +
            ", descriptionShortenedURL='" + getDescriptionShortenedURL() + "'" +
            ", generationAlgorithmType='" + getGenerationAlgorithmType() + "'" +
            ", dateEncoding='" + getDateEncoding() + "'" +
            ", dateLastAccess='" + getDateLastAccess() + "'" +
            ", isActive='" + isIsActive() + "'" +
            ", qtyDecodingRequestsByAPI=" + getQtyDecodingRequestsByAPI() +
            ", qtyCallsForRedirecting=" + getQtyCallsForRedirecting() +
            "}";
    }

    public void addQtyCallsForRedirecting(int pQty) {
        this.qtyCallsForRedirecting += pQty;
    }
}
