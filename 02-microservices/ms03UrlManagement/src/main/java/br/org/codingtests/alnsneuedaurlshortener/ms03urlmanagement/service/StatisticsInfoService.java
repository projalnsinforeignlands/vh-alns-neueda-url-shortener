package br.org.codingtests.alnsneuedaurlshortener.ms03urlmanagement.service;

import br.org.codingtests.alnsneuedaurlshortener.ms03urlmanagement.service.dto.StatisticsInfoDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing StatisticsInfo.
 */
public interface StatisticsInfoService {

    /**
     * Save a statisticsInfo.
     *
     * @param statisticsInfoDTO the entity to save
     * @return the persisted entity
     */
    StatisticsInfoDTO save(StatisticsInfoDTO statisticsInfoDTO);

    /**
     * Get all the statisticsInfos.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<StatisticsInfoDTO> findAll(Pageable pageable);


    /**
     * Get the "id" statisticsInfo.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<StatisticsInfoDTO> findOne(Long id);

    /**
     * Delete the "id" statisticsInfo.
     *
     * @param id the id of the entity
     */
    void delete(Long id);


    /**
     * Search the StatisticsInfo's by ShortenerURL.Id.
     *
     * @param pShortenerURLId the id of the ShortenerURL selected.
     * @param pPageable the paging control.
     * @return the list of entities
     */
    Page<StatisticsInfoDTO> searchByShortenerURLId(Long pShortenerURLId, Pageable pPageable);

}
