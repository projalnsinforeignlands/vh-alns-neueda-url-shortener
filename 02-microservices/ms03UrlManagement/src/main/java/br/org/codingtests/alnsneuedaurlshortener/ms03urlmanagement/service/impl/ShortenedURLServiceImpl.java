package br.org.codingtests.alnsneuedaurlshortener.ms03urlmanagement.service.impl;

import br.org.codingtests.alnsneuedaurlshortener.ms03urlmanagement.domain.ShortenedURL;
import br.org.codingtests.alnsneuedaurlshortener.ms03urlmanagement.repository.ShortenedURLRepository;
import br.org.codingtests.alnsneuedaurlshortener.ms03urlmanagement.repository.feignclients.KeyTextEncoderPreGeneratedClient;
import br.org.codingtests.alnsneuedaurlshortener.ms03urlmanagement.repository.feignclients.dtos.SimpleParametersWrapperDTO;
import br.org.codingtests.alnsneuedaurlshortener.ms03urlmanagement.repository.feignclients.dtos.enumeration.KeyStatusEnum;
import br.org.codingtests.alnsneuedaurlshortener.ms03urlmanagement.service.ShortenedURLService;
import br.org.codingtests.alnsneuedaurlshortener.ms03urlmanagement.service.StatisticsInfoService;
import br.org.codingtests.alnsneuedaurlshortener.ms03urlmanagement.service.dto.ShortenedURLDTO;
import br.org.codingtests.alnsneuedaurlshortener.ms03urlmanagement.service.dto.StatisticsInfoDTO;
import br.org.codingtests.alnsneuedaurlshortener.ms03urlmanagement.service.mapper.ShortenedURLMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing ShortenedURL.
 */
@Service
@Transactional
public class ShortenedURLServiceImpl implements ShortenedURLService {

    private final Logger log = LoggerFactory.getLogger(ShortenedURLServiceImpl.class);

    private final ShortenedURLRepository shortenedURLRepository;

    private final ShortenedURLMapper shortenedURLMapper;

    private final StatisticsInfoService statisticsInfoService;

    private final KeyTextEncoderPreGeneratedClient keyTextEncoderPreGeneratedClient;


    public ShortenedURLServiceImpl(ShortenedURLRepository shortenedURLRepository,
                                   ShortenedURLMapper shortenedURLMapper,
                                   StatisticsInfoService pStatisticsInfoService,
                                   KeyTextEncoderPreGeneratedClient pKeyTextEncoderPreGeneratedClient) {

        this.shortenedURLRepository = shortenedURLRepository;
        this.shortenedURLMapper = shortenedURLMapper;
        this.statisticsInfoService = pStatisticsInfoService;
        this.keyTextEncoderPreGeneratedClient = pKeyTextEncoderPreGeneratedClient;
    }

    /**
     * Save a shortenedURL.
     *
     * @param shortenedURLDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public ShortenedURLDTO save(ShortenedURLDTO shortenedURLDTO) {
        log.debug("Request to save ShortenedURL : {}", shortenedURLDTO);

        ShortenedURL shortenedURL = shortenedURLMapper.toEntity(shortenedURLDTO);
        shortenedURL = shortenedURLRepository.save(shortenedURL);

        keyTextEncoderPreGeneratedClient.changeStatusEncodedKey(shortenedURL.getDescriptionShortenedURL(), KeyStatusEnum.IN_USE.toString());

        return shortenedURLMapper.toDto(shortenedURL);
    }

    /**
     * Get all the shortenedURLS.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ShortenedURLDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ShortenedURLS");
        return shortenedURLRepository.findAll(pageable)
            .map(shortenedURLMapper::toDto);
    }


    /**
     * Get one shortenedURL by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ShortenedURLDTO> findOne(Long id) {
        log.debug("Request to get ShortenedURL : {}", id);
        return shortenedURLRepository.findById(id)
            .map(shortenedURLMapper::toDto);
    }

    /**
     * Delete the shortenedURL by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {

        String strEncodedKeyToBeDeleted = null;

        strEncodedKeyToBeDeleted = shortenedURLRepository.findById(id).get().getDescriptionShortenedURL();

        log.debug("Request to delete ShortenedURL : {}", id);

        shortenedURLRepository.deleteById(id);

        keyTextEncoderPreGeneratedClient.changeStatusEncodedKey(strEncodedKeyToBeDeleted, KeyStatusEnum.INACTIVE.toString());

    }

    
    @Override
    @Transactional
    public Optional<ShortenedURLDTO> searchByEncodedKey(String pEncodedKey, StatisticsInfoDTO pStatisticsInfoDTO) {

        Optional<ShortenedURL> optShortenedURLEntity = null;
        ShortenedURL shortenedURLTarget = null;


        log.debug("Request to get ShortenedURL by Encoded Key : {}", pEncodedKey);

        optShortenedURLEntity = shortenedURLRepository.findFirstByDescriptionShortenedURL(pEncodedKey);

        if (optShortenedURLEntity.isPresent()) {

            shortenedURLTarget = optShortenedURLEntity.get();

            shortenedURLTarget.setDateLastAccess(LocalDateTime.now());
            shortenedURLTarget.addQtyCallsForRedirecting(1);
            shortenedURLRepository.saveAndFlush(shortenedURLTarget);

            pStatisticsInfoDTO.setShortenedURLId(shortenedURLTarget.getId());
            statisticsInfoService.save(pStatisticsInfoDTO);

        }

        return optShortenedURLEntity.map(shortenedURLMapper::toDto);

    }


    /**
     * Search the shortenedURLS by UserId.
     *
     * @param pUserId the id of the User selected.
     * @param pPageable the paging control.
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ShortenedURLDTO> searchByUserId(Long pUserId, Pageable pPageable) {

        Example<ShortenedURL> exampleEntity = null;

        log.debug("Request to Search the shortenedURLS by UserId: " + pUserId);

        exampleEntity = Example.of(new ShortenedURL().userId(pUserId));

        return shortenedURLRepository.findAll(exampleEntity, pPageable)
                                     .map(shortenedURLMapper::toDto);
    }


    /**
     * Delete the shortenedURL by UserId.
     *
     * @param pUserId the id of the entity
     */
    @Override
    @Transactional
    public SimpleParametersWrapperDTO deleteChildrenByUserId(Long pUserId) {

        SimpleParametersWrapperDTO simpleParamDTOResult = null;
        Example<ShortenedURL> exampleEntity = null;
        List<ShortenedURL> shortenedsURLsList = null;

        log.debug("Request to Delete the shortenedURLS by UserId: " + pUserId);

        simpleParamDTOResult = new SimpleParametersWrapperDTO();

        exampleEntity = Example.of(new ShortenedURL().userId(pUserId));

        shortenedsURLsList = shortenedURLRepository.findAll(exampleEntity);

        if (!CollectionUtils.isEmpty(shortenedsURLsList)) {

            simpleParamDTOResult.setSimpleParameterAsInteger(shortenedsURLsList.size());

            shortenedsURLsList.forEach( pOneShortURL -> keyTextEncoderPreGeneratedClient.changeStatusEncodedKey(pOneShortURL.getDescriptionShortenedURL(), KeyStatusEnum.INACTIVE.toString()));

            shortenedURLRepository.deleteAll(shortenedsURLsList);

            simpleParamDTOResult.setSimpleParameterAsString("ShortenedURL of the UserId: " + pUserId + " were succesfully deleted.");

        } else {

            simpleParamDTOResult.setSimpleParameterAsInteger(0);
            simpleParamDTOResult.setSimpleParameterAsString("None ShortenedURL of the UserId: " + pUserId + " were deleted.");

        }

        return simpleParamDTOResult;
    }



}
