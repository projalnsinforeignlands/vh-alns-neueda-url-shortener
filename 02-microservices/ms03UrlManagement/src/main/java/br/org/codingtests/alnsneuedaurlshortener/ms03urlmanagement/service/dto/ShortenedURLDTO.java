package br.org.codingtests.alnsneuedaurlshortener.ms03urlmanagement.service.dto;

import br.org.codingtests.alnsneuedaurlshortener.ms03urlmanagement.domain.enumeration.AlgorithmShorteningURLEnum;
import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

/**
 * A DTO for the ShortenedURL entity.
 */
public class ShortenedURLDTO implements Serializable {

    private Long id;

    private Long userId;

    @NotNull
    @URL
    @Size(min = 10, max = 800)
    private String descriptionLongURL;

    @Size(max = 40)
    private String descriptionShortenedURL;

    @NotNull
    private AlgorithmShorteningURLEnum generationAlgorithmType;

    @NotNull
    private LocalDateTime dateEncoding;

    private LocalDateTime dateLastAccess;

    @NotNull
    private Boolean isActive;

    private Integer qtyDecodingRequestsByAPI;

    private Integer qtyCallsForRedirecting;

    private String fullLinkToRedirect;


    public ShortenedURLDTO() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getDescriptionLongURL() {
        return descriptionLongURL;
    }

    public void setDescriptionLongURL(String descriptionLongURL) {
        this.descriptionLongURL = descriptionLongURL;
    }

    public String getDescriptionShortenedURL() {
        return descriptionShortenedURL;
    }

    public void setDescriptionShortenedURL(String descriptionShortenedURL) {
        this.descriptionShortenedURL = descriptionShortenedURL;
    }

    public AlgorithmShorteningURLEnum getGenerationAlgorithmType() {
        return generationAlgorithmType;
    }

    public void setGenerationAlgorithmType(AlgorithmShorteningURLEnum generationAlgorithmType) {
        this.generationAlgorithmType = generationAlgorithmType;
    }

    public LocalDateTime getDateEncoding() {
        return dateEncoding;
    }

    public void setDateEncoding(LocalDateTime dateEncoding) {
        this.dateEncoding = dateEncoding;
    }

    public LocalDateTime getDateLastAccess() {
        return dateLastAccess;
    }

    public void setDateLastAccess(LocalDateTime dateLastAccess) {
        this.dateLastAccess = dateLastAccess;
    }

    public Boolean isIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public String getFullLinkToRedirect() {
        return this.fullLinkToRedirect;
    }
    public void setFullLinkToRedirect(String pFullLinkToRedirect) {
        this.fullLinkToRedirect = pFullLinkToRedirect;
    }

    public Integer getQtyDecodingRequestsByAPI() {
        return qtyDecodingRequestsByAPI;
    }

    public void setQtyDecodingRequestsByAPI(Integer qtyDecodingRequestsByAPI) {
        this.qtyDecodingRequestsByAPI = qtyDecodingRequestsByAPI;
    }

    public Integer getQtyCallsForRedirecting() {
        return qtyCallsForRedirecting;
    }

    public void setQtyCallsForRedirecting(Integer qtyCallsForRedirecting) {
        this.qtyCallsForRedirecting = qtyCallsForRedirecting;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ShortenedURLDTO shortenedURLDTO = (ShortenedURLDTO) o;
        if (shortenedURLDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), shortenedURLDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ShortenedURLDTO{" +
            "id=" + getId() +
            ", userId=" + getUserId() +
            ", descriptionLongURL='" + getDescriptionLongURL() + "'" +
            ", descriptionShortenedURL='" + getDescriptionShortenedURL() + "'" +
            ", generationAlgorithmType='" + getGenerationAlgorithmType() + "'" +
            ", dateEncoding='" + getDateEncoding() + "'" +
            ", dateLastAccess='" + getDateLastAccess() + "'" +
            ", isActive='" + isIsActive() + "'" +
            ", qtyDecodingRequestsByAPI=" + getQtyDecodingRequestsByAPI() +
            ", qtyCallsForRedirecting=" + getQtyCallsForRedirecting() +
            "}";
    }

}
