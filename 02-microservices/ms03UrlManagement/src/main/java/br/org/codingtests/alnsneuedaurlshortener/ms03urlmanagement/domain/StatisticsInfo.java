package br.org.codingtests.alnsneuedaurlshortener.ms03urlmanagement.domain;

import br.org.codingtests.alnsneuedaurlshortener.ms03urlmanagement.domain.enumeration.ReasonCallEnum;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

/**
 * StatisticsInfo entity.
 * Stores analitics data and matadata for each URLShortened
 * @author Andre Nascimento
 */
@ApiModel(description = "StatisticsInfo entity. Stores analitics data and matadata for each URLShortened @author Andre Nascimento")
@Entity
@Table(name = "TB_STATISTICS_INFO")
public class StatisticsInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ID")    
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "TYP_REASON_CALL_DONE", nullable = false)
    private ReasonCallEnum reasonCallDoneType;

    @NotNull
    @Column(name = "DTH_ACCESS", nullable = false)
    private LocalDateTime dateAccess;

    @Size(max = 20)
    @Column(name = "NUM_IP_ADDRESS_CALLER", length = 20)
    private String numberIPAddressCaller;

    @Size(max = 2)
    @Column(name = "COD_COUNTRY_CALLER", length = 2)
    private String countryCallerCode;

    @ManyToOne
    @JsonIgnoreProperties("statisticsInfosList")
    private ShortenedURL shortenedURL;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    
    public StatisticsInfo() {    	
    	
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ReasonCallEnum getReasonCallDoneType() {
        return reasonCallDoneType;
    }

    public StatisticsInfo reasonCallDoneType(ReasonCallEnum reasonCallDoneType) {
        this.reasonCallDoneType = reasonCallDoneType;
        return this;
    }

    public void setReasonCallDoneType(ReasonCallEnum reasonCallDoneType) {
        this.reasonCallDoneType = reasonCallDoneType;
    }

    public LocalDateTime getDateAccess() {
        return dateAccess;
    }

    public StatisticsInfo dateAccess(LocalDateTime dateAccess) {
        this.dateAccess = dateAccess;
        return this;
    }

    public void setDateAccess(LocalDateTime dateAccess) {
        this.dateAccess = dateAccess;
    }

    public String getNumberIPAddressCaller() {
        return numberIPAddressCaller;
    }

    public StatisticsInfo numberIPAddressCaller(String numberIPAddressCaller) {
        this.numberIPAddressCaller = numberIPAddressCaller;
        return this;
    }

    public void setNumberIPAddressCaller(String numberIPAddressCaller) {
        this.numberIPAddressCaller = numberIPAddressCaller;
    }

    public String getCountryCallerCode() {
        return countryCallerCode;
    }

    public StatisticsInfo countryCallerCode(String countryCallerCode) {
        this.countryCallerCode = countryCallerCode;
        return this;
    }

    public void setCountryCallerCode(String countryCallerCode) {
        this.countryCallerCode = countryCallerCode;
    }

    public ShortenedURL getShortenedURL() {
        return shortenedURL;
    }

    public StatisticsInfo shortenedURL(ShortenedURL shortenedURL) {
        this.shortenedURL = shortenedURL;
        return this;
    }

    public void setShortenedURL(ShortenedURL shortenedURL) {
        this.shortenedURL = shortenedURL;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        StatisticsInfo statisticsInfo = (StatisticsInfo) o;
        if (statisticsInfo.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), statisticsInfo.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "StatisticsInfo{" +
            "id=" + getId() +
            ", reasonCallDoneType='" + getReasonCallDoneType() + "'" +
            ", dateAccess='" + getDateAccess() + "'" +
            ", numberIPAddressCaller='" + getNumberIPAddressCaller() + "'" +
            ", countryCallerCode='" + getCountryCallerCode() + "'" +
            "}";
    }
}
