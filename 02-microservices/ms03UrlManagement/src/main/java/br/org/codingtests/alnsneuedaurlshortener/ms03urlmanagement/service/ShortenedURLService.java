package br.org.codingtests.alnsneuedaurlshortener.ms03urlmanagement.service;

import br.org.codingtests.alnsneuedaurlshortener.ms03urlmanagement.repository.feignclients.dtos.SimpleParametersWrapperDTO;
import br.org.codingtests.alnsneuedaurlshortener.ms03urlmanagement.service.dto.ShortenedURLDTO;
import br.org.codingtests.alnsneuedaurlshortener.ms03urlmanagement.service.dto.StatisticsInfoDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing ShortenedURL.
 */
public interface ShortenedURLService {

    /**
     * Save a shortenedURL.
     *
     * @param shortenedURLDTO the entity to save
     * @return the persisted entity
     */
    ShortenedURLDTO save(ShortenedURLDTO shortenedURLDTO);

    /**
     * Get all the shortenedURLS.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<ShortenedURLDTO> findAll(Pageable pageable);


    /**
     * Get the "id" shortenedURL.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<ShortenedURLDTO> findOne(Long id);

    /**
     * Delete the "id" shortenedURL.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
    

    Optional<ShortenedURLDTO> searchByEncodedKey(String pEncodedKey, StatisticsInfoDTO pStatisticsInfoDTO);


    Page<ShortenedURLDTO> searchByUserId(Long pUserId, Pageable pPageable);

    SimpleParametersWrapperDTO deleteChildrenByUserId(Long pUserId);

}
