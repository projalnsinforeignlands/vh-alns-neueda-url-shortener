package br.org.codingtests.alnsneuedaurlshortener.ms03urlmanagement.service.impl;

import br.org.codingtests.alnsneuedaurlshortener.ms03urlmanagement.domain.ShortenedURL;
import br.org.codingtests.alnsneuedaurlshortener.ms03urlmanagement.domain.StatisticsInfo;
import br.org.codingtests.alnsneuedaurlshortener.ms03urlmanagement.repository.StatisticsInfoRepository;
import br.org.codingtests.alnsneuedaurlshortener.ms03urlmanagement.service.StatisticsInfoService;
import br.org.codingtests.alnsneuedaurlshortener.ms03urlmanagement.service.dto.StatisticsInfoDTO;
import br.org.codingtests.alnsneuedaurlshortener.ms03urlmanagement.service.mapper.StatisticsInfoMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing StatisticsInfo.
 */
@Service
@Transactional
public class StatisticsInfoServiceImpl implements StatisticsInfoService {

    private final Logger log = LoggerFactory.getLogger(StatisticsInfoServiceImpl.class);

    private final StatisticsInfoRepository statisticsInfoRepository;

    private final StatisticsInfoMapper statisticsInfoMapper;

    public StatisticsInfoServiceImpl(StatisticsInfoRepository statisticsInfoRepository, StatisticsInfoMapper statisticsInfoMapper) {
        this.statisticsInfoRepository = statisticsInfoRepository;
        this.statisticsInfoMapper = statisticsInfoMapper;
    }

    /**
     * Save a statisticsInfo.
     *
     * @param statisticsInfoDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public StatisticsInfoDTO save(StatisticsInfoDTO statisticsInfoDTO) {
        log.debug("Request to save StatisticsInfo : {}", statisticsInfoDTO);

        StatisticsInfo statisticsInfo = statisticsInfoMapper.toEntity(statisticsInfoDTO);
        statisticsInfo = statisticsInfoRepository.save(statisticsInfo);
        return statisticsInfoMapper.toDto(statisticsInfo);
    }

    /**
     * Get all the statisticsInfos.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<StatisticsInfoDTO> findAll(Pageable pageable) {
        log.debug("Request to get all StatisticsInfos");
        return statisticsInfoRepository.findAll(pageable)
            .map(statisticsInfoMapper::toDto);
    }


    /**
     * Get one statisticsInfo by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<StatisticsInfoDTO> findOne(Long id) {
        log.debug("Request to get StatisticsInfo : {}", id);
        return statisticsInfoRepository.findById(id)
            .map(statisticsInfoMapper::toDto);
    }

    /**
     * Delete the statisticsInfo by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete StatisticsInfo : {}", id);
        statisticsInfoRepository.deleteById(id);
    }



    /**
     * Search the StatisticsInfo's by ShortenerURL.Id.
     *
     * @param pShortenerURLId the id of the ShortenerURL selected.
     * @param pPageable the paging control.
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<StatisticsInfoDTO> searchByShortenerURLId(Long pShortenerURLId, Pageable pPageable) {

        Example<StatisticsInfo> exampleEntity = null;
        ShortenedURL shortenerURLParent = null;

        log.debug("Request to Search the StatisticsInfo's by ShortenerURL.id: " + pShortenerURLId);

        shortenerURLParent = new ShortenedURL();
        shortenerURLParent.setId(pShortenerURLId);

        exampleEntity = Example.of(new StatisticsInfo().shortenedURL( shortenerURLParent ));

        return statisticsInfoRepository.findAll(exampleEntity, pPageable)
                                       .map(statisticsInfoMapper::toDto);
    }


}
