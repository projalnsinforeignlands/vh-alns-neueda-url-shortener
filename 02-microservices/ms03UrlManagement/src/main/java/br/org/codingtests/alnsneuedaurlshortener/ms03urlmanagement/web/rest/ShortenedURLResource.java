package br.org.codingtests.alnsneuedaurlshortener.ms03urlmanagement.web.rest;

import br.org.codingtests.alnsneuedaurlshortener.ms03urlmanagement.repository.feignclients.dtos.SimpleParametersWrapperDTO;
import br.org.codingtests.alnsneuedaurlshortener.ms03urlmanagement.service.ShortenedURLService;
import br.org.codingtests.alnsneuedaurlshortener.ms03urlmanagement.service.dto.ShortenedURLDTO;
import br.org.codingtests.alnsneuedaurlshortener.ms03urlmanagement.service.dto.StatisticsInfoDTO;
import br.org.codingtests.alnsneuedaurlshortener.ms03urlmanagement.web.rest.errors.BadRequestAlertException;
import br.org.codingtests.alnsneuedaurlshortener.ms03urlmanagement.web.rest.util.HeaderUtil;
import br.org.codingtests.alnsneuedaurlshortener.ms03urlmanagement.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ShortenedURL.
 */
@RestController
@RequestMapping("/api")
public class ShortenedURLResource {

    private final Logger log = LoggerFactory.getLogger(ShortenedURLResource.class);

    private static final String ENTITY_NAME = "shortenedURL";

    private final ShortenedURLService shortenedURLService;

    private String hostnameToRedirect;
    private String portNumberToRedirect;
    private String contextPathNameToRedirect;


    public ShortenedURLResource(ShortenedURLService pShortenedURLService,
                                @Value("${application.config.redirect-to.hostname-to-redirect}") String pHostnameToRedirect,
                                @Value("${application.config.redirect-to.port-number-to-redirect}") String pPortNumberToRedirect,
                                @Value("${application.config.redirect-to.context-path-name-to-redirect}") String pContextPathNameToRedirect) {

        this.shortenedURLService = pShortenedURLService;
        this.hostnameToRedirect = pHostnameToRedirect;
        this.portNumberToRedirect = pPortNumberToRedirect;
        this.contextPathNameToRedirect = pContextPathNameToRedirect;

    }

    /**
     * POST  /shortened-urls : Create a new shortenedURL.
     *
     * @param shortenedURLDTO the shortenedURLDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new shortenedURLDTO, or with status 400 (Bad Request) if the shortenedURL has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/shortened-urls")
    public ResponseEntity<ShortenedURLDTO> createShortenedURL(@Valid @RequestBody ShortenedURLDTO shortenedURLDTO) throws URISyntaxException {
        log.debug("REST request to save ShortenedURL : {}", shortenedURLDTO);
        if (shortenedURLDTO.getId() != null) {
            throw new BadRequestAlertException("A new shortenedURL cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ShortenedURLDTO result = shortenedURLService.save(shortenedURLDTO);
        return ResponseEntity.created(new URI("/api/shortened-urls/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /shortened-urls : Updates an existing shortenedURL.
     *
     * @param shortenedURLDTO the shortenedURLDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated shortenedURLDTO,
     * or with status 400 (Bad Request) if the shortenedURLDTO is not valid,
     * or with status 500 (Internal Server Error) if the shortenedURLDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/shortened-urls")
    public ResponseEntity<ShortenedURLDTO> updateShortenedURL(@Valid @RequestBody ShortenedURLDTO shortenedURLDTO) throws URISyntaxException {
        log.debug("REST request to update ShortenedURL : {}", shortenedURLDTO);
        if (shortenedURLDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ShortenedURLDTO result = shortenedURLService.save(shortenedURLDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, shortenedURLDTO.getId().toString()))
            .body(result);
    }


    /**
     * GET  /shortened-urls : get all the shortenedURLS.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of shortenedURLS in body
     */
    @GetMapping("/shortened-urls")
    public ResponseEntity<List<ShortenedURLDTO>> getAllShortenedURLS(Pageable pageable) {

        List<ShortenedURLDTO> resultList = null;

        log.debug("REST request to get a page of ShortenedURLS");

        Page<ShortenedURLDTO> page = shortenedURLService.findAll(pageable);

        resultList = page.getContent();

        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/shortened-urls");

        resultList.forEach( pOneShortURLDTO -> pOneShortURLDTO.setFullLinkToRedirect( buildFullLinkBasedOnShortenedURL(pOneShortURLDTO.getDescriptionShortenedURL()) ));

        return ResponseEntity.ok().headers(headers).body(resultList);
    }


    private String buildFullLinkBasedOnShortenedURL(String pEncodedKey) {

        return String.format("%s:%s%s/%s",
                             this.hostnameToRedirect,
                             this.portNumberToRedirect,
                             this.contextPathNameToRedirect,
                             pEncodedKey);
    }


    /**
     * GET  /shortened-urls/:id : get the "id" shortenedURL.
     *
     * @param id the id of the shortenedURLDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the shortenedURLDTO, or with status 404 (Not Found)
     */
    @GetMapping("/shortened-urls/{id}")
    public ResponseEntity<ShortenedURLDTO> getShortenedURL(@PathVariable Long id) {
        log.debug("REST request to get ShortenedURL : {}", id);
        Optional<ShortenedURLDTO> shortenedURLDTO = shortenedURLService.findOne(id);
        return ResponseUtil.wrapOrNotFound(shortenedURLDTO);
    }

    /**
     * DELETE  /shortened-urls/:id : delete the "id" shortenedURL.
     *
     * @param id the id of the shortenedURLDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/shortened-urls/{id}")
    public ResponseEntity<Void> deleteShortenedURL(@PathVariable Long id) {
        log.debug("REST request to delete ShortenedURL : {}", id);
        shortenedURLService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
    
    
    /**
     * POST  /shortened-urls/:pEncodedKey : Get/Post the ShortenedURL by it's short URL (Encoded Key)
     *
     * @param pEncodedKey the Encoded Key (Shorter URL) of the shortenedURLDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the shortenedURLDTO, or with status 404 (Not Found)
     */
    @PostMapping("/shortened-urls/_search-by-encodedkey/{pEncodedKey}")
    public ResponseEntity<ShortenedURLDTO> searchByEncodedKey(@PathVariable String pEncodedKey, @RequestBody StatisticsInfoDTO pStatisticsInfoDTO) {

        Optional<ShortenedURLDTO> optShortenedURLDTO = null;

        log.debug("REST request to get ShortenedURL by Encoded Key : {}", pEncodedKey);

        optShortenedURLDTO = shortenedURLService.searchByEncodedKey(pEncodedKey, pStatisticsInfoDTO);

        if (!optShortenedURLDTO.isPresent()) {

            optShortenedURLDTO = Optional.of(new ShortenedURLDTO());

        }

        return ResponseUtil.wrapOrNotFound(optShortenedURLDTO);
    }


    /**
     * GET  /shortened-urls : get the shortenedURLS by UserId.
     *
     * @param pPageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of shortenedURLS in body
     */
    @GetMapping("/shortened-urls/_search-by-userid/{pUserId}")
    public ResponseEntity<List<ShortenedURLDTO>> searchByUserId(@PathVariable("pUserId") Long pUserId, Pageable pPageable) {

        List<ShortenedURLDTO> resultList = null;

        log.debug("REST Request to Search the shortenedURLS by UserId: " + pUserId);

        Page<ShortenedURLDTO> page = shortenedURLService.searchByUserId(pUserId, pPageable);

        resultList = page.getContent();

        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/shortened-urls/_search-by-userid/{pUserId}");

        resultList.forEach( pOneShortURLDTO -> pOneShortURLDTO.setFullLinkToRedirect( buildFullLinkBasedOnShortenedURL(pOneShortURLDTO.getDescriptionShortenedURL()) ));

        return ResponseEntity.ok().headers(headers).body(resultList);

    }


    /**
     * DELETE  /shortened-urls/_search_by_userid/:pUserId : delete the ShortenedURL by UserId.
     *
     * @param pUserId the id of the UserId parent
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/shortened-urls/_delete-by-userid/{pUserId}")
    public ResponseEntity<SimpleParametersWrapperDTO> deleteShortenedsURLsByUserId(@PathVariable Long pUserId) {

        SimpleParametersWrapperDTO simpleParamsDTOResult = null;

        log.debug("REST request to delete ShortenedURL children of the UserId : {}", pUserId);

        simpleParamsDTOResult = shortenedURLService.deleteChildrenByUserId(pUserId);

        return ResponseEntity.ok(simpleParamsDTOResult);

    }


}
