package br.org.codingtests.alnsneuedaurlshortener.ms01users.service.mapper;

import br.org.codingtests.alnsneuedaurlshortener.ms01users.domain.UserExtraInfo;
import br.org.codingtests.alnsneuedaurlshortener.ms01users.service.dto.UserExtraInfoDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity UserExtraInfo and its DTO UserExtraInfoDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface UserExtraInfoMapper extends EntityMapper<UserExtraInfoDTO, UserExtraInfo> {



    default UserExtraInfo fromId(Long id) {
        if (id == null) {
            return null;
        }
        UserExtraInfo userExtraInfo = new UserExtraInfo();
        userExtraInfo.setId(id);
        return userExtraInfo;
    }
}
