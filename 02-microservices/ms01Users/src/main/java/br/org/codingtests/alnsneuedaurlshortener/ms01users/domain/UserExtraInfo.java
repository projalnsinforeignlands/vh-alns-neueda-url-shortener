package br.org.codingtests.alnsneuedaurlshortener.ms01users.domain;


import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.Objects;

/**
 * Extra Info of an User, managed by UAA
 */
@Entity
@Table(name = "TB_USER_EXTRA_INFO")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class UserExtraInfo implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "ds_username_login", nullable = false, unique = true)
    private String descriptionUserNameLogin;

    @Column(name = "ds_fullname", nullable = true)
    private String descriptionFullname;

    @NotNull
    @Pattern(regexp = "^[^@\\s]+@[^@\\s]+\\.[^@\\s]+$")
    @Column(name = "ds_main_email", nullable = false)
    private String descriptionMainEmail;

    @Column(name = "ds_geographic_localization", nullable = true)
    private String descriptionGeographicLocalization;


    public UserExtraInfo(){

    }


    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }


    public String getDescriptionUserNameLogin() {
        return descriptionUserNameLogin;
    }
    public void setDescriptionUserNameLogin(String descriptionUserNameLogin) {
        this.descriptionUserNameLogin = descriptionUserNameLogin;
    }

    public String getDescriptionFullname() {
        return descriptionFullname;
    }
    public void setDescriptionFullname(String descriptionFullname) {
        this.descriptionFullname = descriptionFullname;
    }

    public String getDescriptionMainEmail() {
        return descriptionMainEmail;
    }
    public void setDescriptionMainEmail(String descriptionMainEmail) {
        this.descriptionMainEmail = descriptionMainEmail;
    }

    public String getDescriptionGeographicLocalization() {
        return descriptionGeographicLocalization;
    }
    public void setDescriptionGeographicLocalization(String descriptionGeographicLocalization) {
        this.descriptionGeographicLocalization = descriptionGeographicLocalization;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserExtraInfo that = (UserExtraInfo) o;
        return getId().equals(that.getId()) &&
                getDescriptionUserNameLogin().equals(that.getDescriptionUserNameLogin()) &&
                Objects.equals(getDescriptionFullname(), that.getDescriptionFullname()) &&
                getDescriptionMainEmail().equals(that.getDescriptionMainEmail()) &&
                Objects.equals(getDescriptionGeographicLocalization(), that.getDescriptionGeographicLocalization());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getDescriptionUserNameLogin(), getDescriptionFullname(), getDescriptionMainEmail(), getDescriptionGeographicLocalization());
    }


    @Override
    public String toString() {
        return "UserExtraInfo{" +
                "id=" + id +
                ", descriptionUserNameLogin='" + descriptionUserNameLogin + '\'' +
                ", descriptionFullname='" + descriptionFullname + '\'' +
                ", descriptionMainEmail='" + descriptionMainEmail + '\'' +
                ", descriptionGeographicLocalization='" + descriptionGeographicLocalization + '\'' +
                '}';
    }
}
