package br.org.codingtests.alnsneuedaurlshortener.ms01users.service;

import br.org.codingtests.alnsneuedaurlshortener.ms01users.service.dto.UserExtraInfoDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing UserExtraInfo.
 */
public interface UserExtraInfoService {

    /**
     * Save a userExtraInfo.
     *
     * @param userExtraInfoDTO the entity to save
     * @return the persisted entity
     */
    UserExtraInfoDTO save(UserExtraInfoDTO userExtraInfoDTO);

    /**
     * Get all the userExtraInfos.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<UserExtraInfoDTO> findAll(Pageable pageable);


    /**
     * Get the "id" userExtraInfo.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<UserExtraInfoDTO> findOne(Long id);

    /**
     * Delete the "id" userExtraInfo.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
