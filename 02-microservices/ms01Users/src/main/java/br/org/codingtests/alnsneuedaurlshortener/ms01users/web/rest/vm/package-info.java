/**
 * View Models used by Spring MVC REST controllers.
 */
package br.org.codingtests.alnsneuedaurlshortener.ms01users.web.rest.vm;
