package br.org.codingtests.alnsneuedaurlshortener.ms01users.service.impl;

import br.org.codingtests.alnsneuedaurlshortener.ms01users.repository.UserExtraInfoRepository;
import br.org.codingtests.alnsneuedaurlshortener.ms01users.service.UserExtraInfoService;
import br.org.codingtests.alnsneuedaurlshortener.ms01users.service.mapper.UserExtraInfoMapper;
import br.org.codingtests.alnsneuedaurlshortener.ms01users.domain.UserExtraInfo;
import br.org.codingtests.alnsneuedaurlshortener.ms01users.service.dto.UserExtraInfoDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing UserExtraInfo.
 */
@Service
@Transactional
public class UserExtraInfoServiceImpl implements UserExtraInfoService {

    private final Logger log = LoggerFactory.getLogger(UserExtraInfoServiceImpl.class);

    private final UserExtraInfoRepository userExtraInfoRepository;

    private final UserExtraInfoMapper userExtraInfoMapper;

    public UserExtraInfoServiceImpl(UserExtraInfoRepository userExtraInfoRepository, UserExtraInfoMapper userExtraInfoMapper) {
        this.userExtraInfoRepository = userExtraInfoRepository;
        this.userExtraInfoMapper = userExtraInfoMapper;
    }

    /**
     * Save a userExtraInfo.
     *
     * @param userExtraInfoDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public UserExtraInfoDTO save(UserExtraInfoDTO userExtraInfoDTO) {
        log.debug("Request to save UserExtraInfo : {}", userExtraInfoDTO);
        UserExtraInfo userExtraInfo = userExtraInfoMapper.toEntity(userExtraInfoDTO);
        userExtraInfo = userExtraInfoRepository.save(userExtraInfo);
        return userExtraInfoMapper.toDto(userExtraInfo);
    }

    /**
     * Get all the userExtraInfos.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<UserExtraInfoDTO> findAll(Pageable pageable) {
        log.debug("Request to get all UserExtraInfos");
        return userExtraInfoRepository.findAll(pageable)
            .map(userExtraInfoMapper::toDto);
    }


    /**
     * Get one userExtraInfo by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<UserExtraInfoDTO> findOne(Long id) {
        log.debug("Request to get UserExtraInfo : {}", id);
        return userExtraInfoRepository.findById(id)
            .map(userExtraInfoMapper::toDto);
    }

    /**
     * Delete the userExtraInfo by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete UserExtraInfo : {}", id);
        userExtraInfoRepository.deleteById(id);
    }
}
