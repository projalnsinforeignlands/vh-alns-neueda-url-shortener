package br.org.codingtests.alnsneuedaurlshortener.ms01users.repository;

import br.org.codingtests.alnsneuedaurlshortener.ms01users.domain.UserExtraInfo;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the UserExtraInfo entity.
 */
@Repository
public interface UserExtraInfoRepository extends JpaRepository<UserExtraInfo, Long> {

}
