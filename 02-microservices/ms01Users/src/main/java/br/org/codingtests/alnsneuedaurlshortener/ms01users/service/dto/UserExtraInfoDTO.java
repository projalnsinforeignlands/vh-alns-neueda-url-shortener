package br.org.codingtests.alnsneuedaurlshortener.ms01users.service.dto;

import io.swagger.annotations.ApiModel;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the UserExtraInfo entity.
 */
@ApiModel(description = "Extra Info of an User, managed by UAA")
public class UserExtraInfoDTO implements Serializable {

    private Long id;

    @NotNull
    private String descriptionUserNameLogin;

    private String descriptionFullname;

    @NotNull
    @Pattern(regexp = "^[^@\\s]+@[^@\\s]+\\.[^@\\s]+$")
    private String descriptionMainEmail;

    private String descriptionGeographicLocalization;


    public UserExtraInfoDTO() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public String getDescriptionUserNameLogin() {
        return descriptionUserNameLogin;
    }
    public void setDescriptionUserNameLogin(String descriptionUserNameLogin) {
        this.descriptionUserNameLogin = descriptionUserNameLogin;
    }

    public String getDescriptionFullname() {
        return descriptionFullname;
    }
    public void setDescriptionFullname(String descriptionFullname) {
        this.descriptionFullname = descriptionFullname;
    }

    public String getDescriptionMainEmail() {
        return descriptionMainEmail;
    }
    public void setDescriptionMainEmail(String descriptionMainEmail) {
        this.descriptionMainEmail = descriptionMainEmail;
    }

    public String getDescriptionGeographicLocalization() {
        return descriptionGeographicLocalization;
    }
    public void setDescriptionGeographicLocalization(String descriptionGeographicLocalization) {
        this.descriptionGeographicLocalization = descriptionGeographicLocalization;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        UserExtraInfoDTO userExtraInfoDTO = (UserExtraInfoDTO) o;
        if (userExtraInfoDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), userExtraInfoDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }


    @Override
    public String toString() {
        return "UserExtraInfoDTO{" +
                "id=" + id +
                ", descriptionUserNameLogin='" + descriptionUserNameLogin + '\'' +
                ", descriptionFullname='" + descriptionFullname + '\'' +
                ", descriptionMainEmail='" + descriptionMainEmail + '\'' +
                ", descriptionGeographicLocalization='" + descriptionGeographicLocalization + '\'' +
                '}';
    }
}
