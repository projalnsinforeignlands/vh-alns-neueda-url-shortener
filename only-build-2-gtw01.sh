#!/bin/bash

clear

cd ./03-app-gateways/gtw01AlnsNeuedaUrlShortener

# nvm use --lts 

# npm install

# npm run build 

echo "Building Gtw1-Alns-Neueda-Url-Shortener App..."
mvn -DskipTests clean package -Pprod verify jib:dockerBuild

cd ..
cd ..

cd docker-compose

echo "Re-building containers with Docker Compose..."
docker-compose build app-gw01-alnsneuedaurlshortener

echo "\n\n\n\n\n\n\n App Gateway gtw01AlnsNeuedaUrlShortener built successfully! \n\n\n\n\n\n"
 
