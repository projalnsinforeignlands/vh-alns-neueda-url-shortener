#!/bin/bash

clear

cd docker-compose

echo "Starting Container for Gateway App with Docker Compose..."
docker-compose build app-gw01-alnsneuedaurlshortener && docker-compose up -d app-gw01-alnsneuedaurlshortener 

echo "\n\n\n\n\n\n\n Container for Gateway App Started successfully! \n\n\n\n\n\n"

echo "\n\n\n\n\n\n\n Access Swagger UI from: http://localhost:8080/swagger-ui.html \n\n\n\n\n\n"
