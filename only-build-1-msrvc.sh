#!/bin/bash

clear

echo "Building MS01-Users App..."
mvn -DskipTests clean package -Pprod verify jib:dockerBuild -f ./02-microservices/ms01Users/pom.xml

echo "Building MS02-TextEncoder App..."
mvn -DskipTests clean package -Pprod verify jib:dockerBuild -f ./02-microservices/ms02TextEncoder/pom.xml

echo "Building MS03-Url-Management App..."
mvn -DskipTests clean package -Pprod verify jib:dockerBuild -f ./02-microservices/ms03UrlManagement/pom.xml

cd docker-compose

echo "Re-building containers with Docker Compose..."
docker-compose build app-ms01-users app-ms02-textencoder app-ms03-urlmanagement
 
echo "\n\n\n\n\n\n\n MicroServices cleaned and rebuilt. \n\n\n\n\n\n"
