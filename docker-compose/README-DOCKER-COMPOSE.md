# JHipster generated Docker-Compose configuration

## Usage

Launch all your infrastructure by running: `docker-compose up -d`.

## Configured Docker services

### Service registry and configuration server:
- [JHipster Registry](http://localhost:8761)

### Applications and dependencies:
- gtw01AlnsNeuedaUrlShortener (gateway application)
- gtw01AlnsNeuedaUrlShortener's mysql database
- ms01Users (microservice application)
- ms01Users's mysql database
- ms02TextEncoder (microservice application)
- ms02TextEncoder's mongodb database
- ms03UrlManagement (microservice application)
- ms03UrlManagement's mysql database
- uaa (uaa application)
- uaa's mysql database

### Additional Services:

- [JHipster Console](http://localhost:5601)
- [Zipkin](http://localhost:9411)
