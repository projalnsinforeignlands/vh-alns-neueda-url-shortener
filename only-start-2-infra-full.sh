#!/bin/bash

clear

cd docker-compose

echo "Starting Containers for Full Infra with Docker Compose..."
docker-compose build app-infra-phpmyadmin app-infra-eureka-registry db-mongodb-server && docker-compose up -d app-infra-phpmyadmin app-infra-eureka-registry db-mongodb-server

echo "\n\n\n\n\n\n\n All Full Infra Started successfully! \n\n\n\n\n\n"
 

 
