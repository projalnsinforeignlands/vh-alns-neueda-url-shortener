#!/bin/bash

clear

cd docker-compose

echo "Starting Containers for Microservices and Gateways with Docker Compose..."
docker-compose build app-ms01-users app-ms02-textencoder app-ms03-urlmanagement  && docker-compose up -d app-ms01-users app-ms02-textencoder app-ms03-urlmanagement 

echo "\n\n\n\n\n\n\n All Containers for Microservices Started successfully! \n\n\n\n\n\n"


