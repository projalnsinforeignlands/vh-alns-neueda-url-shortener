#!/bin/bash

cd docker-compose

echo "Stopping all containers with Docker Compose..."
docker-compose down

echo "\n\n\n\n\n\n\n Containers were Stopped. \n\n\n\n\n\n"

cd ..

echo "Only Cleaning MS01-Users App..."
mvn -DskipTests clean -f ./02-microservices/ms01Users/pom.xml

echo "Only Cleaning MS02-TextEncoder App..."
mvn -DskipTests clean -f ./02-microservices/ms02TextEncoder/pom.xml

echo "Only Cleaning MS03-Url-Management App..."
mvn -DskipTests clean -f ./02-microservices/ms03UrlManagement/pom.xml

echo "Only Cleaning Gtw1-Alns-Neueda-Url-Shortener App..."
mvn -DskipTests clean -f ./03-app-gateways/gtw01AlnsNeuedaUrlShortener/pom.xml

echo "\n\n\n\n\n\n\n Everything is Stopped and Cleaned. Bye! \n\n\n\n\n\n"