#!/bin/bash

clear

echo "Building MS01-Users App..."
mvn -DskipTests clean package -Pprod verify jib:dockerBuild -f ./02-microservices/ms01Users/pom.xml

echo "Building MS02-TextEncoder App..."
mvn -DskipTests clean package -Pprod verify jib:dockerBuild -f ./02-microservices/ms02TextEncoder/pom.xml

echo "Building MS03-Url-Management App..."
mvn -DskipTests clean package -Pprod verify jib:dockerBuild -f ./02-microservices/ms03UrlManagement/pom.xml

echo "Building Gtw1-Alns-Neueda-Url-Shortener App..."
mvn -DskipTests clean package -Pprod verify jib:dockerBuild -f ./03-app-gateways/gtw01AlnsNeuedaUrlShortener/pom.xml

cd docker-compose
echo "Starting containers with Docker Compose..."
docker-compose build && docker-compose up -d

echo "\n\n\n\n\n\n\n Access Swagger UI from: http://localhost:8080/swagger-ui.html \n\n\n\n\n\n"
 
